# Dune Playground

*Play, Learn, Earn, and have Fun!*

## How to play Dune Playground games

Visit [https://playground.dune.network/](https://playground.dune.network/) and
start playing our games. You have questions ? ask them on Dune Network Telegram or Discord discussion channels.

You can find below some medium posts describing Dune Playground:
- [Free DUNs and the Rock-Paper-Scissors Game](https://medium.com/dune-network/introducing-dune-playground-part-1-free-duns-and-the-rock-paper-scissors-game-e9bcdb599e99)
- [DGG Token and Moonshot Game](https://medium.com/dune-network/introducing-dune-playground-part-2-dgg-token-and-moonshot-game-3617332127e8)

## How to deploy this infra

A step-by-step tutorial is provided in [DEPLOY.md](./DEPLOY.md).

## How to develop/add DApps

WIP

## Licensing

The files in this repository (except the static ones in `website/` directory) are copyright [Origin-Labs](https://www.origin-labs.com), and are distributed under the terms of the MIT license (see LICENSE.txt for more details).

Static files in `website/` directory are part of the [Editorial template by HTML5 UP](https://html5up.net/editorial), and are licensed under the terms of the CCA 3.0 license (see website/README.txt and website/LICENSE.txt for more details).

## Contributing

You might contribute in various ways:

- play our games and report unexpected behaviors or bugs;
- improve UI/UX if you're a talented designer;
- improve Javascript / DApps code;
- implement new DApps / games;
- suggest new games;
- write tutorials / make videos;
- spread the word / invite friends.

## Contact us

Join Dune Network Telegram or Discord discussion channels, or drop us an email at contact@origin-labs.com.