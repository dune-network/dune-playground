
***This page explains how to configure and deploy Dune Playground on Dune
   devnet, testnet, or even mainnet.***

We assume a Debian-based OS (the following has been tested on Ubuntu 18.04), but
it should be quite easy to adapt the process described below to other Linux distros.


## I. General Architecture

The project's architecture is as follows:
- `config-templates/` contains some JSON configuration templates that should be adapted to deploy (parts of) the project, depending of the targeted network.
- `postgresql-db/`: contains scripts related to the initialization of the database (mainly tables and types creation).
- `website/`: already contains some files from [Editorial template By HTML5UP](https://html5up.net/editorial). Additional files will be generated there by `make website`.
- `systemd-services/` contains shell scripts and systemd services that start different programs, as well as their log files.
- `src` is the most important directory of the project. It contains:
  - Apps / DApps sub-directories, like `rps-with-server/`, `dun-airdrop/`, `dgg-token-exchange/` and `dune-raffle`. Each application's code is itself organized into sub-directories, such as
`smart-contracts/`, `crawler/`,  `monitor/`, `www/`. These sub-directories are present with some content when relevant.
  - common libraries and scripts, such as `nodejs-utils-lib/`, `shell-common` and `website-common`.

<hr/>

## II. Preparing and configuring the project

### 1. Dune Network

You'll (at least) need `dune-client` command-line. Please, follow the steps given
[here](https://dune.network/docs/dune-node-mainnet/introduction/howtoget.html#build-from-sources)
to build Dune Network binaries. Checkout mainnet, testnet or next branches depending on whether you'd like to deploy on mainnet, testnet, or
devnet. Next branch will actually work for all these networks provided that you either prefix `dune-client` and `dune-node` with `DUNE_CONFIG=<network>` (where `<network> in {mainnet, testnet, devnet}`), or you export the environment variable `DUNE_CONFIG=<network>`.

If you're planning to start/use your own Dune node, you should at least:
- generate an identity with `./dune-node identity generate`
- [import a
  snapshot](https://medium.com/dune-network/how-to-import-a-dune-network-snapshot-76a7876f7cce)
  to speedup synchronization (available for mainnet and testnet)

Otherwise, you can just use our public nodes, already provided in the config files.

### 2. Extra tools and dependencies

This tutorial has been tested on Ubuntu 18.04 with nodejs version 12.18.2 and npm version 6.14.5. You may want to add the following sources to get recent versions of these packages:

    echo "deb https://deb.nodesource.com/node_12.x bionic main" | sudo tee -a /etc/apt/sources.list
    echo "deb-src https://deb.nodesource.com/node_12.x bionic main" | sudo tee -a /etc/apt/sources.list

Then, run the following commands to install the needed system tools:

    sudo apt-get update
    sudo apt-get install jq postgresql nodejs php

Some of our scripts are written in `NodeJS` and/or use a `Postgresql` database. Some
shell scripts use `jq` to query JSON files, and `php` is used to quickly start a
local web server and serve the website.

### 3. Config file

Assuming you want to deploy Playground on `Xnet` (with `X in {main, test,
dev}`), start by copying **config-templates/Xnet-config.json** to
Xnet-config.json and **config-templates/global-config.json** to
global-config.json. Then, modify them to fit your settings. In particular, do the following in global-config.json:

- indicate the config file you'll use (ie. Xnet-config.json) in global-config.json.

and do the following in Xnet-config.json:

- `playground-manager` will be the alias of the main Dune Playground wallet. Make sure to have some DUNs (at least ~ 1000 $DUN) in it. If you'd like to deploy on testnet or devnet, you may want to activate a [faucet account](https://faucet.dune.network/) to get some fake $DUN coins. You can use `./dune-client show address <alias> -S` to display `<alias>`'s keys to fill `addr` and `skey` fields of `playground-manager` wallet.

- provide dn1 and private keys for the other wallets (airdrop-manager, rps-manager, ...) listed in the config file. You can use `./dune-client gen keys <alias>` to create fresh wallets and `./dune-client show address <alias> -S` to display their keys, including private keys.

- if you already deployed some contracts, provide their KT1 addresses in the
  contracts field. Otherwise, empty the fields or just keep them unchanged.

- put the right (absolute) path to Dune Network binaries in `dune_network_directory` field.

- provide your own password for the database (which will be configured below).

- provide the UNIX user, UNIX group and the root path of dune-playground
  repository on your machine in fields
  `unix_user`, `unix_group` and `root_path` respectively. This will, in particular be used to generate services, and to set absolute paths of local NodeJS libs.

- provide a correct path to the config file of your network for field
  `dune_config` if you're using a local/private network. Keep it unchanged if
  you're targeting mainnet, testnet, or devnet.

- check that the information provided in `public_api_server` fit your settings, or adapt them if needed.

- [optional] configure the `start_local_node_info` if you want to start a local node, and copy relevant information to `node_info` field so that different programs request this local node.

- [optional] If you have a functioning `mail` command on your testing machine, fill the
  `alert_email_addresses` to receive alerts when services crash. Otherwise, just
  keep the list empty.

- [optional] If you want to enable the DUNs airdrop capability from the website, you should provide a Google captcha secret key in `google_captcha_secret_key` field. Also, edit file `src/website-common/php/template.php` and replace the value of `data-sitekey` attribute (at the end of the document) with the sitekey associated to your secret.

Once config file is correctly filled, run the following command to configure the project:

        ./configure

This configure script will mainly generate files from templates (systemd services and package.json files) using some of the information provided in the JSON config file.

### 4. Initializing wallets

Once the configure file is correctly filled, run the following script to import
wallets (and contracts) to Dune Network client's base directory:

        ./src/accounts-manager/import-addresses/import-addresses.sh

Then, consider running this script to fund your wallets with some $DUNs:

        ./src/accounts-manager/accounts-airdrop/accounts-airdrop.sh


Make sure everything went well. Of course, you can skip these steps if wallets
have already been initialized.

<hr/>

## III. Deploying contracts

### 1. Airdrop Contract

Just run in the root directory of this Git repository:

        ./src/dun-airdrop/smart-contracts/deploy.sh

This will deploy a KT1 contract and store its address with the
**airdrop_contract** alias. Copy the displayed KT1 address and paste it in the
appropriate field in the config file.

### 2. DGG Token

Just run in a terminal:

        ./src/dgg-token/smart-contracts/deploy.sh

The contract will be memorized locally as **dgg_token**. It will be managed by
**playground_manager** alias. Copy the obtained KT1 address in the config file.

### 3. DGG Token Exchange

The following command:

        ./src/dgg-token-exchange/smart-contracts/deploy.sh

Will originated the **dgg_token_exchange** contract. The contract is
associated to dgg_token's address and will be managed by **playground_manager**. Use the displayed KT1 address to fill the corresponding field in the config file.

**Bonus:** Default exchange ratio is `1 $DUN = $10 DGG`. If you want to change
this value once the contract is deployed, just run:
./src/dgg-token-exchange/smart-contracts/update-ratio.sh
&lt;NEW-DGG-VALUE-FOR-ONE-DUNE-HERE&gt;

We'll see below how to fund this contract with $DUNs to be able to exchange them for DGG tokens.

### 4. Rock Paper Scissors (RPS) with server (and crawler)

First, run the following command to originate the contract:

        ./src/rps-with-server/smart-contracts/deploy.sh

The contract will be saved locally as **rps_with_server**. it will be managed by
**rps_with_server_manager** alias, but other manager could be added/removed. Copy the displayed KT1
address after the origination in the corresponding field in the config file.

Once the contract is deployed, we can run the following the command to fund its address
with some $DGG tokens (so that it'll be able to reward players):

./src/dgg-token/smart-contracts/mint.sh rps_with_server

Note that this step is also performed by an `accounts monitor`, so we can skip this step for now and do everything at once for all games contracts.

If its balance is not empty, this smart contract is designed to be able to pay fees (and burn) for players whose addresses are revealed. We'll fund it below to enable this capability.

### 5. Serverless RPS

This version is currently not deployed on https://playground.dune.network. it
stores all the history on-chain and doesn't need an API server or a crawler to
work. However, a lot of burn is payed at each game to store information in the
contract, whereas burn cost is asymptotically zero in the version above, because
old data are automatically garbage-collected and crawled to a relational database.

To deploy this contract, run:

        ./src/serverless-rps/smart-contracts/deploy.sh

The contract will be saved locally as **serverless_rps**. it will be managed by
**serverless_rps_manager** alias. Put the KT1 address displayed at the end of the log in the config file.

Collect-call feature is not implemented for this contract.

### 6. Dune Moonshot with server (and crawler)

The following command should deploy the smart contract::

        ./src/moonshot-with-server/smart-contracts/deploy.sh

The contract will be saved locally as **moonshot_with_server**. it will be managed by
**moonshot_with_server_manager** alias, but other manager could be added/removed. Paste the KT1 address resulting from the origination in corresponding field of the config file.

This contract provides collect-call feature for the `play` entrypoint. We'll fund it with some DUNs and DGGs below.

### 7. Serverless Moonshot

Like for "serverless RPS", this "Moonshot" version is not deployed on https://playground.dune.network, because is requires $DUN burn to store information of each game on-chain. To deploy this contract, run the following script:

        ./src/serverless-moonshot/smart-contracts/deploy.sh

The contract will be saved locally as **serverless_moonshot**. it will be managed by
**serverless_moonshot_manager** alias. Put the KT1 address displayed at the end of the log in the config file.

Collect-call feature is not implemented for this contract.

### 8. Dune Raffle


The following command should deploy the smart contract:

        ./src/dune-raffle/smart-contracts/deploy.sh

Three contracts named `hourly_raffle`, `daily_raffle` and `weekly_raffle` will be saved.
Paste the KT1 addresses resulting from the originations in the corresponding fields of the config file.

These contracts provide collect-call feature for entrypoints that may be called by players. We'll fund it with some DUNs and DGGs below.

### 9. Funding contracts with $DGGs and $DUNs

Games contracts need $DGG tokens to be able to reward players. In addition, the exchange as well as the games contracts with collect-call feature also require $DUNs to exchange them for DGGs, and to pay fees respectively. A systemd service is provided below to monitor the balances and airdrop them when needed. Some helper scripts are also provided to perform individual airdrops (can be useful in dev/debug phase).

For instance, running the following command will transfer 10K $DGG to the contract whose alias is given:

        ./src/dgg-token/smart-contracts/mint.sh <CONTRACT_ALIAS>

and running the following scripts will fund exchange, rps_with_server, moonshot_with_server and raffle contracts with the given amount of $DUNs:

        ./src/dgg-token-exchange/smart-contracts/deposit-duns.sh <DUN_AMOUNT>
        ./src/moonshot-with-server/smart-contracts/deposit-duns.sh <DUN_AMOUNT>
        ./src/rps-with-server/smart-contracts/deposit-duns.sh <DUN_AMOUNT>
        ./src/dune-raffle/smart-contracts/deposit-duns.sh <KIND> <DUN_AMOUNT>

where `<KIND>` is one of `hourly`, `daily` or `weekly`.

<hr/>

## IV. Setting up the database

In what follows, we'll configure a postgresql database (we assume its name is `dune_playground_testnet_db`) and create a user `dune_playground_testnet_user` with password `dune_playground_testnet_password`. Of course, use your own values if you changed default ones in your JSON config file.

### 1. Preparing the database

Assuming you've installed postgresql, you can create the
`dune_playground_testnet_user` user as follows:

        sudo -i -u postgres
        psql
        CREATE USER dune_playground_testnet_user;
        ALTER ROLE dune_playground_testnet_user CREATEDB;


Then, set a password for dune_playground_testnet_user (`dune_playground_testnet_password` in our case) with

        \password dune_playground_testnet_user
        (enter, then provide the password for dune_playground_testnet_user);

After that, create the database with

        CREATE DATABASE dune_playground_testnet_db;

Once done, grant access rights for `dune_playground_testnet_user` to `dune_playground_testnet_db` database:

        GRANT CONNECT ON DATABASE dune_playground_testnet_db to dune_playground_testnet_user;

You're done. You can now quit PSQL and postgress sessions.

### 2. Initializing the database

To (re-)initialize the content of database `dune_playground_testnet_db`, run the
following script:

        ./postgresql-db/reset_database.sh


And at any time, you can connect to the database from a terminal (with regular user, not `postgres`) with:

        psql <db-name> --host <host> --port <port> --username <user>

With the default (testnet) parameters, the command should look like:

    psql dune_playground_testnet_db --host localhost --port 5432 --username dune_playground_testnet_user

<hr/>


## V- Website

### 1. Building the website

The following command should copy/generate necessary file for the website in `website/` directory:

        make website

In particular, a file `parameters.js` containing some of the information provided in config file about contracts , APIs and nodes addresses. Generated files can be wiped at any time with:

        make website-clean

### 2. Serving the website

The easiest way is to use the server provided by `php`. Assuming you'd like to serve the website on `localhost:8001`, this can be achieved by running these command in a terminal:

        cd website
        php -S localhost:8001

Then, visit [http://localhost:8001](http://localhost:8001) in your favorite browser. This should display the main page similar to the one shown at [https://playground.dune.network](https://playground.dune.network). Visiting DApp pages will require the installation of Metal extension. Moreover:

- all DApps supplying information from the API will not work until the API server is started (section below)

- you'll likely not be able until to interact with the games until the monitors/oracles and crawlers are started (section below as well).

<hr />

## VI. Starting programs (monitors, bots, APIs, crawlers) with shell scripts

There are (at least) two ways to start different bots, oracles and APIs: you can
either run the shell scripts provided in `systemd-services/shell-scripts`
(suitable for local deploy and testing), or generate and install systemd services from the templates provided in
`systemd-services/systemd-templates`. The latter solution is rather suitable for
deployment in production and actually relies on the shell scripts.

Let's first see how to start different programs using the shell scripts. Each script `<0N-SCRIPT>.sh` in
`systemd-services/shell-scripts` (where `<0N-SCRIPT>` is the name of the script)
can be:

- started, with `./systemd-services/shell-scripts/<0N-SCRIPT>.sh start`
- stopped, with `./systemd-services/shell-scripts/<0N-SCRIPT>.sh stop`
- restarted, with `./systemd-services/shell-scripts/<0N-SCRIPT>.sh restart`
- queried for its status, with `./systemd-services/shell-scripts/<0N-SCRIPT>.sh status`

Note that, LOG and PID files are stored in `systemd-services/logs/<SCRIPT>.log`
and `systemd-services/logs/<SCRIPT>.pid` respectively. Moreover, at each service / script (re-)start:

- the LOGs will be rotated using `dune-rotate`

- if the `email` command is configured to send emails from your machine, an
  email is sent to the addresses provided in the config file. This is quite useful
  when the scripts are deployed in production via systemd services.

### 1. Local node

If you enabled starting a local node in `start_local_node_info` field of configure json file, running the following script will start the node:

        ./systemd-services/shell-scripts/01-dune-node.sh start

Once started, have a look into LOG file to check if the node is running correctly and connecting to others.

**/!\ If you're using a local node, make sure it's synchronized before starting other programs.**

### 2. API server

REST API server can be started with the following command:

        ./systemd-services/shell-scripts/02-api-server.sh start

The (local) server will be started on the address/port given in the config file.
Do the necessary to expose the API server to the Internet if you're testing a public deployment. In any case, have a look to the LOG file to check that everything went well. You can also test the API server as follows (assuming the default settings):

        curl 127.0.0.1:3302/about

which should respond with: `REST API server for Dune Playground`. You may want to adapt the command `curl <API_ADDRESS>:<API_PORT>/about` if you're using different parameters.


### 3. Playground accounts airdrop

The following script will start a process that monitors the DUN and/or DGG balances of accounts and contracts given in JSON configure file:

        ./systemd-services/shell-scripts/03-playground-accounts-airdrop.sh start

 and airdrop any account/contract whose balance is below a threshold (current threshold is fixed to `'airdrop amount indicated in config file' / 2`). For instance, this script will automatically airdrop the contracts we originated with some DUN and DGG tokens if not already done above.

### 4. Airdrop bot

 This program can be started as follows:

        ./systemd-services/shell-scripts/04-airdrop-bot.sh start

It monitors the `accounts_to_airdrop` table in the database and airdrops any dn1 address added to it. If you want to test this bot, just connected to the database and insert a dn1 address into `accounts_to_airdrop`:

        insert into accounts_to_airdrop (dn1_) values ('dn1XhF8QLJyYhwzZWa7q8dLwRmipXe9iUzpd');


### 5. RPS-with-server crawler

The following command will start the crawler of RPS-with-server's smart contract:

        ./systemd-services/shell-scripts/05-rps-with-server-crawler.sh start

It will monitor (play and reveal) transactions to `rps_with_server` contract and record the information it extracts in the `crawled_rps_with_server` table of the database.


### 6. RPS-with-server monitor

This script is the main manager of `rps_with_server` contract. It is responsible for commitments injection and revelation. It can be started as follows:

        ./systemd-services/shell-scripts/06-rps-with-server-monitor.sh start

At this point, the RPS-with-server game is almost ready: only the website/UI that allows to interact with the DApp is missing. Alternatively, you can also play in command-line, which really useful for testing. Don't forget to have a look to the LOG file to check if everything is running correctly.

### 6.b. Serverless-RPS monitor
Serverless-RPS don't has a crawler, but does have a monitor. It can be ran with the following command:

        ./systemd-services/shell-scripts/06b-serverless-rps-monitor.sh start

The principle is similar to the one above: it watches the contract and insert or reveal commitments when needed.

### 7. Moonshot-with-server crawler

Similarly to `rps_with_server`, the following command will start the crawler of
`moonshot_with_server`:

        ./systemd-services/shell-scripts/07-moonshot-with-server-crawler.sh start

The two crawlers are actually quite similar and use the same lib/basis. This crawler monitors (play and reveal) transactions made to `moonshot_with_server` contract and records the information it extracts in the `crawled_moonshot_with_server` table of the database.

### 8. Moonshot-with-server monitor

The oracle monitor of the `moonshot_with_server` contract can be started with:

        ./systemd-services/shell-scripts/08-moonshot-with-server-monitor.sh start

Its role is to reveal commitments once players made some choices, and to insert fresh commitments when the number of open parties is below some threshold.

### 8.b. Serverless-Moonshot monitor

Following the same logic, this command allows to start the oracle monitor of the serverless version of Dune Moonshot:

        ./systemd-services/shell-scripts/08b-serverless-moonshot-monitor.sh start


### 9. Dune-raffle crawler

The following command will start the crawler of `raffle` contracts:

        ./systemd-services/shell-scripts/09-raffle-with-server-crawler.sh start

The program monitors different calls to deployed raffle smart contracts and records the information it extracts in appropiate tables.

### 8. Dune-Raffle monitor

The monitor of the `raffle` contracts can be started with:

        ./systemd-services/shell-scripts/10-raffle-with-server-monitor.sh start

Its roles are:
- opening new raffle games, by inserting a commitment,
- revealing the secret (and nonce) correponding to a commitment to end a game,
- reimbursing players in case of compromised or expired game.


<hr/>

## VII. Starting programs as systemd services

This part is rather intended for deployment in production, as systemd services allow to automatically restart the programs if/when they crash.

services template and scripts are provided in `systemd-services/systemd/`. Running the `../configure` script previously already generated `.service` files from `.service.in` templates.

To deploy and enable system-d services, run the following command:

        make services-deploy

Once this is done, you can start the services with:

        make services-start

At any time, you can query the status of the services with:

        make services-status

Of course, you can restart or stop the services, or clean the services generated from the templates with `make services-restart`, `make services-stop`, and `make services-clean` respectively.

Finally, services can be undeployed with:
        make services-undeploy

Note that, if a local node is not enabled, its corresponding service will not be deployed.
