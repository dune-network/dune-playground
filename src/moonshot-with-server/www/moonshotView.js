/* global utils */

var moonshotView = {

    __colorOfStatus: function(status) {
        switch (status) {
            case "Won":
                return "#cfc";
            case "Lost":
                return "#fcc";
            case "Tie":
                return "#dde";
            case "Waiting for inclusion":
                return "#fc6";
            default:
                return "#ff6";
        }
    },

    updatePlayed: function(data, last_game) {
        // player stats
        utils.setHTML('play-self-finished', data.user_nb_games);
        utils.setHTML('play-self-nb-won', data.user_nb_won);
        utils.setHTML('play-self-success-rate', data.user_success);
        // global stats
        utils.setHTML('play-global-finished', data.global_last_revealed);
        utils.setHTML('play-global-ongoing', data.global_last_played - data.global_last_revealed);
        utils.setHTML('play-global-open', data.global_last_commit - data.global_last_played);
        utils.setHTML('moonshotChoice', '' + data.sliderValue + ' km');

        if (last_game !== null) {
            console.log('Data: ', last_game.player_move, last_game.oracle_move);
            moonshot_animation(
                last_game.player_move,
                last_game.oracle_move
            );
        }

        var play_msg = '';
        var next_msg = '';
        if (data.last_move_position !== null) {
            next_msg += 'Latest injected choice';
            if (data.last_move_hash !== null) {
                let op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                next_msg += ' (hash ' + op_hash + ')';
            }
            if (data.last_move_position < 0) {
                next_msg += ' is waiting for inclusion.';
            } else {
                next_msg += ' has been included ' + data.last_move_position + ' block(s) ago.';
            }
            next_msg = '<p class="pp" style="margin:0 80px">' + next_msg + '</p>';
            if (data.last_move_position >= data.params.lock_player_x_blocks) {
                play_msg += '<h2>You can play again! Make another choice...</h2><p class="pp">Select a number between 1 and 8 above</p><br/>';
                document.getElementById('play-area').classList.remove('disabled-div');
            } else {
                play_msg += '<h2>Wait for your last choice to be included...</h2>';
                document.getElementById('play-area').classList.add('disabled-div');
            }
        } else {
            document.getElementById('play-area').classList.remove('disabled-div');
            play_msg += '<h2>Make Your Choice</h2><p class="pp">Select a number between 1 and 8 above</p><br/>';
        }
        utils.setHTML('play-move-status', next_msg);
        utils.setHTML('play-message', play_msg);
    },

    pendingAnimation: function() {
        return ('<img src="images/loader.gif" style="height:30px"></img>');
    },

    addRow: function(rows, g) {
        if (g.failed) {
            color = moonshotView.__colorOfStatus(g.game_status);
            let player_move = (g.op_played == null) ? g.player_move : utils.explorerLink(g.op_played, g.player_move, true);
            rows = rows + "<tr class='pp' style='background-color:#ccc'>";
            rows += "<td>-</td>";
            rows += "<td>" + player_move + "</td>";
            rows += "<td>-</td>";
            rows += "<td>Operation failed</td>";
            rows += "<td>-</td>";
            rows += "</tr>";
        } else {
            color = moonshotView.__colorOfStatus(g.game_status);
            let player_move = (g.op_played == null) ? g.player_move : utils.explorerLink(g.op_played, g.player_move, true);
            rows = rows + "<tr class='pp'>";
            rows += "<td>" + g.game_id + "</td>";
            rows += "<td>" + player_move + "</td>";
            if (g.oracle_move === 'Not revealed yet') {
                rows += "<td>" + (moonshotView.pendingAnimation()) + "</td>";
            } else {
                let oracle_move = (g.op_revealed == null) ? g.oracle_move : utils.explorerLink(g.op_revealed, g.oracle_move, true);
                rows += "<td>" + oracle_move + "</td>";
            }
            rows += "<td style=\"background-color:" + color + "\">" + g.game_status + "</td>";
            rows += "<td>" + g.reward + " DGG</td>";
            rows += "</tr>";
        }
        return rows;
    },

    showPendingAsRow: function(rows, data, dt) {
        if (data.last_move_hash === null || data.current_history_page > 1 ||
            dt.some(e => { return (e.op_played === data.last_move_hash) })) {
            return rows;
        } else {
            // even if the operation is included, keep showing it as 'waiting for inclusion'
            // until indexer sees it. This is different from serverless version
            let g = {
                game_id: moonshotView.pendingAnimation(),
                oracle_move: ' ',
                player_move: data.last_move_value,
                reward: '?',
                op_played: data.last_move_hash,
                game_status: 'Waiting for inclusion'
            };
            return (moonshotView.addRow(rows, g));
        }
    },

    updatePage: function(data, dt, hpage) {
        last_played = dt.length > 0 ? dt[0] : null;
        moonshotView.updatePlayed(data, last_played);
        var rows = "";
        rows = moonshotView.showPendingAsRow(rows, data, dt);
        for (i = 0; i < dt.length; i++) {
            let g = dt[i];
            rows = moonshotView.addRow(rows, g);
        }
        // default
        //document.getElementById('reveal-pubkey').style.display = 'none';
        document.getElementById('pay-fees').style.display = 'block';
        document.getElementById('get-free-duns').style.display = 'none';
        document.getElementById('collectCall').checked = false;
        document.getElementById('collectCall').disabled = true;

        if (data.can_pay_fees) {
            document.getElementById('collectCall').checked = data.do_pay_fees;
            document.getElementById('collectCall').disabled = false;
        }
        let metalState = metalModel.getState();
        if (metalState === null || !metalState.revealed || metalState.dun_balance === 0) {
            document.getElementById('get-free-duns').style.display = 'inline-block';
        }

        utils.setHTML('moonshot-history-games-list', rows);
        utils.setHTML('history-page-number', 'Page ' + hpage);
    },

    updateSlider: function(data) {
        utils.setHTML('moonshotChoice', '' + data.sliderValue + ' km');
        // nothing to do
    },

    updatePlayedChoice: function(op) {
        return new Promise(
            function(resolve, reject) {
                if (op.ok && op.msg !== undefined) {
                    console.log(JSON.stringify(op));
                    let op_hash = utils.explorerLink(op.msg, op.msg, true);
                    var msg = '';
                    msg += '<p class="pp" style="margin:0 80px">Latest choice injected with hash ' + op_hash + '</p>';
                    utils.setHTML('play-move-status', msg);
                }
                resolve();
            });
    }
};
var moonshotRocketView = {
    sleep: function(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    },

    red: 120,
    green: 120,
    blue: 255,
    max_gh: 120,
    cur_gh: 0,
    rocket_angle: 0,
    moon_x: -100,
    moon_y: 700,
    rocket_top: 300,
    rocket_left: 0,

    set_sky_color: function(launched) {
        document.getElementById('sky1').style.backgroundColor = 'rgb(' + moonshotRocketView.red + ',' + moonshotRocketView.green + ',' + moonshotRocketView.blue + ')';
        //document.getElementById('sky2').style.backgroundColor = 'rgb(' + moonshotRocketView.red + ',' + green + ',' + blue + ')';
        document.getElementById('sky3').style.backgroundColor = 'rgb(' + moonshotRocketView.red + ',' + moonshotRocketView.green + ',' + moonshotRocketView.blue + ')';
        if (launched) {
            if (moonshotRocketView.cur_gh < 120) {
                moonshotRocketView.cur_gh += 10;
                moonshotRocketView.max_gh -= 10;
                document.getElementById('sky3').style.height = moonshotRocketView.cur_gh + 'px';
                document.getElementById('green').style.height = moonshotRocketView.max_gh + 'px';
            }
        }
    },

    set_ground_color: function() {
        document.getElementById('green').style.backgroundColor =
            'rgb(33,' + moonshotRocketView.green + ',33)';
    },

    rotate_rocket: function(i) {
        if (i >= 10 && moonshotRocketView.rocket_angle <= 45) {
            moonshotRocketView.rocket_angle += 2;
            moonshotRocketView.rocket_top -= 7.5;
            moonshotRocketView.rocket_left += 3.1;
            document.getElementById('rocket').style.transform = 'rotate(' + moonshotRocketView.rocket_angle + 'deg)';
            document.getElementById('rocket').style.top = moonshotRocketView.rocket_top + 'px';
            document.getElementById('rocket').style.left = moonshotRocketView.rocket_left + 'px';
        }
    },

    move_moon: function(i) {
        if (i >= 10 && moonshotRocketView.rocket_angle <= 45) {
            moonshotRocketView.moon_x += 15;
            moonshotRocketView.moon_y -= 20;
            document.getElementById('sky1').style.backgroundPosition =
                moonshotRocketView.moon_y + 'px ' + moonshotRocketView.moon_x + 'px';
        }
    },

    demo: async function() {
        moonshotRocketView.blue -= 5;
        moonshotRocketView.green -= 5;
        moonshotRocketView.red -= 5;
        await moonshotRocketView.sleep(5000);
        console.log('Two seconds later, showing sleep in a loop...');
        // Sleep in loop
        for (let i = 0; i < 40; i++) {
            console.log('blue ' + moonshotRocketView.blue + ' | max_gh ' + moonshotRocketView.max_gh);
            moonshotRocketView.blue -= 5;
            moonshotRocketView.green -= 5;
            moonshotRocketView.red -= 5;
            moonshotRocketView.set_sky_color(true);
            moonshotRocketView.rotate_rocket(i);
            moonshotRocketView.move_moon(i);
            await moonshotRocketView.sleep(100);
            console.log(i);
        }
    },

    init: function() {
        moonshotRocketView.set_ground_color();
        moonshotRocketView.set_sky_color(false);
    }
};