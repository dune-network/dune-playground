#love

type choice = nat

(* A played game is made of the Oracle's commit (hashed choice), and of the
   player's address and choice, in addition to the player choice's timestamp and block level *)
type played = {
  commit : bytes;
  player : address;
  p_choice : choice;
  when_played : timestamp;
  block_level : nat
}

(* A pending game is either 'Initialized' with some hidden (hashed) choice from
   the oracle; or 'Played', where some player made a choice. In this case the
   oracle can reveal its hidden choice and finish the game. *)
type pending_game =
  | Initialized of bytes (* commitment *)
  | Played of played (* some player made a choice *)

(* A finished game is either won or lost by the player *)
type status =
  | PlayerWins
  | PlayerLoses

(* A game ID is just an int (used as index in bigmap of games) *)
type game_id = int

(* The token players earn when playing RPS is a nat *)
type dgg = nat

(* A finished game's result: all needed fields to ease crawling and displaying the
  information of the game in a DApp *)
type game_result = {
  id : game_id;
  block_lvl : nat;
  player_addr : address;
  player_choice : choice;
  oracle_choice : choice;
  status : status;
  reward : dgg
}

type params = {
  oracle_addresses : (address, unit) bigmap;
   (* address authorized to insert commitments and reveal them. We could put a set to scale *)
  nb_oracle_addresses : int;
  tokens_addr : address; (* ERC-20 like token address *)
  min_time_between_blocks : nat; (* min time between blocks of this chain *)
  lock_player_x_blocks : nat; (* min number of blocks between two games from the same address *)
  lock_reveal_x_blocks : nat; (* min number of blocks between a played game and its revelation *)
  bet_amount : dun; (* Amount each player should bet to make a choice *)
  bytes_size : nat; (* Size in bytes of commitments and nonces: smaller values
                       allow to pay less fees and storage, but they are easier
                       to bruteforce. *)
  min_choice: choice;
  max_choice: choice;
  looser_reward : dgg; (* The amount of tokens earned if the player loses *)
  winner_reward_map : (choice, dgg) bigmap; (* map of rewards *)
}

(* Games is a map from game_ids to pending_games *)
type games = (game_id, pending_game) bigmap

(* To prevent players from playing more than once in each block, we regitster
   for each address the timestamp of its last game. There are also other
   statistics stored directly in the contract. *)
type player_info = {
  last_game : game_id;
  last_game_timestamp : timestamp;
  nb_games : nat;
  nb_won : nat;
  nb_lost : nat;
}

type players = (address, player_info) bigmap

type storage = {
  games : games; (* map of games *)
  players : players; (* map of players *)
  last_revealed : game_id; (* last revealed/finished game. Always <= last_played and last_commit *)
  last_played : game_id; (* last played (pending) game. Always <= last_commit *)
  last_commit : game_id; (* last commited (initialied) game by the oracle *)
  params : params
}

(* --- auxiliary functions --- *)

(* initial storage *)
val%init storage (params : params) = {
  games =  BigMap.empty [:game_id] [:pending_game];
  players =  BigMap.empty [:address] [:player_info];
  last_revealed = 0;
  last_played = 0;
  last_commit = 0;
  params;
}

val check_is_oracle (oracle_addresses : (address, unit) bigmap) : unit =
  let sender = Current.sender () in
  if not (BigMap.mem [:address] [:unit] sender oracle_addresses) then
    failwith [:string]
      "This entrypoint can only be called by the oracle addr" [:unit]

(* Check that Current.amount () is equal to the given amount. Fail otherwise *)
val check_is_amount (amnt : dun) : unit =
  if (Current.amount ()) <>[:dun] amnt then
   failwith [: string * dun * dun]
     ("Invalid amount. (expected, provided) = ", amnt, (Current.amount ())) [: unit]


contract type Token = sig
  val%entry transfer : (address * nat * bytes option)
end

val make_transfer (tokens_addr: address) (dest : address) (amount : dgg) : operation =
  match Contract.at<:Token> tokens_addr with
  | None ->
    failwith [:string * address] ("No Token contract found at ", tokens_addr)
    [:operation]

  | Some (contract TKN : Token)->
    TKN.transfer 0DUN (dest, amount, (None [:bytes]))

(* Check that sha256 (0x (choice ^ nonce) = commit. Fail otherwise *)
val check_commitment (choice : bytes) (nonce : bytes) (commit : bytes) (bytes_size : nat) : unit =
  (* commit and nonce are assumed to have size 'params.bytes_size' *)
  let s = Bytes.concat ([choice; nonce] [:bytes]) in
  let h = Crypto.sha256 s in
  let h = match Bytes.slice 0p bytes_size h with (*shrink to params.bytes_size bytes*)
    | None ->
      failwith [:string * bytes * nat]
        ("check_commitment. Invalid length in Bytes.slice", h, bytes_size) [:bytes]
    | Some h ->
      h
  in
  if h <>[:bytes] commit then
   failwith [: string * bytes * bytes * bytes * bytes * bytes]
     ("revelation does not match commitment. (choice, nonce, s, hash(s), com) =",
      choice, nonce, s, h, commit) [:unit]

val pack_choice (choice : choice) : bytes =
  Bytes.pack [:choice] choice

val find_game (gid : game_id) (games : games) : pending_game =
  match BigMap.find [:game_id] [:pending_game] gid games with
  | None ->
    failwith [:string * game_id] ("No game found with id, ", gid) [:pending_game]
  | Some g ->
    g

(* Given a game ID, the oracle's choice and nonce, this view (a) checks that the
   game exists and some player made a choice, (b) determines the winner, and (c) returns
   the result of type 'game_result'.
   This vue is called by both the oracle that monitors this smart contract to automatically
   get the information to reveal for a game, and by the reveal entrypoint of this contract. *)
val%view get_game_result
    (storage : storage)
    (id : game_id)
    (oracle_choice : choice)
    (nonce : bytes)
  : (player_info * game_result) =
  let params = storage.params in
  match find_game id storage.games with
  | Initialized _ ->
    failwith [:string]
      "[Oracle failure] shouldn't reveal a commit until a player makes a choice"
      [:player_info * game_result]
  | Played played ->
    let { commit; player; p_choice; when_played; block_level } = played in
    (* wait time as an integer (extra *+! 1) *)
    let wait_time = (params.min_time_between_blocks *+ params.lock_reveal_x_blocks) *+! 1 in
    let delta = Current.time () -: when_played in (* delta is an integer *)
    if delta <[:int] wait_time then
      failwith [:string * int]
        ("[Oracle failure] should wait before revealing", delta) [:unit];
    (* If we are here, it remains to check nonce+choice against commit, and update storage *)
    let oracle_xmv = pack_choice oracle_choice in
    let _player_xmv = pack_choice p_choice in
    check_commitment oracle_xmv nonce commit params.bytes_size;
    match BigMap.find [:address] [:player_info] player storage.players with
    | None ->
      failwith [:string * address]
        ("Invariant: if the player made a choice, he is in the players map!", player)
        [:player_info * game_result];
    | Some pinfo ->
      let diff = oracle_choice -+ p_choice in
      let pinfo, game_stt, dgg_amount =
        match Nat.of_int diff with
        | None ->
          { pinfo with nb_lost = pinfo.nb_lost ++1p }, PlayerLoses, params.looser_reward
        | Some _d ->
          match BigMap.find [:choice] [:dgg] p_choice params.winner_reward_map with
          | Some rew ->
            { pinfo with nb_won = pinfo.nb_won ++1p }, PlayerWins, rew
          | None ->
            failwith [:string * choice]
              ("No corresponding reward found for this winning choice. Is the \
                contract well initialized?", p_choice)
              [:player_info * status * dgg]
      in
      pinfo,
      { id;
        block_lvl = block_level;
        player_addr = player;
        player_choice = p_choice;
        oracle_choice;
        status = game_stt;
        reward = dgg_amount }

(* Add a new user choice in the map of games *)
val play_choice (storage : storage) (player : address) (p_choice : choice) : storage =
  let params = storage.params in
  check_is_amount params.bet_amount;
  if p_choice <[:choice] params.min_choice || p_choice >[:choice] params.max_choice then
    failwith [:string] "Bad choice!" [:unit];
  let last_played = storage.last_played + 1 in
  if last_played >[:int] storage.last_commit then
    failwith [:string]
      "No open games for the moment. Wait until the oracle inserts some" [:unit];
  let pinfo =
    match BigMap.find [:address] [:player_info] player storage.players with
    | None ->
      { last_game = last_played;
        last_game_timestamp = Current.time();
        nb_games = 1p;
        nb_won  = 0p;
        nb_lost = 0p; }
    | Some pinfo ->
      (* wait time as an integer (extra *+! 1) *)
      let wait_time = (params.min_time_between_blocks *+ params.lock_player_x_blocks) *+! 1 in
      let delta = Current.time () -: pinfo.last_game_timestamp in (* integer *)
      if delta <[:int] wait_time then
        failwith [:string * int] ("You should wait before playing again", delta) [:unit];
      { pinfo with
        last_game = last_played;
        last_game_timestamp = Current.time();
        nb_games = pinfo.nb_games ++ 1p }
  in
  match find_game last_played storage.games with
  | Played _ ->
    failwith
      [:string] "[Invariant] should not have an Initialized game with a status 'Played'"
      [:storage];
  | Initialized commit ->
    let game = Played {
        commit ;
        player;
        p_choice;
        when_played = Current.time();
        block_level = Current.level()
      }
    in
    let games = BigMap.add [:game_id] [:pending_game] last_played game storage.games in
    let players = BigMap.add [:address] [:player_info] player pinfo storage.players in
    { storage with last_played; games; players }

val check_same_game (g1 : game_result) (g2 : game_result) : unit =
  if g1.id <>[:int] g2.id ||
  g1.block_lvl <>[:nat] g2.block_lvl ||
  g1.player_addr <>[:address] g2.player_addr ||
  g1.player_choice <>[:choice] g2.player_choice ||
  g1.oracle_choice <>[:choice] g2.oracle_choice ||
  g1.status <>[:status] g2.status ||
  g1.reward <>[:nat] g2.reward
  then
    failwith [:string * int * int]
      ("[Oracle failure] revealed game info doesn't match the one computed. ID = ", g1.id, g2.id)
      [:unit]


(* --- entrypoints --- *)

(* a case where a player makes some choice *)
val%entry play storage d (choice : choice) =
  [%fee (* This entrypoint may pay for fees *)
    if Current.balance () <[:dun] 0.010DUN ||
     storage.last_played =[:int] storage.last_commit
     then
      (0DUN, 0p)
    else
      match BigMap.find [:address] [:player_info] (Current.sender()) storage.players with
      | None -> (0.020DUN, 300p)
      | Some pinfo ->
        let params = storage.params in
        let wait_time = (params.min_time_between_blocks *+ params.lock_player_x_blocks) *+! 1 in
        let delta = Current.time () -: pinfo.last_game_timestamp in
        if delta <[:int] wait_time then (0DUN, 0p)
        else (0.015DUN, 150p)
  ]
  [][:operation], play_choice storage (Current.sender ()) choice

(* a case where the oracle makes a choice on behalf of players *)
val%entry play_for storage d (users_choices : (address * choice) list) =
  check_is_oracle storage.params.oracle_addresses;
  let storage =
    List.fold [:address * choice] [:storage]
      begin
        fun ((addr, choice) : address * choice) (storage : storage) ->
          play_choice storage addr choice
      end
      users_choices
      storage
  in
  [] [:operation], storage

(* insert a list of commitments *)
val%entry insert_commits storage d ((last_commit, commits) : int  * bytes list) =
  check_is_oracle storage.params.oracle_addresses;
  if last_commit <>[:int] storage.last_commit then
    failwith [:string * int * int]
      ("bad provided last_commit. (expected, provided) = ", storage.last_commit, last_commit)
      [:unit];
  let last_commit, games =
    List.fold [:bytes] [:game_id * games]
      begin
        fun (commit : bytes) ((last_commit, games) : game_id * games) ->
          let g = Initialized commit in
          let last_commit = last_commit + 1 in
          (last_commit, BigMap.add [:game_id] [:pending_game] last_commit g games)
      end
      commits
      (storage.last_commit, storage.games)
  in
  [] [:operation], { storage with games; last_commit }

(* reveal a list of commitments. This consists in providing the nonce, and directly the games' results.
This simplify crawling the games by just reading the content of transactions parameters *)
val%entry reveal_commits storage d (revelations : (game_result * bytes) list) =
  check_is_oracle storage.params.oracle_addresses;
  let params = storage.params in
  List.fold [:game_result * bytes] [:operation list * storage]
    (fun ((game_res, nonce) : (game_result * bytes)) ((ops, storage) : (operation list * storage)) ->
       let last_revealed = storage.last_revealed + 1 in
       let pinfo, g_res = get_game_result storage last_revealed game_res.oracle_choice nonce in
       check_same_game g_res game_res;
       let ops =
         if game_res.reward =[:dgg] 0p then ops
         else
           (make_transfer params.tokens_addr game_res.player_addr game_res.reward) :: ops
       in
       let players =
         BigMap.add [:address] [:player_info] game_res.player_addr pinfo storage.players in
       let games =
         BigMap.remove [:game_id] [:pending_game] last_revealed storage.games in
       ops, { storage with last_revealed; players; games }
    ) revelations ([] [:operation], storage)

val%entry deposit storage d (_ : unit) =
  [][:operation], storage

val%entry withdraw storage d (amount : dun) =
  check_is_oracle storage.params.oracle_addresses;
  [Account.transfer (Current.sender()) amount][:operation], storage

(* add/remove an oracle address *)
val%entry update_oracle_address storage d (addr : address) =
  check_is_oracle storage.params.oracle_addresses;
  let params = storage.params in
  let oracles = params.oracle_addresses in
  let nb_oracles = params.nb_oracle_addresses in
  let params =
    match BigMap.find [:address] [:unit] addr params.oracle_addresses with
    | None -> (* add address *)
      { params with
          oracle_addresses = BigMap.add [:address] [:unit] addr () oracles;
          nb_oracle_addresses = nb_oracles + 1 }
    | Some () -> (* remove address *)
      if nb_oracles <=[:int] 1 then failwith [:string] "Cannot remove more oracles" [:unit];
      { params with
          oracle_addresses = BigMap.remove [:address] [:unit] addr oracles;
          nb_oracle_addresses = nb_oracles - 1 }
  in
  [][:operation], {storage with params}
