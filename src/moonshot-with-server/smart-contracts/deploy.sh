#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

if [ "$DUNE_PLAYGROUND_TIME_BETWEEN_BLOCKS" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_TIME_BETWEEN_BLOCKS env variable"
    exit 1
fi

# bytes_size = 10 means 80 bits, harder to bruteforce than an int64

REWARDS_MAP="{nat, nat: 1p -> 1p; 2p -> 2p; 3p -> 3p; 4p -> 5p; 5p -> 8p; 6p -> 13p; 7p -> 21p; 8p -> 34p}"

$DUNE_PLAYGROUND_CLIENT_CMD originate contract moonshot_with_server \
                            transferring 0 from playground_manager \
                            running file:contract.lov.ml \
                            --init '#love:{ min_choice = 1p; max_choice = 8p; min_time_between_blocks = '$DUNE_PLAYGROUND_TIME_BETWEEN_BLOCKS'p ; bytes_size=10p; oracle_addresses = {address, unit:'\${pkh:moonshot_with_server_manager}' -> ()}; nb_oracle_addresses = 1; tokens_addr = '\${contract:dgg_token}'; lock_player_x_blocks = 1p; lock_reveal_x_blocks = 0p; bet_amount = 0dun; looser_reward = 1p; winner_reward_map = '"$REWARDS_MAP"';  }' \
                            --burn-cap 10 --force
