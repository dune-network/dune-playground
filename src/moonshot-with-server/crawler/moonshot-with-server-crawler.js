const crawler_lib = require('crawler-lib-1');

decode_choice = function(e) {
    return parseInt(e.nat)
}

let info = {
    table: 'crawled_moonshot_with_server',
    contract_name: 'moonshot_with_server',
    decode_choice: decode_choice,
}

crawler_lib.main(info);