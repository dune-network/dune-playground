const contracts_monitor_lib = require('contracts-monitor-lib');
const utils = require('utils-lib');
const axios = require('axios');


random_commit = async function(config, info) {
    let res_nonce = await utils.shell_cmd(`echo ${utils.randInt(Number.MAX_SAFE_INTEGER)} | sha256sum | cut -d" " -f1`);
    let nonce = `0x${res_nonce.stdout.trim().substring(0, 20)}`;
    let move = utils.randInt(8) + 1; // between 1 and 8
    let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/head//context/contracts/${info.contract_addr}/exec_fun/pack_choice`);
    let data = { expr: { dune_expr: { nat: `${move}` } }, gas: "2000" };
    let headers = { headers: { 'Content-Type': 'application/json' } };
    let res = await axios.post(req, JSON.stringify(data), headers);
    let xmove = res.data.dune_expr.bytes;
    let res_commit = await utils.shell_cmd(`echo ${xmove}${nonce}| xxd -r -p | sha256sum -t | cut -d" " -f1`);
    let commit = res_commit.stdout.trim().substring(0, 20);
    console.log(xmove);
    let elt = {
        nonce: nonce,
        choice: move,
        commit: commit
    };
    return (elt);
};


encode_choice = async function(config, info, e) {
    let nonce = e.nonce_.substring(2); // remove 0x

    let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/head/context/contracts/${info.contract_addr}/exec_fun/get_game_result`);
    let data = { expr: { dune_expr: { tuple: [{ int: `${e.id_}` }, { nat: `${e.choice_}` }, { bytes: nonce }] } }, gas: "800000" }
    let headers = { headers: { 'Content-Type': 'application/json' } };
    let res = await axios.post(req, JSON.stringify(data), headers);
    console.log(JSON.stringify(res.data));
    let r = res.data.dune_expr.tuple[1].record;
    let result = `{
         id = ${parseInt(r.id.int)};
         block_lvl = ${parseInt(r.block_lvl.nat)}p;
         player_addr = ${r.player_addr.addr};
         player_choice = ${parseInt(r.player_choice.nat)}p;
         oracle_choice = ${parseInt(r.oracle_choice.nat)}p;
         status = ${r.status.constr[0]};
         reward = ${parseInt(r.reward.nat)}p
    }`;
    return `(${result}, ${e.nonce_})`
}

let info = {
    random_commit: random_commit,
    encode_choice: encode_choice,
    manager: 'moonshot_with_server_manager',
    contract_name: 'moonshot_with_server',
    table: 'moonshot_with_server_commits'
};

contracts_monitor_lib.main(info);