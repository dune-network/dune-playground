#love
(* A player's choice is either hidden: hashed value obtained from his/her choice
and an arbitrary (secret) nonce, or a revealed choice that may have value 0
(Rock), 1 (Paper), or 2 (Scissors) *)
type move = Rock | Paper | Scissors

type result =
  | Ongoing
  | Won of move
  | Tie of move
  | Lost of move

type game_id = int

(* A player's state, made of the address and the choice made *)
type player = {
  addr : address;
  move : move;
  block_level : nat;
  prev_game : game_id;
}

(* The storage of the contract is made of players' states, if they already
played, a timestamp indicating the date of the latest storage, and the value
  of the current bet. *)
type game = {
  (* commitment. ie. blind choice, sha256 (move : bytes, nonce : bytes) *)
  oracle_choice : bytes;
  player : player option;
  result : result;
}

type games = (game_id, game) bigmap

type player_info = {
  last_game : game_id;
  nb_games : nat;
  nb_won : nat;
  nb_tie : nat
}

type players = (address, player_info) bigmap

type dgg = nat (* or dun *)

type params = {
  oracle_addr : address;
  tokens_addr : address;
  lock_player_x_blocks : nat;
  lock_reveal_x_blocks : nat;
  bet_amount : dun;
  winner_reward : dgg;
  looser_reward : dgg;
  tie_reward : dgg;
}

type storage = {
  games : games;
  players : players;
  last_revealed : game_id;
  last_played : game_id;
  last_commit : game_id;
  params : params
}

(* --- auxiliary functions --- *)

(* initial storage *)
val%init storage (params : params) = {
  games =  BigMap.empty [:game_id] [:game];
  players = BigMap.empty [:address] [:player_info] ;
  last_revealed = 0;
  last_played = 0;
  last_commit = 0;
  params;
}

val find_game (gid : game_id) (games : games) : game =
  match BigMap.find [:game_id] [:game] gid games with
  | None -> failwith [:string * game_id] ("No game found with id, ", gid) [:game]
  | Some g -> g

val check_is_oracle (oracle_addr : address) : unit =
  if Current.sender () <>[:address] oracle_addr then
    failwith [:string]
      "This entrypoint can only be called by the oracle addr"
      [:unit]

(* Check that Current.amount () is equal to the given amount. Fail otherwise *)
val check_is_amount (amnt : dun) : unit =
  if (Current.amount ()) <>[:dun] amnt then
   failwith [: string * dun * dun]
     ("Invalid amount. (expected, provided) = ", amnt, (Current.amount ())) [: unit]


contract type Token = sig
  val%entry transfer : (address * nat * bytes option)
end

(* Transfer the given amount to the given destination. Assumes a Unit contract
   for the destination *)

val make_transfer (tokens_addr: address) (dest : address) (amount : dgg) : operation =
  (* Account.transfer dest amount *)
  let tokens_opt = Contract.at<:Token> tokens_addr in
  match tokens_opt with
  | None ->
    failwith [:string * address] ("No Token contract found at ", tokens_addr)
    [:operation]

  | Some (contract TK : Token)->
    TK.transfer 0DUN (dest, amount, (None [:bytes]))


(* Check that sha256 (0x (move ^ nonce) = commit. Fails otherwise *)
val check_commitment (move : bytes) (nonce : bytes) (commit : bytes) : unit =
  let s = Bytes.concat ([move; nonce] [:bytes]) in
  let h = Crypto.sha256 s in
  if h <>[:bytes] commit then
   failwith [: string * bytes * bytes * bytes * bytes * bytes]
     ("revelation does not match commitment. (move, nonce, s, hash(s), com) =",
      move, nonce, s, h, commit) [:unit]

(* Decode the given choice. Returns 0 (Rock), 1 (Paper), or 2 (Scissors)
   depending on the value of the move. Fails if input is different from 0x01, 0x02
   and 0x03 *)
val decode_move (move : bytes) : move * int =
  if move =[:bytes] 0x00 then Rock, 0
  else if move =[:bytes] 0x01 then Paper, 1
  else if move =[:bytes] 0x02 then Scissors, 2
  else failwith [:string] "invalid move" [:move * int]

val get_player_info (addr : address) (players:players) : player_info =
   match BigMap.find [:address] [:player_info] addr players with
   | None -> { last_game = -1; nb_games = 0p; nb_won = 0p ; nb_tie = 0p }
   | Some pinfo -> pinfo


val reveal_commit
    ((choice, nonce) : (bytes * bytes))
    (ops : operation list) (storage : storage) : operation list * storage =
  let last_revealed = storage.last_revealed + 1 in
  let game = find_game last_revealed storage.games in
  let params = storage.params in
  match game.player with
  | None ->
    failwith [:string]
      "[Oracle failure] shouldn't reveal until a player makes a choice"
      [:operation list * storage]

  | Some p ->
    let min_reveal_block = p.block_level ++ params.lock_reveal_x_blocks in
    let level = Current.level () in
    if min_reveal_block > [:nat] level then
      failwith
        [:string * nat * nat]
        ("[Oracle failure] Cannot reveal yet! (current block, min reveal block) =",
         level, min_reveal_block)
        [:unit];
    check_commitment choice nonce game.oracle_choice;
    let oracle_mv, oracle_move = decode_move choice in
    let player_move = match p.move with
      | Rock -> 0
      | Paper -> 1
      | Scissors -> 2
    in
    let pinfo = get_player_info p.addr storage.players in
    let diff = oracle_move - player_move in
    let amount, result, pinfo =
      if diff = 0 then
        params.tie_reward,
        Tie oracle_mv,
        { pinfo with nb_tie = pinfo.nb_tie ++ 1p }
      else
      if diff = 1 || diff = -2 then
        params.looser_reward,
        Lost oracle_mv,
        pinfo
      else
        params.winner_reward,
        Won oracle_mv,
        { pinfo with nb_won = pinfo.nb_won ++ 1p }
    in
    let ops =
      if amount =[:dgg] 0p then ops
      else (make_transfer params.tokens_addr p.addr amount) :: ops
    in
    let players =
      BigMap.add [:address] [:player_info] p.addr pinfo storage.players in
    let game = { game with result } in
    let games = BigMap.add [:game_id] [:game] last_revealed game storage.games in
    ops,
    { storage with last_revealed; players; games }


val make_play (addr : address) (last_game : int) (move : move) (storage:storage) : player =
  let block_level = Current.level () in
  let prev_game =
    if last_game < 0 then last_game
    else
      begin
        let og = find_game last_game storage.games in
        let old_lvl = match og.player with
          | None -> failwith [:string] "Invariant: not possible" [:nat]
          | Some p -> p.block_level
        in
        let min_play_again = storage.params.lock_player_x_blocks in
        if old_lvl ++ min_play_again >[:nat] block_level then
          failwith
            [:string * nat * nat]
            ("Cannot play again yet. Wait! (current block, min reveal block) =",
             block_level, min_play_again)
            [:unit];
        last_game
      end
  in
  { addr; move; block_level; prev_game }

(* --- entrypoints --- *)

val%entry play storage d (move : move) =
  let params = storage.params in
  check_is_amount params.bet_amount;
  let last_played = storage.last_played + 1 in
  if last_played >[:int] storage.last_commit then
    failwith [:string]
      "No open games for the moment. Wait until the oracle inserts some" [:unit];
  let addr = Current.sender () in
  let pinfo = get_player_info addr storage.players in
  let play = make_play addr pinfo.last_game move storage in
  let block_level = Current.level () in
  let game = find_game last_played storage.games in
  begin
    match game.player with
    | None -> ()
    | Some _ ->
      failwith [:string] "[Invariant] no move from the player yet" [:unit];
  end;
  let game = { game with player = Some play [:player] } in
  let games = BigMap.add [:game_id] [:game] last_played game storage.games in
  let pinfo = {pinfo with nb_games = pinfo.nb_games ++ 1p; last_game = last_played} in
  let players = BigMap.add [:address] [:player_info] addr pinfo storage.players in
  [] [:operation], { storage with last_played; games; players }


val%entry insert_commits storage d ((last_commit, commits) : int  * bytes list) =
  check_is_oracle storage.params.oracle_addr;
  if last_commit <>[:int] storage.last_commit then
    failwith [:string * int * int]
      ("bad provided last_commit. (expected, provided) = ", storage.last_commit, last_commit)
      [:unit];
  let games, last_commit =
    List.fold [:bytes] [:games * int]
      begin
        fun (commit : bytes) ((games, last_commit) : games * int) ->
          let g = {
            oracle_choice = commit;
            player = None [:player];
            result = Ongoing
          }
          in
          let last_commit = last_commit + 1 in
          BigMap.add [:game_id] [:game] last_commit g games, last_commit
      end
      commits
      (storage.games, storage.last_commit)
  in
  [] [:operation], { storage with games; last_commit }

val%entry reveal_commits storage d (revelations : ((bytes * bytes) list)) =
  check_is_oracle storage.params.oracle_addr;
  List.fold [:bytes * bytes] [:operation list * storage]
    (fun (reveal : (bytes * bytes)) ((ops, storage) : (operation list * storage)) ->
       reveal_commit reveal ops storage
    ) revelations ([] [:operation], storage)
