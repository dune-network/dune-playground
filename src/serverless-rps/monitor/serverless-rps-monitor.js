const contracts_monitor_lib = require('contracts-monitor-lib');
const utils = require('utils-lib');

random_commit = async function(config, info) {
    let res_nonce = await utils.shell_cmd(`echo ${utils.randInt(Number.MAX_SAFE_INTEGER)} | sha256sum | cut -d" " -f1`);
    let nonce = `0x${res_nonce.stdout.trim()}`;
    let choice = '0x0' + utils.randInt(3);
    let res_commit = await utils.shell_cmd(`echo ${choice}${nonce}| xxd -r -p | sha256sum -t | cut -d" " -f1`);
    let commit = res_commit.stdout.trim();
    let elt = {
        nonce: nonce,
        choice: choice,
        commit: commit
    };
    return (elt);
};

encode_choice = async function(config, info, e) {
    return `(${e.choice_}, ${e.nonce_})`
}

let info = {
    random_commit: random_commit,
    encode_choice: encode_choice,
    manager: 'serverless_rps_manager',
    contract_name: 'serverless_rps',
    table: 'serverless_rps_commits'
};

contracts_monitor_lib.main(info);