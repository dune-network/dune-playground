/* global parameters */
/* global nodeRPC */
/*  global metal, utils */

var rpsModel = {

    state: {
        keyHash: null,
        params: null,

        global_last_revealed: -1,
        global_last_played: -1,
        global_last_commit: -1,

        user_nb_games: 0,
        user_nb_won: 0,
        user_nb_tie: 0,
        user_success: 100,
        user_last_game: -1,

        old_history: [],
        new_history: [],
        current_history_page: 1,

        last_move_hash: null,
        last_move_position: null,
        last_move_value: null,
        inited: false,
        revealed: true, // we assume it's revealed unless we got info of the opposite

        dun_balance: null,
        dgg_balance: null,

        games_bigmap: null,
        players_bigmap: null
    },

    __playersMapID: function() {
        return new Promise(
            function(resolve, reject) {
                if (rpsModel.state.players_bigmap !== null) {
                    resolve(rpsModel.state.players_bigmap);
                } else {
                    nodeRpcHelpers.bigMapID(parameters.serverless_rps, e => e.players).then(function(res) {
                        rpsModel.state.players_bigmap = res;
                        resolve(rpsModel.state.players_bigmap);
                    }, function(err) { reject(utils.networkError(err)) });
                }
            });
    },


    __gamesMapID: function() {
        return new Promise(
            function(resolve, reject) {
                if (rpsModel.state.games_bigmap !== null) {
                    resolve(rpsModel.state.games_bigmap);
                } else {
                    nodeRpcHelpers.bigMapID(parameters.serverless_rps, e => e.games).then(function(res) {
                        rpsModel.state.games_bigmap = res;
                        resolve(rpsModel.state.games_bigmap);
                    }, function(err) { reject(utils.networkError(err)) });
                }
            });
    },

    __playersMap: function(keyHash) {
        return JSON.stringify({ dune_expr: { address: keyHash } });
    },

    __gamesMap: function(gid) {
        return JSON.stringify({ dune_expr: { int: gid.toString() } });
    },

    __tokenMap: function(keyHash) {
        return JSON.stringify({ dune_expr: { address: keyHash } });
    },

    __initParams: function(reject, pars) {
        let p = pars.record;
        return {
            oracle_addr: p.oracle_addr.address,
            lock_player_x_blocks: parseInt(p.lock_player_x_blocks.nat),
            lock_reveal_x_blocks: parseInt(p.lock_reveal_x_blocks.nat),
            bet_amount: parseInt(p.bet_amount.dun),
            winner_reward: parseInt(p.winner_reward.nat),
            looser_reward: parseInt(p.looser_reward.nat),
            tie_reward: parseInt(p.tie_reward.nat)
        };
    },

    __storageState: function() {
        return new Promise(
            function(resolve, reject) {
                nodeRPC.storage(parameters.serverless_rps, parameters.node).then(function(res) {
                    let storage = res.response.dune_expr.record;
                    if (rpsModel.state.params === null) {
                        pars = rpsModel.__initParams(reject, storage.params);
                    } else {
                        pars = rpsModel.state.params;
                    }
                    resolve({
                        global_last_revealed: storage.last_revealed.int,
                        global_last_played: storage.last_played.int,
                        global_last_commit: storage.last_commit.int,
                        params: pars
                    });
                }, function(err) { reject(utils.networkError(err)) });
            }
        );
    },

    __gameState: function(game_id) {
        return new Promise(
            function(resolve, reject) {
                if (game_id < 0) {
                    reject("game_id is negative");
                } else {
                    rpsModel.__gamesMapID().then(function(games_bigmap_id) {
                        nodeRPC.bigmapGet(
                            rpsModel.__gamesMap(game_id),
                            games_bigmap_id, parameters.node).then(function(r) {
                            if (r.response === null) {
                                reject("failed to retrieve game_id ' + game_id negative");
                            } else {

                                let st = r.response.dune_expr.record;
                                let p = st.player.constr[1][0].record;

                                let p_move = p.move.constr[0];
                                let block = parseInt(p.block_level.nat);
                                let prev_game = parseInt(p.prev_game.int);
                                let result = st.result.constr;
                                status = result[0];
                                if (status !== "Ongoing") {
                                    oracle_move = result[1][0].constr[0];
                                } else {
                                    oracle_move = "Not revealed yet";
                                }
                                switch (status) {
                                    case "Won":
                                        reward = rpsModel.state.params.winner_reward;
                                        break;
                                    case "Lost":
                                        reward = rpsModel.state.params.looser_reward;
                                        break;
                                    case "Tie":
                                        reward = rpsModel.state.params.tie_reward;
                                        break;
                                    default:
                                        reward = "?";
                                        break;
                                }
                                let g = {
                                    game_id: game_id,
                                    player_move: p_move,
                                    oracle_move: oracle_move,
                                    game_status: status,
                                    game_block: block,
                                    prev_game: prev_game,
                                    reward: reward
                                };
                                resolve(g);
                            }
                        });
                    });
                }
            }
        );
    },

    __playerStats: function(keyHash) {
        return new Promise(
            function(resolve, reject) {
                rpsModel.__playersMapID().then(function(players_bigmap_id) {
                    nodeRPC.bigmapGet(
                        rpsModel.__playersMap(rpsModel.state.keyHash),
                        players_bigmap_id, parameters.node).then(function(r) {
                        if (r.response === null) {
                            resolve({
                                user_last_game: -1,
                                user_nb_games: 0,
                                user_nb_won: 0,
                                user_nb_tie: 0
                            });
                        } else {
                            let st = r.response.dune_expr.record;
                            resolve({
                                user_last_game: parseInt((st.last_game.int)),
                                user_nb_games: parseInt((st.nb_games.nat)),
                                user_nb_won: parseInt((st.nb_won.nat)),
                                user_nb_tie: parseInt((st.nb_tie.nat))
                            });
                        }
                    }, function(err) { reject(utils.networkError(err)) });
                }, function(err) { reject(utils.networkError(err)) });
            }
        );
    },

    __statusOfRecentChoice: function() {
        return new Promise(
            function(resolve, reject) {
                // look in mempool, otherwise, OK ? also look in head ?
                nodeRpcHelpers.trackOperation(parameters.node, rpsModel.state.last_move_hash,
                    rpsModel.state.keyHash, parameters.serverless_rps,
                    rpsModel.state.params.lock_player_x_blocks + 1).then(function(last_op) {
                    rpsModel.state.last_move_position = last_op.position;
                    if (rpsModel.state.last_move_hash === null) {
                        rpsModel.state.last_move_hash = last_op.hash;
                    }
                    rpsModel.state.last_move_value = last_op.tx.parameters.value.dune_expr.constr[0];
                    resolve();
                }, function(err) {
                    rpsModel.state.last_move_hash = null;
                    rpsModel.state.last_move_position = null;
                    rpsModel.state.last_move_value = null;
                    resolve();
                });
            });
    },

    refreshModel: function() {
        return new Promise(
            function(resolve, reject) {
                rpsModel.__storageState().then(function(s_state) {
                    rpsModel.state.global_last_revealed = s_state.global_last_revealed;
                    rpsModel.state.global_last_played = s_state.global_last_played;
                    rpsModel.state.global_last_commit = s_state.global_last_commit;
                    rpsModel.state.params = s_state.params;
                    if (typeof metal === 'undefined' || metal === undefined) {
                        reject(utils.metalNotDetected);
                    } else {
                        metal.isEnabled(function(res) {
                            if (!res) {
                                reject(utils.metalNotConfigured);
                            } else {
                                metal.getAccount(function(keyHash) {
                                    rpsModel.state.keyHash = keyHash;
                                    rpsModel.state.dun_balance = null;
                                    nodeRPC.manager_key(rpsModel.state.keyHash, parameters.node).then(function(mk) {
                                        rpsModel.state.revealed = (mk.response !== null);
                                        nodeRPC.balance(keyHash, parameters.node).then(function(dunBal) {
                                            if (dunBal.success) {
                                                rpsModel.state.dun_balance = parseInt(dunBal.response) / 1000000;
                                            }
                                            rpsModel.state.dgg_balance = null;
                                            dggModel.__accountsMapID().then(function(accounts_bigmap_id) {
                                                nodeRPC.bigmapGet(
                                                    rpsModel.__tokenMap(rpsModel.state.keyHash),
                                                    accounts_bigmap_id, parameters.node).then(function(dggBal) {
                                                    if (dggBal.success) {
                                                        if (dggBal.response === null) {
                                                            rpsModel.state.dgg_balance = 0; // user not in token bigmap yet ?
                                                        } else {
                                                            rpsModel.state.dgg_balance = parseInt(dggBal.response.dune_expr.nat);
                                                        }
                                                    }
                                                    rpsModel.__playerStats(rpsModel.state.keyHash).then(function(st) {
                                                        rpsModel.state.user_last_game = st.user_last_game;
                                                        rpsModel.state.user_nb_games = st.user_nb_games;
                                                        rpsModel.state.user_nb_won = st.user_nb_won;
                                                        rpsModel.state.user_nb_tie = st.user_nb_tie;
                                                        let success =
                                                            (rpsModel.state.user_nb_games === 0) ?
                                                            100 :
                                                            (rpsModel.state.user_nb_won * 100 / rpsModel.state.user_nb_games);
                                                        rpsModel.state.user_success = success.toFixed(2);
                                                        rpsModel.__statusOfRecentChoice().then(
                                                            function() {
                                                                if (rpsModel.state.user_last_game < 0) {
                                                                    rpsModel.state.inited = true;
                                                                    resolve(rpsModel.state);
                                                                } else {
                                                                    rpsModel.__gameState(rpsModel.state.user_last_game).then(function(r) {
                                                                        rpsModel.__rememberNewGame(r).then(function(_) {
                                                                            rpsModel.state.inited = true;
                                                                            resolve(JSON.parse(JSON.stringify(rpsModel.state)));
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                    });
                                                });
                                            }, function(err) { reject(utils.networkError(err)) });

                                        });
                                    }, function(err) { reject(utils.networkError(err)) });
                                }, function() {
                                    reject(utils.metalGetAccountFailed);
                                });
                            }
                        });
                    }
                }, function(err) { reject(utils.networkError(err)) });
            }
        );
    },

    playChoice: function(choice) {
        return new Promise(
            function(resolve, reject) {
                if (rpsModel.state.last_move_position !== null &&
                    rpsModel.state.last_move_position < rpsModel.state.params.lock_player_x_blocks) {
                    reject('You cannot play again. Should wait');
                } else {
                    if (typeof metal === 'undefined' || metal === undefined) {
                        reject(utils.metalNotDetected);
                    } else {
                        let ok = false;
                        metal.isEnabled(function(res) {
                            if (!res) {
                                reject(utils.metalNotConfigured);
                            }
                            metal.getAccount(function(r) {
                                let op = {
                                    dst: parameters.serverless_rps,
                                    amount: '0',
                                    network: parameters.network,
                                    entrypoint: 'play',
                                    parameter: '#love:' + choice,
                                    collect_call: false,
                                    gas_limit: '115000',
                                    storage_limit: '240',
                                    cb: function(res) {
                                        if (res.ok) {
                                            rpsModel.state.last_move_hash = res.msg;
                                            rpsModel.state.last_move_position = -1; // mempool
                                            rpsModel.state.last_move_value = choice;
                                        }
                                        resolve(res);
                                    }
                                };
                                metal.send(op);
                            });
                        });
                    }
                }
            }
        );
    },

    //

    __loadNewGameList: function(env) {
        return new Promise(
            function(resolve, _reject) {
                if (env.stop.game_id === env.current.game_id) {
                    resolve(env);
                } else {
                    env.delta[env.next_cell] = env.current;
                    env.next_cell = env.next_cell + 1;
                    if (env.current.game_id < 0 || env.current.prev_game === env.stop.game_id) {
                        resolve(env);
                    } else {
                        let id = env.current.prev_game;
                        rpsModel.__gameState(id).then(function(gm_id) {
                            env.current = gm_id;
                            rpsModel.__loadNewGameList(env).then(function(x) {
                                resolve(x);
                            });
                        });
                    }
                }
            });
    },

    __rememberNewGame: function(gm) {
        return new Promise(
            function(resolve, _reject) {
                let len = rpsModel.state.new_history.length;
                if (len === 0) {
                    rpsModel.state.new_history[0] = gm;
                    resolve(0);
                } else {
                    let stop = rpsModel.state.new_history[len - 1];
                    if (stop.game_id === gm.game_id) {
                        resolve(2);
                    } else {
                        let env0 = {
                            stop: stop,
                            delta: [],
                            next_cell: 0,
                            current: gm
                        };
                        rpsModel.__loadNewGameList(env0).then(function(env) {
                            let dlen = env.delta.length;
                            for (var i = dlen - 1; i >= 0; i--) {
                                rpsModel.state.new_history[len] = env.delta[i];
                                len++;
                            }
                            resolve(1);
                        });
                    }
                }
            });
    },

    __loadOldGameList: function(env) {
        return new Promise(
            function(resolve, _reject) {
                if (env.todo <= 0 || env.next_game_to_load < 0) {
                    resolve(env);
                } else {
                    rpsModel.__gameState(env.next_game_to_load).then(function(gm) {
                        env.next_game_to_load = gm.prev_game;
                        env.todo = env.todo - 1;
                        env.delta[env.next_cell] = gm;
                        env.next_cell = env.next_cell + 1;
                        rpsModel.__loadOldGameList(env).then(function(x) {
                            resolve(x);
                        });
                    });
                }
            });
    },

    __rememberOldGames: function(nb_to_load, start_id) {
        return new Promise(
            function(resolve, reject) {
                let env0 = {
                    next_game_to_load: start_id,
                    todo: nb_to_load,
                    delta: rpsModel.state.old_history,
                    next_cell: rpsModel.state.old_history.length
                };
                rpsModel.__loadOldGameList(env0).then(function(env) {
                    rpsModel.state.old_history = env.delta;
                    resolve(1);
                });
            });
    },

    __loadOldHistoryIfNeeded: function(min_item) {
        return new Promise(
            function(resolve, reject) {
                let old_len = rpsModel.state.old_history.length;
                let new_len = rpsModel.state.new_history.length;
                if (min_item + 10 < old_len) {
                    resolve(1);
                } else {
                    start_id = -1; /*if failed = -1*/
                    if (old_len > 0) {
                        start_id = rpsModel.state.old_history[old_len - 1].prev_game;
                    } else if (new_len > 0) {
                        start_id = rpsModel.state.new_history[0].prev_game;
                    }
                    rpsModel.__rememberOldGames(min_item + 10 + 10, start_id).then(function(_) {
                        resolve(1);
                    });
                }
            });
    },

    __extractFromNewHistory: function(start, end, pos_in_new) {
        var acc = [];
        let aux = function(start) {
            return new Promise(
                function(resolve, reject) {
                    if (start > end) {
                        resolve(start);
                    } else {
                        let g = rpsModel.state.new_history[pos_in_new - start];
                        if (g === undefined || g === null) {
                            resolve(start);
                        } else {
                            if (g.game_status === "Ongoing") {
                                rpsModel.__gameState(g.game_id).then(function(gm) {
                                    rpsModel.state.new_history[pos_in_new - start] = gm;
                                    acc.push(gm);
                                    aux(start + 1).then(
                                        function(start) {
                                            resolve(start);
                                        });
                                });
                            } else {
                                acc.push(g);
                                aux(start + 1).then(
                                    function(start) {
                                        resolve(start);
                                    });
                            }
                        }
                    }
                });
        };
        return new Promise(
            function(resolve, reject) {
                aux(start).then(function(_start) {
                    resolve(acc);
                });
            });
    },

    __extractFromOldHistory: function(min_item) {
        var acc = [];
        let aux = function(i) {
            return new Promise(
                function(resolve, reject) {
                    if (i >= 10) {
                        resolve();
                    } else {
                        let g = rpsModel.state.old_history[min_item + i];
                        if (g === undefined || g === null) {
                            resolve();
                        } else {
                            if (g.game_status === "Ongoing") {
                                rpsModel.__gameState(g.game_id).then(function(gm) {
                                    rpsModel.state.old_history[min_item + i] = gm;
                                    acc.push(gm);
                                    aux(i + 1).then(
                                        function() {
                                            resolve();
                                        });
                                });
                            } else {
                                acc.push(g);
                                aux(i + 1).then(
                                    function() {
                                        resolve();
                                    });
                            }
                        }
                    }
                });
        };
        return new Promise(
            function(resolve, reject) {
                rpsModel.__loadOldHistoryIfNeeded(min_item).then(function(_) {
                    aux(0).then(function() {
                        resolve(acc);
                    });

                });
            });
    },

    getHistoryPage: function(hpage) {
        return new Promise(
            function(resolve, reject) {
                if (hpage !== undefined) {
                    rpsModel.state.current_history_page = hpage;
                }
                let max_item = rpsModel.state.current_history_page * 10;
                let min_item = max_item - 10 + 1;
                let len_new = rpsModel.state.new_history.length;
                if (len_new >= max_item) {
                    rpsModel.__extractFromNewHistory(0, 10 - 1, rpsModel.state.new_history.length - min_item).then(function(res) {
                        resolve(res);
                    });
                } else if (len_new < min_item) {
                    rpsModel.__extractFromOldHistory(min_item - len_new - 1).then(function(res) {
                        resolve(res);
                    });
                } else {
                    rpsModel.__extractFromNewHistory(0, 10 - 1, rpsModel.state.new_history.length - min_item, []).then(function(from_new) {
                        rpsModel.__extractFromOldHistory(0).then(function(res) {
                            len = from_new.length;
                            for (i = len; i < len + res.length; i++) {
                                from_new[i] = res[i - len];
                            }
                            resolve(from_new);
                        });
                    });
                }
            });
    },

    nbHistoryPages: function() {
        return Math.ceil(rpsModel.state.user_nb_games / 10);
    }


};
