var rocket_config = {
    targets: 'img#rocket',
    translateY: {
        value: -100,
        duration: 6000,
        easing: 'easeInOutQuart'
    },
    rotate: {
        value: 186,
        duration: 6000,
        delay: 8000
    },
    translateX: {
        value: -10,
        duration: 6000,
        delay: 8000
    },
    autoplay: false
}

var line_config = {
    targets: 'img#line',
    translateY: {
        value: 100,
        duration: 4000,
        easing: 'easeInOutQuart'
    },
    autoplay: false
}

var moon_config = {
    targets: 'img#moon',
    translateX: {
        value: 35,
        duration: 10000,
        delay: 200,
        easing: 'easeInOutQuart'
    },
    translateY: {
        value: -67,
        duration: 10000,
        delay: 200,
        easing: 'easeInOutQuart'
    },
    scale: {
        value: 20,
        duration: 10000,
        delay: 200,
        easing: 'easeInOutQuart'
    },
    autoplay: false,
}

var background_config = {
    targets: 'img#background',
    translateY: {
        value: 3300,
        duration: 10000,
        delay: 200,
        easing: 'easeInOutQuart'
    },
    scale: {
        value: 30,
        duration: 10000,
        delay: 200,
        easing: 'easeInOutQuart'
    },
    autoplay: false,
}

var parachute_config = {
    targets: 'img#parachute',
    translateY: {
        value: 200,
        duration: 1500,
        easing: 'easeInQuart',
        autoplay: false
    },
}

function parachute_animation() {
    console.log("Leaving the rocket !");
    var parachute = document.createElement("img");
    parachute.src = "/images/parachute.png";
    parachute.id = "parachute";
    var anim = document.getElementById("animation");
    anim.appendChild(parachute);
    anime(parachute_config).play();

}

function updateKm(dist) {
    var x = 0;
    setTimeout(
        function() {
            var interval =
                setInterval(
                    function() {
                        x += 1;
                        document.getElementById('distance').textContent = 120 * x.toPrecision(1);
                    }, 0.1
                );
            setTimeout(function() { clearInterval(interval) }, dist * 1000 + 300);
        }, 1000);
}

var mutex_animation = false

function moonshot_animation(guess, altitude) {
    var guess_num = parseInt(guess);
    var altitude_num = parseInt(altitude);
    if (!(mutex_animation || isNaN(guess_num) || isNaN(altitude_num))) {
        mutex_animation = true;
        var old_html = document.getElementById("animation").innerHTML;
        var r_animation = anime(rocket_config);
        var l_animation = anime(line_config);
        var m_animation = anime(moon_config);
        var b_animation = anime(background_config);

        function play() {
            r_animation.play();
            l_animation.play();
            m_animation.play();
            b_animation.play();
        };

        function pause() {
            r_animation.pause();
            l_animation.pause();
            m_animation.pause();
            b_animation.pause();
        };

        console.log("Starting animation with ", guess, " and ", altitude);
        play();
        updateKm(altitude_num);
        if (guess_num <= altitude_num && guess_num != 8) {
            setTimeout(parachute_animation, (guess_num + 1) * 1000);
        };

        setTimeout(function() {
            if (altitude_num == 8) {
                setTimeout(
                    function() {
                        console.log('The rocket reached the moon!')
                    }, 3000)
            } else {
                pause();
                var rocket = document.getElementById('rocket');
                rocket.setAttribute('src', '/images/explosion.png');
            }
        }, (altitude_num + 1) * 1000);

        setTimeout(function() {
            document.getElementById("animation").innerHTML = old_html;
        }, (altitude_num + 3) * 1000);
    } else {
        if (altitude == "Not revealed yet") {
            mutex_animation = false;
        }
    }
};
