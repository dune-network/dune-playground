/* global parameters */
/* global nodeRPC */
/*  global metal, utils */

var moonshotModel = {

    state: {
        sliderFactor: 1,
        sliderValue: 1,

        keyHash: null,
        params: null,

        global_last_revealed: -1,
        global_last_played: -1,
        global_last_commit: -1,

        user_nb_games: 0,
        user_nb_won: 0,
        user_nb_tie: 0,
        user_success: 100,
        user_last_game: -1,

        old_history: [],
        new_history: [],
        current_history_page: 1,

        last_move_hash: null,
        last_move_position: null,
        last_move_value: null,
        inited: false,
        revealed: true, // we assume it's revealed unless we got info of the opposite

        dun_balance: null,
        dgg_balance: null,

        games_bigmap: null,
        players_bigmap: null
    },

    updateSlider: function(delta) {
        return new Promise(
            function(resolve, reject) {
                var v = delta * moonshotModel.state.sliderFactor + moonshotModel.state.sliderValue;
                if (delta !== 1 && delta !== -1) {
                    reject();
                } else {
                    if (v > moonshotModel.state.params.max_move || v < moonshotModel.state.params.min_move) {
                        reject();
                    } else {
                        console.log(v);
                        moonshotModel.state.sliderValue = v;
                        resolve(moonshotModel.state);
                    }
                }
            });
    },

    __playersMapID: function() {
        return new Promise(
            function(resolve, reject) {
                if (moonshotModel.state.players_bigmap !== null) {
                    resolve(moonshotModel.state.players_bigmap);
                } else {
                    nodeRpcHelpers.bigMapID(parameters.serverless_moonshot, e => e.players).then(function(res) {
                        moonshotModel.state.players_bigmap = res;
                        resolve(moonshotModel.state.players_bigmap);
                    }, function(err) { reject(utils.networkError(err)) });
                }
            });
    },

    __gamesMapID: function() {
        return new Promise(
            function(resolve, reject) {
                if (moonshotModel.state.games_bigmap !== null) {
                    resolve(moonshotModel.state.games_bigmap);
                } else {
                    nodeRpcHelpers.bigMapID(parameters.serverless_moonshot, e => e.games).then(function(res) {
                        moonshotModel.state.games_bigmap = res;
                        resolve(moonshotModel.state.games_bigmap);
                    }, function(err) { reject(utils.networkError(err)) });
                }
            });
    },

    __playersMap: function(keyHash) {
        return JSON.stringify({ dune_expr: { address: keyHash } });
    },

    __gamesMap: function(gid) {
        return JSON.stringify({ dune_expr: { int: gid.toString() } });
    },

    __tokenMap: function(keyHash) {
        return JSON.stringify({ dune_expr: { address: keyHash } });
    },

    __initParams: function(reject, pars) {
        let p = pars.record;
        return {
            oracle_addr: p.oracle_addr.address,
            lock_player_x_blocks: parseInt(p.lock_player_x_blocks.nat),
            lock_reveal_x_blocks: parseInt(p.lock_reveal_x_blocks.nat),
            bet_amount: parseInt(p.bet_amount.dun),
            winner_reward_factor: parseInt(p.winner_reward_factor.nat),
            looser_reward: parseInt(p.looser_reward.nat),
            min_move: parseInt(p.min_move.nat),
            max_move: parseInt(p.max_move.nat)
        };
    },

    __storageState: function() {
        return new Promise(
            function(resolve, reject) {
                nodeRPC.storage(parameters.serverless_moonshot, parameters.node).then(function(res) {
                    let storage = res.response.dune_expr.record;
                    if (moonshotModel.state.params === null) {
                        pars = moonshotModel.__initParams(reject, storage.params);
                    } else {
                        pars = moonshotModel.state.params;
                    }
                    resolve({
                        global_last_revealed: storage.last_revealed.int,
                        global_last_played: storage.last_played.int,
                        global_last_commit: storage.last_commit.int,
                        params: pars
                    });
                }, function(err) { reject(utils.networkError(err)) });
            }
        );
    },

    __gameState: function(game_id) {
        return new Promise(
            function(resolve, reject) {
                if (game_id < 0) {
                    reject("game_id is negative");
                } else {
                    moonshotModel.__gamesMapID().then(function(games_bigmap_id) {
                        nodeRPC.bigmapGet(
                            moonshotModel.__gamesMap(game_id),
                            games_bigmap_id, parameters.node).then(function(r) {
                            if (r.response === null) {
                                reject("failed to retrieve game_id ' + game_id negative");
                            } else {

                                let st = r.response.dune_expr.record;
                                let p = st.player.constr[1][0].record;

                                let p_move = parseInt(p.move.nat);
                                let block = parseInt(p.block_level.nat);
                                let prev_game = parseInt(p.prev_game.int);
                                let result = st.result.constr;
                                status = result[0];
                                if (status !== "Ongoing") {
                                    oracle_move = parseInt(result[1][0].nat);
                                } else {
                                    oracle_move = "Not revealed yet";
                                }
                                switch (status) {
                                    case "Won":
                                        reward = moonshotModel.state.params.winner_reward_factor * p_move;
                                        break;
                                    case "Lost":
                                        reward = moonshotModel.state.params.looser_reward;
                                        break;
                                    case "Tie":
                                        reward = moonshotModel.state.params.tie_reward;
                                        break;
                                    default:
                                        reward = "?";
                                        break;
                                }
                                let g = {
                                    game_id: game_id,
                                    player_move: p_move,
                                    oracle_move: oracle_move,
                                    game_status: status,
                                    game_block: block,
                                    prev_game: prev_game,
                                    reward: reward
                                };
                                resolve(g);
                            }
                        });
                    }, function(err) { reject(utils.networkError(err)) });
                }
            }
        );
    },

    __playerStats: function(keyHash) {
        return new Promise(
            function(resolve, reject) {
                moonshotModel.__playersMapID().then(function(players_bigmap_id) {
                    nodeRPC.bigmapGet(
                        moonshotModel.__playersMap(moonshotModel.state.keyHash),
                        players_bigmap_id, parameters.node).then(function(r) {
                        if (r.response === null) {
                            resolve({
                                user_last_game: -1,
                                user_nb_games: 0,
                                user_nb_won: 0,
                                user_nb_tie: 0
                            });
                        } else {
                            let st = r.response.dune_expr.record;
                            resolve({
                                user_last_game: parseInt((st.last_game.int)),
                                user_nb_games: parseInt((st.nb_games.nat)),
                                user_nb_won: parseInt((st.nb_won.nat))
                            });
                        }
                    }, function(err) { reject(utils.networkError(err)) });
                }, function(err) { reject(utils.networkError(err)) });
            }
        );
    },

    __statusOfRecentChoice: function() {
        return new Promise(
            function(resolve, reject) {
                // look in mempool, otherwise, OK ? also look in head ?
                nodeRpcHelpers.trackOperation(parameters.node, moonshotModel.state.last_move_hash,
                    moonshotModel.state.keyHash, parameters.serverless_moonshot,
                    moonshotModel.state.params.lock_player_x_blocks + 1).then(function(last_op) {
                    moonshotModel.state.last_move_position = last_op.position;
                    if (moonshotModel.state.last_move_hash == null) {
                        moonshotModel.state.last_move_hash = last_op.hash;
                    }
                    moonshotModel.state.last_move_value = parseInt(last_op.tx.parameters.value.dune_expr.nat);
                    resolve();
                }, function(err) {
                    moonshotModel.state.last_move_hash = null;
                    moonshotModel.state.last_move_position = null;
                    moonshotModel.state.last_move_value = null;
                    resolve();
                });
            });
    },

    refreshModel: function() {
        return new Promise(
            function(resolve, reject) {
                moonshotModel.__storageState().then(function(s_state) {
                    moonshotModel.state.global_last_revealed = s_state.global_last_revealed;
                    moonshotModel.state.global_last_played = s_state.global_last_played;
                    moonshotModel.state.global_last_commit = s_state.global_last_commit;
                    moonshotModel.state.params = s_state.params;
                    if (typeof metal === 'undefined' || metal === undefined) {
                        reject(utils.metalNotDetected);
                    } else {
                        metal.isEnabled(function(res) {
                            if (!res) {
                                reject(utils.metalNotConfigured);
                            } else {
                                metal.getAccount(function(keyHash) {
                                    moonshotModel.state.keyHash = keyHash;
                                    moonshotModel.state.dun_balance = null;
                                    nodeRPC.manager_key(moonshotModel.state.keyHash, parameters.node).then(function(mk) {
                                        moonshotModel.state.revealed = (mk.response !== null);
                                        nodeRPC.balance(keyHash, parameters.node).then(function(dunBal) {
                                            if (dunBal.success) {
                                                moonshotModel.state.dun_balance = parseInt(dunBal.response) / 1000000;
                                            }
                                            moonshotModel.state.dgg_balance = null;
                                            dggModel.__accountsMapID().then(function(accounts_bigmap_id) {
                                                nodeRPC.bigmapGet(
                                                    moonshotModel.__tokenMap(moonshotModel.state.keyHash),
                                                    accounts_bigmap_id, parameters.node).then(function(dggBal) {
                                                    if (dggBal.success) {
                                                        if (dggBal.response === null) {
                                                            moonshotModel.state.dgg_balance = 0; // user not in token bigmap yet ?
                                                        } else {
                                                            moonshotModel.state.dgg_balance = parseInt(dggBal.response.dune_expr.nat);
                                                        }
                                                    }
                                                    moonshotModel.__playerStats(moonshotModel.state.keyHash).then(function(st) {
                                                        moonshotModel.state.user_last_game = st.user_last_game;
                                                        moonshotModel.state.user_nb_games = st.user_nb_games;
                                                        moonshotModel.state.user_nb_won = st.user_nb_won;
                                                        moonshotModel.state.user_nb_tie = st.user_nb_tie;
                                                        let success =
                                                            (moonshotModel.state.user_nb_games === 0) ?
                                                            100 :
                                                            (moonshotModel.state.user_nb_won * 100 / moonshotModel.state.user_nb_games);
                                                        moonshotModel.state.user_success = success.toFixed(2);
                                                        moonshotModel.__statusOfRecentChoice().then(function() {
                                                            if (moonshotModel.state.user_last_game < 0) {
                                                                moonshotModel.state.inited = true;
                                                                resolve(moonshotModel.state);
                                                            } else {
                                                                moonshotModel.__gameState(moonshotModel.state.user_last_game).then(function(r) {
                                                                    moonshotModel.__rememberNewGame(r).then(function(_) {
                                                                        moonshotModel.state.inited = true;
                                                                        resolve(JSON.parse(JSON.stringify(moonshotModel.state)));
                                                                    });
                                                                });
                                                            }
                                                        });
                                                    });
                                                });
                                            }, function(err) { reject(utils.networkError(err)) });
                                        });
                                    }, function(err) { reject(utils.networkError(err)) });
                                }, function() {
                                    reject(utils.metalGetAccountFailed);
                                });
                            }
                        });
                    }
                }, function(err) { reject(utils.networkError(err)) });
            }
        );
    },

    playChoice: function() {
        return new Promise(
            function(resolve, reject) {
                if (moonshotModel.state.last_move_position !== null &&
                    moonshotModel.state.last_move_position < moonshotModel.state.params.lock_player_x_blocks) {
                    reject('You cannot play again. Should wait');
                } else {
                    if (typeof metal === 'undefined' || metal === undefined) {
                        reject(utils.metalNotDetected);
                    } else {
                        let ok = false;
                        metal.isEnabled(function(res) {
                            if (!res) {
                                reject(utils.metalNotConfigured);
                            }
                            metal.getAccount(function(r) {
                                let op = {
                                    dst: parameters.serverless_moonshot,
                                    network: parameters.network,
                                    amount: '0',
                                    entrypoint: 'play',
                                    parameter: '#love:' + moonshotModel.state.sliderValue + 'p',
                                    cb: function(res) {
                                        if (res.ok) {
                                            moonshotModel.state.last_move_hash = res.msg;
                                            moonshotModel.state.last_move_position = -1; // mempool
                                            moonshotModel.state.last_move_value = moonshotModel.state.sliderValue;
                                        }
                                        resolve(res);
                                    }
                                };
                                metal.send(op);
                            });
                        });
                    }
                }
            }
        );
    },

    //

    __loadNewGameList: function(env) {
        return new Promise(
            function(resolve, _reject) {
                if (env.stop.game_id === env.current.game_id) {
                    resolve(env);
                } else {
                    env.delta[env.next_cell] = env.current;
                    env.next_cell = env.next_cell + 1;
                    if (env.current.game_id < 0 || env.current.prev_game === env.stop.game_id) {
                        resolve(env);
                    } else {
                        let id = env.current.prev_game;
                        moonshotModel.__gameState(id).then(function(gm_id) {
                            env.current = gm_id;
                            moonshotModel.__loadNewGameList(env).then(function(x) {
                                resolve(x);
                            });
                        });
                    }
                }
            });
    },

    __rememberNewGame: function(gm) {
        return new Promise(
            function(resolve, _reject) {
                let len = moonshotModel.state.new_history.length;
                if (len === 0) {
                    moonshotModel.state.new_history[0] = gm;
                    resolve(0);
                } else {
                    let stop = moonshotModel.state.new_history[len - 1];
                    if (stop.game_id === gm.game_id) {
                        resolve(2);
                    } else {
                        let env0 = {
                            stop: stop,
                            delta: [],
                            next_cell: 0,
                            current: gm
                        };
                        moonshotModel.__loadNewGameList(env0).then(function(env) {
                            let dlen = env.delta.length;
                            for (var i = dlen - 1; i >= 0; i--) {
                                moonshotModel.state.new_history[len] = env.delta[i];
                                len++;
                            }
                            resolve(1);
                        });
                    }
                }
            });
    },

    __loadOldGameList: function(env) {
        return new Promise(
            function(resolve, _reject) {
                if (env.todo <= 0 || env.next_game_to_load < 0) {
                    resolve(env);
                } else {
                    moonshotModel.__gameState(env.next_game_to_load).then(function(gm) {
                        env.next_game_to_load = gm.prev_game;
                        env.todo = env.todo - 1;
                        env.delta[env.next_cell] = gm;
                        env.next_cell = env.next_cell + 1;
                        moonshotModel.__loadOldGameList(env).then(function(x) {
                            resolve(x);
                        });
                    });
                }
            });
    },

    __rememberOldGames: function(nb_to_load, start_id) {
        return new Promise(
            function(resolve, reject) {
                let env0 = {
                    next_game_to_load: start_id,
                    todo: nb_to_load,
                    delta: moonshotModel.state.old_history,
                    next_cell: moonshotModel.state.old_history.length
                };
                moonshotModel.__loadOldGameList(env0).then(function(env) {
                    moonshotModel.state.old_history = env.delta;
                    resolve(1);
                });
            });
    },

    __loadOldHistoryIfNeeded: function(min_item) {
        return new Promise(
            function(resolve, reject) {
                let old_len = moonshotModel.state.old_history.length;
                let new_len = moonshotModel.state.new_history.length;
                if (min_item + 10 < old_len) {
                    resolve(1);
                } else {
                    start_id = -1; /*if failed = -1*/
                    if (old_len > 0) {
                        start_id = moonshotModel.state.old_history[old_len - 1].prev_game;
                    } else if (new_len > 0) {
                        start_id = moonshotModel.state.new_history[0].prev_game;
                    }
                    moonshotModel.__rememberOldGames(min_item + 10 + 10, start_id).then(function(_) {
                        resolve(1);
                    });
                }
            });
    },

    __extractFromNewHistory: function(start, end, pos_in_new) {
        var acc = [];
        let aux = function(start) {
            return new Promise(
                function(resolve, reject) {
                    if (start > end) {
                        resolve(start);
                    } else {
                        let g = moonshotModel.state.new_history[pos_in_new - start];
                        if (g === undefined || g === null) {
                            resolve(start);
                        } else {
                            if (g.game_status === "Ongoing") {
                                moonshotModel.__gameState(g.game_id).then(function(gm) {
                                    moonshotModel.state.new_history[pos_in_new - start] = gm;
                                    acc.push(gm);
                                    aux(start + 1).then(
                                        function(start) {
                                            resolve(start);
                                        });
                                });
                            } else {
                                acc.push(g);
                                aux(start + 1).then(
                                    function(start) {
                                        resolve(start);
                                    });
                            }
                        }
                    }
                });
        };
        return new Promise(
            function(resolve, reject) {
                aux(start).then(function(_start) {
                    resolve(acc);
                });
            });
    },

    __extractFromOldHistory: function(min_item) {
        var acc = [];
        let aux = function(i) {
            return new Promise(
                function(resolve, reject) {
                    if (i >= 10) {
                        resolve();
                    } else {
                        let g = moonshotModel.state.old_history[min_item + i];
                        if (g === undefined || g === null) {
                            resolve();
                        } else {
                            if (g.game_status === "Ongoing") {
                                moonshotModel.__gameState(g.game_id).then(function(gm) {
                                    moonshotModel.state.old_history[min_item + i] = gm;
                                    acc.push(gm);
                                    aux(i + 1).then(
                                        function() {
                                            resolve();
                                        });
                                });
                            } else {
                                acc.push(g);
                                aux(i + 1).then(
                                    function() {
                                        resolve();
                                    });
                            }
                        }
                    }
                });
        };
        return new Promise(
            function(resolve, reject) {
                moonshotModel.__loadOldHistoryIfNeeded(min_item).then(function(_) {
                    aux(0).then(function() {
                        resolve(acc);
                    });

                });
            });
    },

    getHistoryPage: function(hpage) {
        return new Promise(
            function(resolve, reject) {
                if (hpage !== undefined) {
                    moonshotModel.state.current_history_page = hpage;
                }
                let max_item = moonshotModel.state.current_history_page * 10;
                let min_item = max_item - 10 + 1;
                let len_new = moonshotModel.state.new_history.length;
                if (len_new >= max_item) {
                    moonshotModel.__extractFromNewHistory(0, 10 - 1, moonshotModel.state.new_history.length - min_item).then(function(res) {
                        resolve(res);
                    });
                } else if (len_new < min_item) {
                    moonshotModel.__extractFromOldHistory(min_item - len_new - 1).then(function(res) {
                        resolve(res);
                    });
                } else {
                    moonshotModel.__extractFromNewHistory(0, 10 - 1, moonshotModel.state.new_history.length - min_item, []).then(function(from_new) {
                        moonshotModel.__extractFromOldHistory(0).then(function(res) {
                            len = from_new.length;
                            for (i = len; i < len + res.length; i++) {
                                from_new[i] = res[i - len];
                            }
                            resolve(from_new);
                        });
                    });
                }
            });
    },

    nbHistoryPages: function() {
        return Math.ceil(moonshotModel.state.user_nb_games / 10);
    }

};
