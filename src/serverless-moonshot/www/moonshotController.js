/* global moonshotModel */
/* global moonshotView */

var moonshotController = {

    showPage: function(autoReload, hist_page) {
        let hpage = utils.historyPage(hist_page);
        utils.setWaitingMsg(utils.loadingPage, autoReload).then(function() {
            hpage = Math.max(1, hpage);
            moonshotModel.refreshModel().then(function(data) {
                var maxPages = moonshotModel.nbHistoryPages();
                maxPages = Number.isInteger(maxPages) ? Math.max(1, maxPages) : hpage;
                hpage = Math.min(hpage, maxPages);
                var pg = '?history=' + hpage;
                window.history.replaceState('state', pg, pg);
                moonshotModel.getHistoryPage(hpage).then(function(dt) {
                    moonshotView.updatePage(data, dt, hpage);
                    utils.unsetWaitingMsg(false /* not autoReload*/ ).then(function() {});
                }, utils.modelErrHandler).catch(utils.modelErrHandler);
            }).catch(utils.modelErrHandler);
        });
    },

    play: function() {
        moonshotModel.playChoice().then(function(res) {
                moonshotView.updatePlayedChoice(res).then(function() {
                    moonshotController.showPage(true);
                })
            },
            function(err) {
                console.log(err);
                utils.modelErrHandler(err);
            });
    },

    changeHistoryPage: function(delta) {
        let hpage = moonshotModel.state.current_history_page + delta; // +1 or -1
        if (hpage <= 0)
            return;
        moonshotController.showPage(false /* not autoReload*/ , hpage);
    },

    slide: function(delta) {
        moonshotModel.updateSlider(delta).then(function(data) {
            moonshotView.updateSlider(data);
        }).catch(function() {});
    }

};