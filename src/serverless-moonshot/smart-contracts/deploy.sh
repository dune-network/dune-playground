#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

$DUNE_PLAYGROUND_CLIENT_CMD originate contract serverless_moonshot \
                            transferring 0 from playground_manager \
                            running file:contract.lov.ml \
                            --init '#love:{ oracle_addr = '\${pkh:serverless_moonshot_manager}'; tokens_addr = '\${contract:dgg_token}'; lock_player_x_blocks = 1p; lock_reveal_x_blocks = 0p; bet_amount = 0dun; winner_reward_factor = 1p; looser_reward = 1p; min_move = 1p; max_move = 8p }' \
                            --burn-cap 10 --force
