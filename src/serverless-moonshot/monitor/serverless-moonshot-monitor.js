const contracts_monitor_lib = require('contracts-monitor-lib');
const utils = require('utils-lib');
const axios = require('axios');

random_commit = async function(config, info) {
    let res_nonce = await utils.shell_cmd(`echo ${utils.randInt(Number.MAX_SAFE_INTEGER)} | sha256sum | cut -d" " -f1`);
    let nonce = `0x${res_nonce.stdout.trim()}`;
    let move = utils.randInt(8) + 1; // between 1 and 8
    let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/head//context/contracts/${info.contract_addr}/exec_fun/pack_move`);
    let data = { expr: { dune_expr: { nat: `${move}` } }, gas: "2000" };
    let headers = { headers: { 'Content-Type': 'application/json' } };
    let res = await axios.post(req, JSON.stringify(data), headers);
    let xmove = res.data.dune_expr.bytes;
    let res_commit = await utils.shell_cmd(`echo ${xmove}${nonce}| xxd -r -p | sha256sum -t | cut -d" " -f1`);
    let commit = res_commit.stdout.trim();
    console.log(xmove);
    let elt = {
        nonce: nonce,
        choice: move,
        commit: commit
    };
    return (elt);
};

encode_choice = async function(config, info, e) {
    return `(${e.choice_}p, ${e.nonce_})`
}

let info = {
    random_commit: random_commit,
    encode_choice: encode_choice,
    manager: 'serverless_moonshot_manager',
    contract_name: 'serverless_moonshot',
    table: 'serverless_moonshot_commits'
};

contracts_monitor_lib.main(info);