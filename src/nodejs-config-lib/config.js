const fs = require('fs');
const minimist = require('minimist');
var path = require('path');

const parse = function() {
    var args = minimist(process.argv.slice(2), {
        string: ['config']
    });

    console.log('\ncommand line options:');
    console.log('  --config: ' + args.config + '\n');

    var config = JSON.parse(fs.readFileSync(args.config));

    if (config.config_file !== undefined) {
        p = path.dirname(args.config);
        config = JSON.parse(fs.readFileSync(`${p}/${config.config_file}`));
    }

    console.log("config:\n" + JSON.stringify(config) + "\n");

    if (config.duns_airdrop.blocking_airdrop_period <= config.operations_ttl) {
        console.log('warning: blocking_airdrop_period smaller than or equal to operations_ttl!');
        console.log('I will replace its value with operations_ttl + 1!');
        config.duns_airdrop.blocking_airdrop_period = config.operations_ttl + 1;
    }

    return config;
};

exports.parseArgs = parse;