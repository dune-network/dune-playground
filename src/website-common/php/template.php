<?php
//error_reporting(E_ALL);
error_reporting(0);

$js_version = 16;

if (!isset($page)) {
    $page = "home";
}

$active_item = [
    'home' => '',
    'free-duns' => 'free-duns',
    'dgg-token' => '',
    'rps+server' => '',
    'moonshot+server' => '',
    'serverless-rps' => '',
    'serverless-moonshot' => '',
    'dune-raffle' => '',
    'about' => ''
];

switch ($page) {
    case 'rps+server':
        $title = 'Rock Paper Scissors';
        $page = "dapps/rps+server/view.html";
        $jsFiles = array('dapps/dgg-token/dggModel.js', 'dapps/rps+server/rpsModel.js', 'dapps/rps+server/rpsView.js', 'dapps/rps+server/rpsController.js');
        $initDApp = "metalController.init_pgMetal(rpsController.showPage)";
        $active_item['rps+server'] = 'class="active-item"';
        break;

    case 'moonshot+server':
        $title = 'Dune Moonshot';
        $page = "dapps/moonshot+server/view.html";
        $jsFiles = array('dapps/dgg-token/dggModel.js', 'dapps/moonshot+server/moonshotAnimation.js', 'dapps/moonshot+server/moonshotModel.js', 'dapps/moonshot+server/moonshotView.js', 'dapps/moonshot+server/moonshotController.js');
        $initDApp = "metalController.init_pgMetal(moonshotController.showPage)";
        $active_item['moonshot+server'] = 'class="active-item"';
        break;

    case 'serverless-rps':
        $title = 'Rock Paper Scissors';
        $page = "dapps/rps/view.html";
        $jsFiles = array('dapps/dgg-token/dggModel.js', 'dapps/rps/rpsModel.js', 'dapps/rps/rpsView.js', 'dapps/rps/rpsController.js');
        $initDApp = "metalController.init_pgMetal(rpsController.showPage)";
        $active_item['serverless-rps'] = 'class="active-item"';
        break;

    case 'serverless-moonshot':
        $title = 'Dune Moonshot';
        $page = "dapps/moonshot/view.html";
        $jsFiles = array('dapps/dgg-token/dggModel.js', 'dapps/moonshot/moonshotAnimation.js', 'dapps/moonshot/moonshotModel.js', 'dapps/moonshot/moonshotView.js', 'dapps/moonshot/moonshotController.js');
        $initDApp = "metalController.init_pgMetal(moonshotController.showPage)";
        $active_item['serverless-moonshot'] = 'class="active-item"';
        break;

    case 'dune-raffle':
        $title = 'Dune Raffle';
        $page = 'dapps/dune-raffle/view.html';
        $jsFiles = array('dapps/dune-raffle/generated_code/javascript/dune_raffle_dgg.0/contract_dune_raffle_dgg.js', 'dapps/dgg-token/dggModel.js', 'dapps/dune-raffle/model.js', 'dapps/dune-raffle/view.js', 'dapps/dune-raffle/controller.js');
        $initDApp = 'metalController.init_pgMetal(controller.showPage)';
        $active_item['dune-raffle'] = 'class="active-item"';
        break;

    case 'dgg-token':
        $title = 'DGG Tokens';
        $page = 'dapps/dgg-token/view.html';
        $jsFiles = array('dapps/dgg-token/dggModel.js', 'dapps/dgg-token/dggView.js', 'dapps/dgg-token/dggController.js');
        $initDApp = "metalController.init_pgMetal(dggController.showPage)";
        $active_item['dgg-token'] = 'class="active-item"';
        break;
    case 'free-duns':
        $title = 'Get Free DUNs';
        $page = 'dapps/free-duns/view.html';
        $jsFiles = array('dapps/free-duns/free-duns-mvc.js');
        $initDApp = "metalController.init_pgMetal(freeDuns.updateDN1)";
        $active_item['free-duns'] = 'class="active-item"';
        break;
    case 'home':
        $title = "Home";
        $page = "dapps/fake/home-view.html";
        $jsFiles = array();
        $initDApp = 'metalController.init_pgMetal()';
        $active_item['home'] = 'class="active-item"';
        break;
    case 'about':
        $title = "About";
        $page = "dapps/fake/about-view.html";
        $jsFiles = array();
        $initDApp = 'metalController.init_pgMetal()';
        $active_item['about'] = 'class="active-item"';
        break;
    default:
        $title = "404 - Page Not Found";
        $page = "dapps/fake/404-view.html";
        $jsFiles = array();
        $initDApp = 'metalController.init_pgMetal()';
        break;
}

// safety check
if (!isset($title) || !isset($page) || !isset($jsFiles) || !isset($initDApp)) {
    header('Location: index.php');
    exit();
}
?>


<!DOCTYPE HTML>
<!--
        Editorial by HTML5 UP
        html5up.net | @ajlkn
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
    <title>Dune Playground | <?php echo $title ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/main.css" />
    <link rel="stylesheet" href="dapps/dapps.css" />
</head>

<body class="is-preload" onload="<?php echo $initDApp ?>">

    <!-- Wrapper -->
    <div id="wrapper">

        <div id="overlay-xxl">
            <div id="overlay-xxl-content">
                &nbsp;
            </div>
        </div>
        <!-- Main -->
        <div id="main">
            <div class="inner">
                <!-- Header -->
                <header id="header">
                    <h2 id="view-title"><?php echo $title ?></h2>
                </header>
                <div>
                    <?php require_once($page); ?>
                </div>
            </div>
            <!-- Footer -->
            <footer id="footer">
                <p class="copyright pp">
                    &copy; Dune Playground, developed by <a target="_blank" href="https://www.origin-labs.com">Origin Labs</a> for <a target="_blank" href="https://dune.network">Dune Network</a>.
                    <br />Design: <a href="https://html5up.net">HTML5 UP</a>.
                </p>
            </footer>
        </div>

        <!-- Sidebar -->
        <div id="sidebar">
            <div class="inner">

                <!-- Search -->
                <section id="logo" class="alt" onclick="document.location = '/index.php';">
                    <h1>Dune<br />Playground</h1>
                </section>

                 <!-- Account -->
                 <section id="account">
                    <header class="major">
                        <h2>Your Account</h2>
                    </header>
                    <div class="icons">
                        <p id='connecting-account'><img src=images/loader.gif style="width: 25%"/></p>
                        <p style="display:none" id='connect-metal' class=button onclick="metalController.connect(true)">Connect Metal</p>
                        <div id='accounts-info' style="display:none; font-family:monospace; font-size:95%">
                        <div id="account-blockies"></div>
                           <p>Your address is<br/>
                           <span style="font-weight:bold" id='user-dn1-address'>{user-dn1-address}</span>
                           </p>
                           <p>You own 
                           <span style="font-weight:bold" id='user-dun-balance'>{user-dun-balance}</span> DUN<br/>
                           and 
                           <span style="font-weight:bold" id='user-dgg-balance'>{user-dgg-balance}</span> DGG
                           </p>
                        </div>
                       </div>
                </section>

                <!-- Menu -->
                <nav id="menu">
                    <header class="major">
                        <h2>Menu</h2>
                    </header>
                    <ul>
                        <li <?php echo $active_item['home'] ?>><a href="index.php">Home</a></li>
                        <li <?php echo $active_item['free-duns'] ?>><a href="free-duns.php">Get Free $DUN</a></li>
                        <li <?php echo $active_item['dgg-token'] ?>><a href="dgg-token.php">DGG Tokens</a></li>
                        <li <?php echo $active_item['rps+server'] ?>><a href="rps+server.php">Play Rock Paper Scissors</a></li>
                        <li <?php echo $active_item['moonshot+server'] ?>><a href="moonshot+server.php">Play Dune Moonshot</a></li>
                        <li <?php echo $active_item['dune-raffle'] ?>><a href="dune-raffle.php">Play Dune Raffle</a></li>
                        <li <?php echo $active_item['about'] ?>><a href="about.php">About</a></li>
                    </ul>
                </nav>

                <!-- Search -->
                <section id="media-orl">
                    <header class="major">
                        <h2>Origin Labs</h2>
                    </header>
                    <ul class="icons">
                        <li><a target="_blank" href="https://www.origin-labs.com" class="icon solid fa-home"><span class="label">Website</span></a></li>
                        <li><a href="mailto:contact@origin-labs.com" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
                        <li><a target="_blank" href="https://twitter.com/LabsOrigin" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a target="_blank" href="https://gitlab.com/o-labs" class="icon brands fa-gitlab"><span class="label">GitLab</span></a></li>
                        <li><a target="_blank" href="https://medium.com/@originlabs" class="icon brands fa-medium-m"><span class="label">Medium</span></a></li>
                        <li><a target="_blank" href="https://fr.linkedin.com/company/labsorigin" class="icon brands fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    </ul>
                    <p>Building enterprise-level decentralized infrastructure and applications.</p>
                </section>
                <section id="media-dune">
                    <header class="major">
                        <h2>Dune Network</h2>
                    </header>
                    <ul class="icons">
                        <li><a target="_blank" href="https://dune.network" class="icon solid fa-home"><span class="label">Website</span></a></li>
                        <li><a href="mailto:contact@dune.network" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
                        <li><a target="_blank" href="https://twitter.com/dune_network" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a target="_blank" href="https://gitlab.com/dune-network" class="icon brands fa-gitlab"><span class="label">GitLab</span></a></li>
                        <li><a target="_blank" href="https://medium.com/dune-network" class="icon brands fa-medium-m"><span class="label">Medium</span></a></li>
                        <li><a target="_blank" href="https://www.linkedin.com/company/dune-network/" class="icon brands fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    </ul>
                    <ul class="icons">

                        <li><a target="_blank" href="https://www.reddit.com/r/dune_network/new/" class="icon brands fa-reddit"><span class="label">Reddit</span></a></li>
                        <li><a target="_blank" href="https://t.me/dune_network" class="icon brands fa-telegram"><span class="label">Telegram</span></a></li>
                        <li><a target="_blank" href="https://discord.gg/JBUGqFg" class="icon brands fa-discord"><span class="label">Discord</span></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/dune_network/" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
                    </ul>
                    <p>A secured Web 3.0 platform designed for enterprise grade decentralized application.</p>
                </section>
            </div>
        </div>

    </div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/anime.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>

    <!-- TODO: every DApp should import its own js in a separate
             page to avoid loading all the scripts here -->
    <script src="dapps/parameters.js?js_version=<?php echo $js_version ?>"></script>
    <script src="dapps/utils.js?js_version=<?php echo $js_version ?>"></script>
    <script src="dapps/nodeRPC.js?js_version=<?php echo $js_version ?>"></script>
    <script src="dapps/pgMetal.js?js_version=<?php echo $js_version ?>"></script>

    <?php
    foreach ($jsFiles as $index => $js) {
        echo "<script src=\"$js?js_version=$js_version\"></script>";
    }
    ?>

    <script src="dapps/captcha.js?js_version=<?php echo $js_version ?>"></script>
    <script>
        // g-recaptcha whant the callback to be at toplevel
        function captchaUpdated(response) {
            captcha.captchaUpdated(response);
        }
    </script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <div id="google-recaptcha" class="captcha-screen">
        <div class="captcha-container">
            <div class="g-recaptcha" data-sitekey="6LdLhvEUAAAAAJ1o_j2lHAMhEl_yumthYgyOLXpi" data-callback="captchaUpdated"></div>
        </div>
    </div>

</body>

</html>