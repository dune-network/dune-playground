var captcha = {

    g_recaptcha_action: null,

    show_captcha: function (callback) {
        captcha.g_recaptcha_action = callback;
        document.getElementById('google-recaptcha').style.display = 'block';
    },

    hide_captcha: function () {
        document.getElementById('google-recaptcha').style.display = 'none';
    },

    captchaUpdated: function (response) {
        captcha.hide_captcha();
        grecaptcha.reset();
        captcha.g_recaptcha_action(response);
    }
};
