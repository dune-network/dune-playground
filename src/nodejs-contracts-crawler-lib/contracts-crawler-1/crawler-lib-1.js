require('log-timestamp');
const Pool = require('pg').Pool;
const axios = require('axios').default;
const exec = require('child_process').exec;
const configLib = require('config-lib');
const utils = require('utils-lib');

/**
 * returns the entrypoint name and value of the given operation
 */
get_entrypoint_of_op = function(operation) {
    if (operation == null || operation.parameters == null || operation.parameters.entrypoint == null) {
        return { entrypoint: '' };
    }
    return { entrypoint: operation.parameters.entrypoint, value: operation.parameters.value };
};

/**
 * decodes the given value/param of "play" entrypoint, and add the information in env.played array
 */
parse_play = function(info, player, value, bl_hash, op_hash, op_level, failed, env) {
    let choice = info.decode_choice(value.dune_expr);
    env.played.push({
        // game_id : // not provided by this param
        op: op_hash,
        block: bl_hash,
        level: op_level,
        player: player,
        player_choice: choice,
        delegated: false,
        failed: failed
    });
};

/**
 * decodes the given value/param of "play_for" entrypoint, and add the information (a list of delegated plays)
 * in env.played array
 */
parse_play_for = function(info, value, bl_hash, op_hash, op_level, failed, env) {
    value.dune_expr.list.forEach(element => {
        let u = element.tuple;
        env.played.push({
            op: op_hash,
            block: bl_hash,
            level: op_level,
            player: u[0].addr,
            player_choice: info.decode_choice(u[1]),
            delegated: true,
            failed: failed
        });
    });
};

/**
 * decodes the given value/param of "reveals" entrypoint, and add the information (a list of revealed games)
 * in env.revealed array
 */
parse_reveals = function(info, value, bl_hash, op_hash, op_level, failed, env) {
    value.dune_expr.list.forEach(element => {
        let res = element.tuple[0].record;
        if (!failed) { // only insert non-failed reveals
            env.revealed.push({
                game_id: parseInt(res.id.int),
                op: op_hash,
                block: bl_hash,
                level: op_level,
                game_block_level: parseInt(res.block_lvl.nat),
                player: res.player_addr.addr,
                player_choice: info.decode_choice(res.player_choice),
                oracle_choice: info.decode_choice(res.oracle_choice),
                status: res.status.constr[0],
                reward: parseInt(res.reward.nat),
                failed: false
            });
        }
    });
};


/**
 * Process a transaction and all its internal transactions:
 * - detect if a fixpoint is reached and return
 * - otherwise, detect if there are ongoing transactions to desired smart contract with desired entrypoint
 * and update 'env' accordingly.
 */
process_a_multi_transaction = async function(info, pool, env, operation) {
    let op_hash = operation.hash;
    let bl_hash = operation.block_hash;
    let req = `SELECT EXISTS(SELECT FROM ${info.table} WHERE block_played_ = '${bl_hash}' OR block_revealed_ = '${bl_hash}' )`;
    //let req = `SELECT EXISTS(SELECT FROM ${info.table} WHERE block_played_ = '${bl_hash}')`;
    let res_exists = await utils.pg_query_promise(pool, req, []);
    if (res_exists.rows[0].exists) {
        env.fixpoint = true;
        return;
    } else {
        operation.type.operations.forEach(function(in_operation) {
            if (in_operation.destination.pkh !== info.contract_addr) {
                console.log('ignore outgoing transaction ...');
            } else {
                let op_level = in_operation.op_level;
                env.min_crawled_level = Math.min(env.min_crawled_level, op_level); // used to detect chain backtracks
                let failed = in_operation.failed;
                let param = get_entrypoint_of_op(in_operation);
                switch (param.entrypoint) {
                    case 'play':
                        console.log('play entrypoint: processing ...');
                        parse_play(info, in_operation.src.pkh, param.value, bl_hash, op_hash, op_level, failed, env);
                        break;
                    case 'play_for':
                        console.log('play_for entrypoint: processing ...');
                        parse_play_for(info, param.value, bl_hash, op_hash, op_level, failed, env);
                        break;
                    case 'insert_commits':
                        console.log('insert_commits entrypoint: ignoring');
                        break;
                    case 'reveal_commits':
                        console.log('reveal_commits entrypoint: processing ...');
                        parse_reveals(info, param.value, bl_hash, op_hash, op_level, failed, env);
                        break;
                    case 'deposit':
                        console.log('deposit entrypoint: ignoring ...');
                        break;
                    case 'withdraw':
                        console.log('withdraw entrypoint: ignoring ...');
                        break;
                    case '':
                        console.log('opereation or parameter null or undefined');
                        break;
                    default:
                        console.log(`Unknown entrypoint ${param.entrypoint}`);
                }
            }
        });
        return;
    }
};

/**
 * recursively crawle the desired smart contract transactions pages until reaching a fixpoint.
 */
crawler = async function(config, info, pool, env, page) {
    let req = utils.mk_request_url(config.explorer_api_info, `/v4/operations/${info.contract_addr}?type=Transaction&number=50&p=${page}`);
    let data = await axios.get(req);
    let result = data.data;
    let len = result.length;
    console.log(`[${info.contract_name}] processing ${len} operations of page ${page}`);
    if (len === 0) {
        return;
    }
    await result.reduce(async function(acc, operation) {
        await acc;
        if (env.fixpoint) {
            return (Promise.resolve())
        } else {
            return process_a_multi_transaction(info, pool, env, operation);
        }
    }, Promise.resolve());
    let nb_plays = env.played.length;
    let nb_reveals = env.revealed.length;
    console.log(`Got ${nb_plays} plays and ${nb_reveals} reveals in page ${page}`);
    if (env.fixpoint) {
        console.log('Fixpoint reached!');
        return;
    }
    await crawler(config, info, pool, env, page + 1);
    return;
};

/**
 * Detect if some latest blocks of have been invalidated due to chain re-organisation, and remove related information from DB.
 */
backtrack_if_needed = async function(info, pool, env, last_crawled_level) {
    if ((env.played.length === 0 && env.revealed.length === 0) ||
        env.min_crawled_level > last_crawled_level ||
        last_crawled_level === 0) {
        return; // no backtrack detected
    } else {
        console.log("[XXX] Chain backtrack detected! Should re-organize DB before saving new data");
        let req = `DELETE FROM ${info.table} WHERE level_played_ >= ${env.min_crawled_level}`;
        let res_del = await utils.pg_query_promise(pool, req, []);
        console.log(res_del);
        let req0 = `UPDATE ${info.table} `;
        let req1 = req0 + "SET op_revealed_ = NULL AND block_revealed_ = NULL AND level_revealed_ = NULL AND oracle_choice_ = NULL AND status_ = NULL AND reward_ = NULL ";
        let req2 = req1 + `WHERE level_revealed_ >= ${env.min_crawled_level}`
        let res_del_2 = await utils.pg_query_promise(pool, req2, []);
        console.log(res_del_2);
        return;
    }
};

/**
 * Once new update in smart contracts fetched, update database
 */
played_game_id = async function(config, e, playersBigMapID) {
    if (e.failed) {
        return (-1);
    }
    let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/${e.block}/context/big_maps/${playersBigMapID}`);
    let dt = { dune_expr: { address: e.player } };
    let res = await axios.post(req, dt, { headers: { 'Content-Type': 'application/json' } });
    return (parseInt(res.data.dune_expr.record.last_game.int));
}

update_database = async function(config, info, pool, env, playersBigMapID) {
    await env.played.reverse().reduce(async function(acc, e) {
        await acc;
        let game_id = await played_game_id(config, e, playersBigMapID);
        console.log(`inserting game_id = ${game_id} to DB ...`);
        let req0 = `INSERT INTO ${info.table} (game_id_, op_played_, block_played_, level_played_, game_block_level_, player_, player_choice_ , failed_, delegated_)`;
        let req = `${req0} VALUES (${game_id}, '${e.op}','${e.block}',${e.level},${e.level},'${e.player}','${e.player_choice}',${e.failed},${e.delegated});`;
        return utils.pg_query_promise(pool, req, []);
    }, Promise.resolve());
    await env.revealed.reverse().reduce(async function(acc, e) {
        await acc;
        console.log(`revealing  game_id = ${e.game_id} will be saved to DB ...`);
        let req0 = `UPDATE ${info.table} SET (op_revealed_, block_revealed_, level_revealed_, oracle_choice_ , status_, reward_)`;
        let req1 = `${req0} = ('${e.op}','${e.block}',${e.level},'${e.oracle_choice}', '${e.status}', ${e.reward}) `;
        let req = `${req1} WHERE game_id_ = ${e.game_id} AND game_block_level_ = ${e.game_block_level} AND player_ = '${e.player}' AND player_choice_ = '${e.player_choice}' and failed_ = FALSE;`;
        return utils.pg_query_promise(pool, req, []);
    }, Promise.resolve());
    return;
};

/**
 * Main loop that repeats these steps: fetch new transactions - backtrack if needed - save transactions to db
 */
loop = async function(config, info, pool, playersBigMapID, sleep_ms) {
    try {
        let res_played =
            await utils.pg_query_promise(pool, `SELECT COALESCE(MAX(level_played_),0) FROM ${info.table}`, []);
        let res_revealed =
            await utils.pg_query_promise(pool, `SELECT COALESCE(MAX(level_revealed_),0) FROM ${info.table}`, []);
        let last_played_level = parseInt(res_played.rows[0].coalesce);
        let last_revealed_level = parseInt(res_revealed.rows[0].coalesce);
        last_crawled_level = Math.max(last_played_level, last_revealed_level);
        console.log('last_played_level: ' + last_played_level);
        console.log('last_revealed_level: ' + last_revealed_level);
        console.log('last_crawled_level: ' + last_crawled_level);
        let env = { played: [], revealed: [], min_crawled_level: Number.MAX_VALUE, fixpoint: false };
        // env will be updated by call to crawler function
        await crawler(config, info, pool, env, 0);
        let nb_plays = env.played.length;
        let nb_reveals = env.revealed.length;
        console.log(`CRAWLER SUMMARY: ${nb_plays} plays and ${nb_reveals} reveals to be saved!\n`);
        await backtrack_if_needed(info, pool, env, last_crawled_level);
        await update_database(config, info, pool, env, playersBigMapID);
        console.log('-- step ended -------------------------------------------\n');
        await utils.sleep(sleep_ms);
        await loop(config, info, pool, playersBigMapID, sleep_ms);
        return;
    } catch (err) {
        console.log("[error] SELECT from DB failed: " + err);
        throw (new Error(err));
    }
};


main = async function(info) {

    var config = configLib.parseArgs();
    const pool = new Pool(config.database);
    const sleep_ms = Math.max(5, (config.time_between_blocks * 1000) / 6); // every 10 seconds on mainnet

    const contract_info = config.contracts.find(e => e.name === info.contract_name);
    info.contract_addr = contract_info.addr;
    console.log("KT1 address is: " + info.contract_addr);
    let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/head/context/contracts/${info.contract_addr}/storage`);
    let storage_data = await axios.get(req);
    let result = storage_data.data;
    playersBigMapID = parseInt(result.dune_expr.record.players.bigmap.some);
    console.log("Players BigMap ID is: " + playersBigMapID);
    loop(config, info, pool, playersBigMapID, sleep_ms).then(function() {
        console.log('Done!');
        process.exit(0);
    }, function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    }).catch(function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    });
}


module.exports = {
    main
};