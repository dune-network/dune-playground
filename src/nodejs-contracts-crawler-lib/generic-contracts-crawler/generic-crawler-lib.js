require('log-timestamp');
const axios = require('axios').default;
const Pool = require('pg').Pool;
const utils = require('utils-lib');

// TODO: review functions and name of params

/**
 * Detect if some latest blocks of have been invalidated due to chain re-organisation, and remove related information from DB.
 */
backtrack_if_needed = async function(info, pool, env, old_crawled_level, contracts_info) {
    console.log('env.min_crawled_level = ' + env.min_crawled_level);
    if ((env.ops.length === 0 && env.ops.length === 0) ||
        env.min_crawled_level > old_crawled_level ||
        old_crawled_level === 0) {
        console.log("no backtrack detected");
        return; // no backtrack detected
    } else {
        console.log("[XXX] Chain backtrack detected! Should re-organize DB before saving new data");
        // backtrack main table
        let req = `DELETE FROM ${info.blocks_table} WHERE level_ >= ${env.min_crawled_level}`;
        let res_del = await utils.pg_query_promise(pool, req, []);
        await Object.keys(contracts_info).reduce(async function(acc, k) {
            await acc;
            let entries_info = contracts_info[k];
            await Object.keys(entries_info).reduce(async function(acc, e) {
                await acc;
                let v = entries_info[e];
                let req = `DELETE FROM ${v.table} WHERE block_level_ >= ${env.min_crawled_level}`;
                return utils.pg_query_promise(pool, req, []);
            }, Promise.resolve());
        }, Promise.resolve());
        return;
    }
};

/**
 * Once new update in smart contracts fetched, update database
 */
update_database = async function(config, info, pool, env) {
    await env.blocks.reverse().reduce(async function(acc, insert_req) {
        await acc;
        x = utils.pg_query_promise(pool, insert_req, []);
        return x;
    }, Promise.resolve());
    await env.ops.reverse().reduce(async function(acc, insert_req) {
        await acc;
        return utils.pg_query_promise(pool, insert_req, []);
    }, Promise.resolve());
    return;
};


/**
 * returns the entrypoint name and value of the given operation
 */
get_entrypoint_of_op = function(operation) {
    if (operation == null || operation.parameters == null || operation.parameters.entrypoint == null) {
        return { entrypoint: '' };
    }
    return { entrypoint: operation.parameters.entrypoint, value: operation.parameters.value };
};


/**
 * Process a transaction and all its internal transactions:
 * - detect if a fixpoint is reached and return
 * - otherwise, detect if there are ongoing transactions to desired smart contract with desired entrypoint
 * and update 'env' accordingly.
 */
process_a_multi_transaction = async function(pool, contracts_info, env, operation, bl_hash, bl_level) {
    let op_hash = operation.hash;
    operation.contents.forEach(function(in_operation) {
        let status = in_operation.metadata.operation_result.status;
        let failed = (status === 'failed');
        if (failed) {
            console.log('ignoring failed transaction ' + op_hash);
        } else {
            let src = in_operation.source;
            let dest = in_operation.destination;
            let entries_info = contracts_info[dest];
            if (entries_info === undefined || entries_info === null) {
                console.log(`Skipping operation to ${dest}. Got no instructions to crawl it.`);
            } else {
                let param = get_entrypoint_of_op(in_operation);
                let entry_info = entries_info[param.entrypoint];
                if (entry_info === null || entry_info === undefined) {
                    console.log(`entrypoint ${param.entrypoint} is unknown or no crawling method is registered for it`);
                } else {
                    console.log('will process operation ' + op_hash);
                    let generic_vals = {
                        sender: `'${src}'`,
                        op_hash: `'${op_hash}'`,
                        bl_hash: `'${bl_hash}'`,
                        op_level: bl_level
                    };
                    let res = entry_info.decoder(generic_vals, param.value);
                    let insert = `INSERT INTO ${entry_info.table}${entry_info.schema} VALUES ${res}`;
                    console.log('PUSH: ' + insert);
                    env.ops.push(insert);
                }
            }
        }
    });
};

/**
 * recursively crawle the desired smart contract transactions pages until reaching a fixpoint.
 */
crawler = async function(config, pool, contracts_info, info, env, block_hash) {
    let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/${block_hash}`);
    let data = await axios.get(req);
    let result = data.data;
    let b_hash = result.hash;
    let b_level = parseInt(result.header.level);
    let pred_hash = result.header.predecessor;
    let transactions = result.operations[3];

    console.log(`processing block ${b_hash} of level ${b_level}: it has ${transactions.length} opereations and its predecessor is ${pred_hash}`);

    let req_fixpoint = `SELECT EXISTS(SELECT FROM ${info.blocks_table} WHERE hash_ = '${b_hash}')`;
    let res_exists = await utils.pg_query_promise(pool, req_fixpoint, []);
    if (res_exists.rows[0].exists) {
        console.log('Fixpoint detected/reached!');
        env.fixpoint = true;
        return;
    } else {
        let insert = `INSERT INTO ${info.blocks_table}(hash_, level_) VALUES ('${b_hash}',${b_level}) ON CONFLICT DO NOTHING`;
        env.blocks.push(insert);
        env.min_crawled_level = Math.min(env.min_crawled_level, b_level); // used to detect chain backtracks

        await transactions.reduce(async function(acc, operation) {
            await acc;
            if (env.fixpoint) {
                return (Promise.resolve())
            } else {
                return process_a_multi_transaction(pool, contracts_info, env, operation, b_hash, b_level);
            }
        }, Promise.resolve());

        let nb_ops = env.ops.length;
        let nb_blocks = env.blocks.length;
        console.log(`Got ${nb_ops} ops and ${nb_blocks} blocks(?) so far`);
        if (b_level <= info.stop_level) {
            console.log('crawling ended because stop-level reached !');
            return;
        } else if (env.fixpoint) {
            console.log('Fixpoint reached!');
            return;
        } else {
            await crawler(config, pool, contracts_info, info, env, pred_hash);
        }
    }
};

/**
 * Main loop that repeats these steps: fetch new transactions - backtrack if needed - save transactions to db
 */
loop = async function(config, pool, sleep_ms, contracts_info, info) {
    try {
        let last_crawled =
            await utils.pg_query_promise(pool, `SELECT COALESCE(MAX(level_),0) FROM ${info.blocks_table}`, []);
        let old_crawled_level = parseInt(last_crawled.rows[0].coalesce);
        console.log('old_crawled_level: ' + old_crawled_level);
        let env = { ops: [], blocks: [], min_crawled_level: Number.MAX_VALUE, fixpoint: false };
        // env will be updated by call to crawler function
        await crawler(config, pool, contracts_info, info, env, info.init_block_hash);
        let nb_ops = env.ops.length;
        let nb_blocks = env.blocks.length;
        console.log(`CRAWLER SUMMARY: nb ${nb_ops} new operations and ${nb_blocks} blocks(?) to be saved!`);

        await backtrack_if_needed(info, pool, env, old_crawled_level, contracts_info);
        await update_database(config, info, pool, env);

        console.log('-- step ended -------------------------------------------\n');
        await utils.sleep(sleep_ms);
        await loop(config, pool, sleep_ms, contracts_info, info);
        return;
    } catch (err) {
        console.log("[error] SELECT from DB failed: " + err);
        throw (new Error(err));
    }
};

main = async function(config, contracts_info, info) {

    const pool = new Pool(config.database);
    const sleep_ms = Math.max(5, (config.time_between_blocks * 1000) / 6); // every 10 seconds on mainnet
    loop(config, pool, sleep_ms, contracts_info, info).then(function() {
        console.log('Done!');
        process.exit(0);
    }, function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    }).catch(function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    });
}

let common_tables_content = {
    op_hash_: 'CHAR(51)',
    block_hash_: 'CHAR(51)',
    block_level_: 'BIGINT'
}

// FOREIGN KEYS MISSING

prepare_sql_info = function(contracts_data, tables_suffix, extra_db) {

    var sql = "";
    sql += extra_db + "\n";
    contracts_info = [];

    contracts_data.forEach(ci => {
        entries_info = [];
        let entrypoints = ci.entrypoints;
        Object.keys(entrypoints).forEach(function(entrypoint) {
            let epoint_info = entrypoints[entrypoint];
            var fields = epoint_info.fields;
            let table_name = ci.name + '__' + entrypoint + '__' + tables_suffix;
            var sql_tab = `DROP TABLE ${table_name} CASCADE;\n\n`;
            sql_tab += `CREATE TABLE ${table_name}(`;
            var tuple = "";
            var first = true;
            Object.keys(fields).forEach(function(field) {
                var fval = fields[field];
                if (first) {
                    sql_tab += `\n  ${field} ${fval}`;
                    tuple = field;
                    first = false;
                } else {
                    sql_tab += `,\n  ${field} ${fval}`;
                    tuple += `, ${field}`;
                }
            });
            Object.keys(common_tables_content).forEach(function(field) {
                var fval = common_tables_content[field];
                sql_tab += `,\n  ${field} ${fval}`;
                tuple += `, ${field}`;
            });
            sql_tab += '\n);\n\n'

            sql += sql_tab;

            epoint_info.indexes.forEach(ind => {
                var concat_index = ind.replace(',', '_');
                concat_index = concat_index.replace(' ', '');
                let ind_name = `${table_name}_IndexOn_${concat_index}`;
                sql += `CREATE INDEX ${ind_name} ON ${table_name}(${ind});\n\n`;
            });

            let dec_func = function(generic_vals, entry_arg) {
                try {
                    let tmp = entrypoints[entrypoint].decoder(entry_arg, generic_vals);
                    console.log(`${entrypoint}: Decoded ${JSON.stringify(entry_arg)} as ${JSON.stringify(tmp)}`);
                    return tmp;
                } catch (error) {
                    console.log(`Decoding ${entry_arg} failed with:\n${error}`);
                    throw (new exception(error));
                }
            };
            entries_info[entrypoint] = { table: table_name, schema: `(${tuple})`, decoder: dec_func };

        });
        contracts_info[ci.addr] = entries_info;
    });
    return { mk_sql_tables: sql, contracts_info: contracts_info };
}


module.exports = {
    main
}