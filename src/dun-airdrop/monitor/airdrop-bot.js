require('log-timestamp');
const Pool = require('pg').Pool;
const axios = require('axios').default;
const configLib = require('config-lib');
const utils = require('utils-lib');

airdrop = async function(config, pool) {
    let res = await utils.pg_query_promise(pool,
        `SELECT * from accounts_to_airdrop WHERE op_ IS NULL ORDER BY id_ ASC LIMIT ${config.duns_airdrop.operations_per_airdrop};`, []
    );
    if (res.rowCount === 0) {
        console.log("No dn1 to airdrop ...");
        return;
    } else {
        console.log('[airdrop] ' + res.rowCount + ' columns selected');
        var mu_amount = 0;
        list = "";
        first = true;
        // console.log(JSON.stringify(res.rows));
        res.rows.forEach(element => {
            //console.log(element.dn1_);
            let new_nb_airdrops = 1 + parseInt(element.nb_airdrops_);
            let a = Math.max(1, Math.round(((config.duns_airdrop.dun_per_dn1 * 1000000) / new_nb_airdrops)));
            mu_amount = mu_amount + a;
            if (first) {
                list = 'Pair "' + element.dn1_ + '" ' + a; // in mudun
                first = false;
            } else {
                list = " " + list + ' ; Pair "' + element.dn1_ + '" ' + a; // in mudun
            }
        });
        amount = Math.ceil((mu_amount / 1000000) * 1000000) / 1000000; // normalize + ceil to avoid FP rounding errors due to additions in previous loop
        let op_hash = await utils.inject_transaction(config, {
            src: 'airdrop_manager',
            dest: 'airdrop_contract',
            amount: amount,
            arg: `'{ ${list} }'`,
            burn_cap: 2,
            wait: 0
        });
        // should do something with the hash if --wait is not none !!
        let level = await utils.current_level(config);
        console.log('Airdrop operation injected with op_hash = ' + op_hash + " and level = " + level);
        await res.rows.reduce(async function(acc, element) {
            await acc;
            return utils.pg_query_promise(pool,
                `UPDATE accounts_to_airdrop SET (op_, op_level_) = ('${op_hash}', ${level}) WHERE dn1_ = '${element.dn1_}'`, []);
        }, Promise.resolve());
        return;
    }
};

clean_raws = async function(pool, op) {
    await utils.pg_query_promise(pool,
        `INSERT INTO airdropped_accounts (dn1_, op_, op_level_) (SELECT dn1_, op_, op_level_ FROM accounts_to_airdrop WHERE op_ = '${op}')`, []);
    await utils.pg_query_promise(pool,
        `DELETE FROM accounts_to_airdrop WHERE op_ = '${op}'`, []);
    return;
};

reset_raws = async function(pool, op) {
    await utils.pg_query_promise(pool,
        `UPDATE accounts_to_airdrop SET (op_, op_level_) = (NULL, NULL) WHERE op_ = '${op}'`, []
    );
    return;
};

archive = async function(config, pool) {
    let level = await utils.current_level(config);
    let delta_lvl = level - config.duns_airdrop.blocking_airdrop_period;
    let req = `SELECT op_ from accounts_to_airdrop WHERE op_ IS NOT NULL AND op_level_ < ${delta_lvl}`;
    let res = await utils.pg_query_promise(pool, req, []);
    if (res.rowCount === 0) {
        console.log("No dn1 to archive ...");
        return;
    } else {
        console.log('current level: ' + level);
        console.log('[archive] ' + res.rowCount + ' columns selected');
        await res.rows.reduce(async function(acc, element) {
            await acc;
            let data = await axios.get(utils.mk_request_url(config.explorer_api_info, '/v4/operation_nomic/' + element.op_));
            if (data.data.type.operations[0].failed) {
                console.log("operation included but failed!");
                return (reset_raws(pool, element.op_));
            } else {
                console.log("operation included!");
                return (clean_raws(pool, element.op_));
            }
        }, Promise.resolve());
        return;
    }
};

loop = async function(config, pool, sleep_ms) {
    await airdrop(config, pool);
    await archive(config, pool);
    console.log('-- step ended --\n');
    await utils.sleep(sleep_ms);
    await loop(config, pool, sleep_ms);
};

var config = configLib.parseArgs();
const pool = new Pool(config.database);
const sleep_ms = (config.time_between_blocks * 1000) / 2;

loop(config, pool, sleep_ms).then(function() {
    console.log('Done!');
    process.exit(0);
}, function(err) {
    console.log('\nFailure!');
    console.log(err);
    process.exit(1);
}).catch(function(err) {
    console.log('\nFailure!');
    console.log(err);
    process.exit(1);
});