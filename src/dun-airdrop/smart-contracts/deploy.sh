#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for
    DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

$DUNE_PLAYGROUND_CLIENT_CMD originate contract airdrop_contract \
                            transferring 0 from playground_manager \
                            running ./airdrop_contract_generic.tz \
                            --init 'Unit' --force --burn-cap 1
