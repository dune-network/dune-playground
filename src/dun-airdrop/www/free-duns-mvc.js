/* global utils */

var freeDuns = {


    refreshModel: async function() {
        let metalState = await metalModel.getStateAsync(false);
        return metalState;
    },

    submitRequest: function(captcha) {
        dn1 = utils.getValue('dn1-to-airdrop');
        nodeRPC.balance(dn1, parameters.node).then(function(_res) { // to check the dn1 is valid
            if (captcha === "") {
                utils.setHTML('free-duns-info-area', 'Please, validate with the provided captcha!');
                return;
            }
            utils.xhr(parameters.apiEndpoint, "/airdrop-duns", "application/x-www-form-urlencoded", "dn1=" + dn1 + "&gcaptcha=" + captcha).then(function(res) {
                console.log(typeof res);
                console.log(typeof res.response);
                if (res.status === 201 || res.status === 200) { // post request succeded
                    utils.setHTML('free-duns-info-area', res.response);
                } else {
                    utils.setHTML('free-duns-info-area', 'An error occured while trying to save your dn1 (err code: ' + res.status + ').');
                }
            }, function(err) {
                utils.modelErrHandler(utils.networkError(err));
            }).catch(
                function(err) {
                    utils.modelErrHandler(utils.networkError(err));
                }
            );
        }, function(err) {
            //console.log(err);
            utils.setHTML('free-duns-info-area', 'Please, provide a valid dn1 to airdrop!');
            return;
        });
    },


    already_auto_filled: false,

    updateDN1: async function(auto) {
        try {
            let old_dn1 = utils.getValue('dn1-to-airdrop');
            if (auto && (old_dn1.length !== 0 || freeDuns.already_auto_filled)) {
                return;
            }
            let data = await freeDuns.refreshModel();
            if (data !== null && data.keyHash !== null) {
                utils.setValue('dn1-to-airdrop', data.keyHash);
                freeDuns.already_auto_filled = true;
            }
        } catch (error) {
            utils.modelErrHandler(error);
        }
    }
};