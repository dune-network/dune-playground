require('log-timestamp');
const axios = require('axios').default;
const Pool = require('pg').Pool;
const configLib = require('config-lib');
const utils = require('utils-lib');

inject_commits = async function(config, info, last_commit, commits_arr) {
    let coms = commits_arr.reduce(function(acc, e) {
        if (acc === "") {
            return ('0x' + e.commit)
        } else {
            return (acc + '; 0x' + e.commit)
        }
    }, "");
    let op_hash = await utils.inject_transaction(config, {
        src: info.manager,
        dest: info.contract_addr,
        amount: 0,
        arg: `'#love:(${last_commit}, [${coms}])'`,
        entrypoint: 'insert_commits',
        burn_cap: 15,
        wait: 0
    });
    return op_hash;
}

fresh_commits = async function(config, info, arr, nb) {
    if (nb <= 0) {
        return;
    } else {
        let res = await info.random_commit(config, info);
        arr.push(res);
        await fresh_commits(config, info, arr, nb - 1);
        return;
    }
}

insert_more_commits = async function(config, pool, info, last_commit) {
    let curr_level = await utils.current_level(config);
    let res = await utils.pg_query_promise(pool, `SELECT COALESCE(MAX(id_),0) from ${info.table}`);
    let last_commit_from_db = parseInt(res.rows[0].coalesce);
    // We know that all commits with id_ <= last_commit are included in the chain
    await utils.pg_query_promise(pool,
        `UPDATE ${info.table} set included_ = TRUE WHERE included_ is NULL and id_ <= ${last_commit}`);
    // Select commits that have not been included yet
    var not_included =
        await utils.pg_query_promise(pool,
            `SELECT id_, commit_, choice_, nonce_, inserted_level_, op_hash_ FROM ${info.table} WHERE included_ is NULL ORDER BY id_ ASC;`);
    if (last_commit_from_db === last_commit) {
        if (not_included.rows.length > 0) {
            console.log('Invariant broken: All inserted commits are supposed to be included');
            process.exit(0);
        }
        // generate fresh commits
        var arr = new Array();
        await fresh_commits(config, info, arr, info.delta_commits);
        console.log('New commits to insert: ' + JSON.stringify(arr));
        var cpt = 0;
        await arr.reduce(async function(accu, e) {
            cpt++;
            let req =
                `INSERT INTO ${info.table}(id_, commit_, choice_, nonce_, inserted_level_)
                   VALUES (${last_commit+cpt}, '${e.commit}', '${e.choice}', '${e.nonce}', ${curr_level})`;
            // console.log(req);
            await utils.pg_query_promise(pool, req);
            return;
        }, Promise.resolve());
        let op_hash = await inject_commits(config, info, last_commit, arr);
        let req2 =
            `UPDATE ${info.table} SET op_hash_ = '${op_hash}'
               WHERE op_hash_ is NULL and inserted_level_ = ${curr_level}`;
        // console.log(req);
        await utils.pg_query_promise(pool, req2);
        return;
    } else {
        let e = not_included.rows[0]; // pick one commit
        let delta = config.operations_ttl - (curr_level - e.inserted_level_);
        if (delta >= 0) {
            console.log(`non included commits already exist! Operation will be outdated in ${delta} blocks`);
            return;
        } else {
            console.log(`Error: outdated operation ${e.op_hash_} never included`);
            console.log(`I will WIP non included commits from table ${info.table}: ${JSON.stringify(not_included.rows)}`);
            // automatically resolve cases where operations that insert new commits are never included //
            await utils.pg_query_promise(pool, `DELETE FROM ${info.table} WHERE included_ is NULL AND id_ > ${last_commit};`);
            return;
            //process.exit(0);
        }
    }

}

reveal_commits = async function(config, pool, info, nb_play, last_revealed) {
    let max = Math.min(nb_play, 10);
    let limit = last_revealed + max; // do not reveal more than 10 at once
    let req = `SELECT id_, choice_, nonce_, commit_ FROM ${info.table} where id_ > ${last_revealed} AND id_ <= ${limit} ORDER BY id_ ASC`;
    let res = await utils.pg_query_promise(pool, req);
    if (res.rows.length !== max) {
        console.log(`Unexpected error: should reveal ${max} commits but only ${res.rows.length} were selected from DB!`);
        process.exit(0);
    }
    let to_reveal =
        await res.rows.reduce(async function(accu, e) {
            let acc = await accu;
            console.log("revealing: " + JSON.stringify(e));
            let tmp = await info.encode_choice(config, info, e);
            if (acc === "") {
                return `${tmp}`;
            } else {
                return `${acc}; ${tmp}`;
            }
        }, Promise.resolve(""));
    let hash = await utils.inject_transaction(config, {
        src: info.manager,
        dest: info.contract_addr,
        amount: 0,
        arg: `'#love:[${to_reveal}]'`,
        entrypoint: 'reveal_commits',
        burn_cap: 5,
        wait: 0
    });
    // should do something with the hash if --wait is not none !!
    console.log(`operation of revealing ${max} commit(s) injected with hash ${hash}...`);
    return hash
}

contract_indexes = async function(config, info) {
    console.log(`Getting ${info.contract_name} contract's storage ...`);
    let endpoint = `/chains/main/blocks/head//context/contracts/${info.contract_addr}/storage`;
    let req = utils.mk_request_url(config.node_info, endpoint);
    let res = await axios.get(req);
    let storage = res.data.dune_expr.record;
    return {
        last_revealed: parseInt(storage.last_revealed.int),
        last_play: parseInt(storage.last_played.int),
        last_commit: parseInt(storage.last_commit.int),
    }
}

loop = async function(config, pool, info, sleep_ms) {
    indexes = await contract_indexes(config, info);
    console.log(indexes);
    let nb_play = indexes.last_play - indexes.last_revealed;
    if (nb_play > 0) {
        console.log(`${nb_play} played but not revealed yet! I'll reveal`);
        await reveal_commits(config, pool, info, nb_play, indexes.last_revealed);
    } else {
        let nb_commits = indexes.last_commit - indexes.last_play;
        if (nb_commits < info.delta_commits) {
            console.log(`nb commits (${nb_commits}) below threshold (${info.delta_commits}). I'll insert more`);
            await insert_more_commits(config, pool, info, indexes.last_commit);
        } else {
            console.log("Nothing to do!");
        }
    }
    console.log('-- round ended -----------------------------------\n');
    await utils.sleep(sleep_ms);
    await loop(config, pool, info, sleep_ms);
}

main = async function(info) {
    const config = configLib.parseArgs();
    const pool = new Pool(config.database);
    const sleep_ms = (config.time_between_blocks * 1000) / 2;
    const contract_info = config.contracts.find(e => e.name === info.contract_name);
    info.contract_addr = contract_info.addr;
    info.delta_commits = contract_info.nb_unrevealed_commits;
    loop(config, pool, info, sleep_ms).then(function() {
        console.log('Done!');
        process.exit(0);
    }, function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    }).catch(function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    });
};

module.exports = {
    main
};