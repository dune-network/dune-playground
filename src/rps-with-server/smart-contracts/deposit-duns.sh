#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

AMOUNT=$1

if [ "$AMOUNT" = "" ]; then
    echo "You should provide the amount of $DUN to transfer to the contract as first argument!"
    exit 1
fi

$DUNE_PLAYGROUND_CLIENT_CMD transfer $AMOUNT \
                            from rps_with_server_manager to rps_with_server \
                            --entrypoint deposit
