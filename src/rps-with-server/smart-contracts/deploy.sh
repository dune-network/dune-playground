#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

if [ "$DUNE_PLAYGROUND_TIME_BETWEEN_BLOCKS" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_TIME_BETWEEN_BLOCKS env variable"
    exit 1
fi

# bytes_size = 10 means 80 bits, harder to bruteforce than an int64

$DUNE_PLAYGROUND_CLIENT_CMD originate contract rps_with_server \
                            transferring 0 from playground_manager \
                            running file:contract.lov.ml \
                            --init '#love:{ min_time_between_blocks = '$DUNE_PLAYGROUND_TIME_BETWEEN_BLOCKS'p ; bytes_size=10p; oracle_addresses = {address, unit:'\${pkh:rps_with_server_manager}' -> ()}; nb_oracle_addresses = 1; tokens_addr = '\${contract:dgg_token}'; lock_player_x_blocks = 1p; lock_reveal_x_blocks = 0p; bet_amount = 0dun; winner_reward = 10p; looser_reward = 1p; tie_reward = 3p }' \
                            --burn-cap 10 --force
