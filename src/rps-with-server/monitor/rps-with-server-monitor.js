const contracts_monitor_lib = require('contracts-monitor-lib');
const utils = require('utils-lib');
const axios = require('axios');

random_commit = async function(config, info) {
    let res_nonce = await utils.shell_cmd(`echo ${utils.randInt(Number.MAX_SAFE_INTEGER)} | sha256sum | cut -d" " -f1`);
    let nonce = `0x${res_nonce.stdout.trim().substring(0, 20)}`;
    let choice = '0x0' + utils.randInt(3);
    let res_commit = await utils.shell_cmd(`echo ${choice}${nonce}| xxd -r -p | sha256sum -t | cut -d" " -f1`);
    let commit = res_commit.stdout.trim().substring(0, 20);
    let elt = {
        nonce: nonce,
        choice: choice,
        commit: commit
    };
    return (elt);
};

encode_choice = async function(config, info, e) {
    //return `(${e.choice_}, ${e.nonce_})`
    let move = (e.choice_ === '0x00') ? 'Rock' : ((e.choice_ === '0x01') ? 'Paper' : 'Scissors');
    let nonce = e.nonce_.substring(2); // remove 0x

    let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/head/context/contracts/${info.contract_addr}/exec_fun/get_game_result`);
    //let data = { expr: { dune_expr: { nat: `${move}` } }, gas: "2000" };
    let data = { expr: { dune_expr: { tuple: [{ int: `${e.id_}` }, { constr: [move, null] }, { bytes: nonce }] } }, gas: "800000" }
    let headers = { headers: { 'Content-Type': 'application/json' } };
    let res = await axios.post(req, JSON.stringify(data), headers);
    console.log(JSON.stringify(res.data));
    let r = res.data.dune_expr.tuple[1].record;
    let result = `{
         id = ${parseInt(r.id.int)};
         block_lvl = ${parseInt(r.block_lvl.nat)}p;
         player_addr = ${r.player_addr.addr};
         player_choice = ${r.player_choice.constr[0]};
         oracle_choice = ${r.oracle_choice.constr[0]};
         status = ${r.status.constr[0]};
         reward = ${parseInt(r.reward.nat)}p
    }`;
    return `(${result}, ${e.nonce_})`
}

let info = {
    random_commit: random_commit,
    encode_choice: encode_choice,
    manager: 'rps_with_server_manager',
    contract_name: 'rps_with_server',
    table: 'rps_with_server_commits'
};

contracts_monitor_lib.main(info);