/* global rpsModel */
/* global rpsView */

var rpsController = {

    showPage: function(autoReload, hist_page) {
        let hpage = utils.historyPage(hist_page);
        utils.setWaitingMsg(utils.loadingPage, autoReload).then(function() {
            hpage = Math.max(1, hpage);
            rpsModel.refreshModel().then(function(data) {
                var maxPages = rpsModel.nbHistoryPages();
                maxPages = Number.isInteger(maxPages) ? Math.max(1, maxPages) : hpage;
                hpage = Math.min(hpage, maxPages);
                var pg = '?history=' + hpage;
                window.history.replaceState('state', pg, pg);
                rpsModel.getHistoryPage(hpage).then(function(dt) {
                    rpsView.updatePage(data, dt, hpage);
                    utils.unsetWaitingMsg(false /* not autoReload*/ ).then(function() {});
                }, utils.modelErrHandler).catch(utils.modelErrHandler);
            }, utils.modelErrHandler).catch(utils.modelErrHandler);
        });
    },

    play: function(choice) {
        rpsModel.playChoice(choice).then(function(res) {
            rpsView.updatePlayedChoice(res).then(function() {
                rpsController.showPage(true);
            });
        }, function(err) {
            console.log(err);
            utils.modelErrHandler(err);
        });
    },

    swapPayFees: function() {
        rpsModel.swapPayFees();
    },

    changeHistoryPage: function(delta) {
        let hpage = rpsModel.state.current_history_page + delta; // +1 or -1
        if (hpage <= 0)
            return;
        rpsController.showPage(false /* not autoReload*/ , hpage);
    }

};