/* global parameters */
/* global nodeRPC */
/*  global metal, utils */

var rpsModel = {

    state: {
        params: null,

        global_last_revealed: -1,
        global_last_played: -1,
        global_last_commit: -1,

        user_nb_games: 0,
        user_nb_won: 0,
        user_nb_tie: 0,
        user_success: 100,
        user_last_game: -1,

        old_history: [],
        new_history: [],
        current_history_page: 1,

        last_move_hash: null,
        last_move_position: null,
        last_move_value: null,
        inited: false,

        can_pay_fees: false,
        do_pay_fees: true,

        games_bigmap: null,
        players_bigmap: null
    },

    __playersMapID: function() {
        return new Promise(
            function(resolve, reject) {
                if (rpsModel.state.players_bigmap !== null) {
                    resolve(rpsModel.state.players_bigmap);
                } else {
                    nodeRpcHelpers.bigMapID(parameters.rps_with_server, e => e.players).then(function(res) {
                        rpsModel.state.players_bigmap = res;
                        resolve(rpsModel.state.players_bigmap);
                    }, function(err) { reject(utils.networkError(err)) });
                }
            });
    },

    __playersMap: function(keyHash) {
        return JSON.stringify({ dune_expr: { address: keyHash } });
    },

    __initParams: function(reject, pars) {
        let p = pars.record;
        return { // some of these infos (eg. reward are useless with server version
            lock_player_x_blocks: parseInt(p.lock_player_x_blocks.nat),
            lock_reveal_x_blocks: parseInt(p.lock_reveal_x_blocks.nat),
            bet_amount: parseInt(p.bet_amount.dun),
            winner_reward: parseInt(p.winner_reward.nat),
            looser_reward: parseInt(p.looser_reward.nat),
            tie_reward: parseInt(p.tie_reward.nat)
        };
    },

    __storageState: function() {
        return new Promise(
            function(resolve, reject) {
                nodeRPC.storage(parameters.rps_with_server, parameters.node).then(function(res) {
                    let storage = res.response.dune_expr.record;
                    if (rpsModel.state.params === null) {
                        pars = rpsModel.__initParams(reject, storage.params);
                    } else {
                        pars = rpsModel.state.params;
                    }
                    resolve({
                        global_last_revealed: storage.last_revealed.int,
                        global_last_played: storage.last_played.int,
                        global_last_commit: storage.last_commit.int,
                        params: pars
                    });
                }, function(err) { reject(utils.networkError(err)) });
            }
        );
    },

    __playerStats: function(metalState) {
        return new Promise(
            function(resolve, reject) {
                if (metalState === null || metalState.keyHash === null) {
                    resolve({
                        user_last_game: -1,
                        user_nb_games: 0,
                        user_nb_won: 0,
                        user_nb_tie: 0
                    });
                } else {
                    rpsModel.__playersMapID().then(function(players_bigmap_id) {
                        nodeRPC.bigmapGet(
                            rpsModel.__playersMap(metalState.keyHash),
                            players_bigmap_id, parameters.node).then(function(r) {
                            if (r.response === null) {
                                resolve({
                                    user_last_game: -1,
                                    user_nb_games: 0,
                                    user_nb_won: 0,
                                    user_nb_tie: 0
                                });
                            } else {
                                let st = r.response.dune_expr.record;
                                resolve({
                                    user_last_game: parseInt((st.last_game.int)),
                                    user_nb_games: parseInt((st.nb_games.nat)),
                                    user_nb_won: parseInt((st.nb_won.nat)),
                                    user_nb_tie: parseInt((st.nb_tie.nat))
                                });
                            }
                        }, function(err) { reject(utils.networkError(err)) });
                    }, function(err) { reject(utils.networkError(err)) });
                }
            }
        );
    },

    __statusOfRecentChoice: function(metalState) {
        return new Promise(
            function(resolve, reject) {
                if (metalState === null || metalState.keyHash === null) {
                    rpsModel.state.last_move_hash = null;
                    rpsModel.state.last_move_position = null;
                    rpsModel.state.last_move_value = null;
                    resolve();
                } else {
                    // look in mempool, otherwise, OK ? also look in head ?
                    nodeRpcHelpers.trackOperation(parameters.node, rpsModel.state.last_move_hash,
                        metalState.keyHash, parameters.rps_with_server,
                        rpsModel.state.params.lock_player_x_blocks + 1).then(function(last_op) {
                        rpsModel.state.last_move_position = last_op.position;
                        if (rpsModel.state.last_move_hash == null) {
                            rpsModel.state.last_move_hash = last_op.hash;
                        }
                        rpsModel.state.last_move_value = last_op.tx.parameters.value.dune_expr.constr[0];
                        resolve();
                    }, function(err) {
                        rpsModel.state.last_move_hash = null;
                        rpsModel.state.last_move_position = null;
                        rpsModel.state.last_move_value = null;
                        resolve();
                    });
                }
            });
    },

    canPayFees: async function(metalState) {
        let bal = await nodeRPC.balance(parameters.rps_with_server, parameters.node);
        return ((metalState === null || metalState.revealed) && parseInt(bal.response) >= 2); // 2 or 2000000 ?
    },

    refreshModel: function() {
        return new Promise(
            function(resolve, reject) {
                metalModel.getStateAsync(false).then(function(metalState) {
                    rpsModel.__storageState().then(function(s_state) {
                            rpsModel.state.global_last_revealed = s_state.global_last_revealed;
                            rpsModel.state.global_last_played = s_state.global_last_played;
                            rpsModel.state.global_last_commit = s_state.global_last_commit;
                            rpsModel.state.params = s_state.params;
                            rpsModel.__playerStats(metalState).then(function(st) {
                                rpsModel.state.user_last_game = st.user_last_game;
                                rpsModel.state.user_nb_games = st.user_nb_games;
                                rpsModel.state.user_nb_won = st.user_nb_won;
                                rpsModel.state.user_nb_tie = st.user_nb_tie;
                                let success =
                                    (rpsModel.state.user_nb_games === 0) ?
                                    100 :
                                    (rpsModel.state.user_nb_won * 100 / rpsModel.state.user_nb_games);
                                rpsModel.state.user_success = success.toFixed(2);
                                rpsModel.__statusOfRecentChoice(metalState).then(function() {
                                    rpsModel.canPayFees(metalState).then(function(can_pay) {
                                        rpsModel.state.can_pay_fees = can_pay;
                                        rpsModel.state.inited = true;
                                        resolve(JSON.parse(JSON.stringify(rpsModel.state)));
                                    });
                                });
                            });
                        },
                        function(err) { reject(utils.networkError(err)) });
                });
            });
    },

    playChoice: function(choice) {
        return new Promise(
            function(resolve, reject) {
                if (rpsModel.state.last_move_position !== null &&
                    rpsModel.state.last_move_position < rpsModel.state.params.lock_player_x_blocks) {
                    reject('You cannot play again. Should wait');
                } else {
                    if (typeof metal === 'undefined' || metal === undefined) {
                        reject(utils.metalNotDetected);
                    } else {
                        let ok = false;
                        metal.isEnabled(function(res) {
                            if (!res) {
                                reject(utils.metalNotConfigured);
                            }
                            metal.getAccount(function(r) {
                                let collectCall = rpsModel.state.can_pay_fees && rpsModel.state.do_pay_fees;
                                let op = {
                                    dst: parameters.rps_with_server,
                                    amount: '0',
                                    network: parameters.network,
                                    entrypoint: 'play',
                                    parameter: '#love:' + choice,
                                    collect_call: collectCall,
                                    gas_limit: '115000',
                                    storage_limit: '240',
                                    cb: function(res) {
                                        console.log(res);
                                        if (res.ok) {
                                            rpsModel.state.last_move_hash = res.msg;
                                            rpsModel.state.last_move_position = -1; // mempool
                                            rpsModel.state.last_move_value = choice;
                                        }
                                        resolve(res);
                                    }
                                };
                                metal.send(op);
                            });
                        });
                    }
                }
            }
        );
    },

    __statusOfInternalStatus: function(istatus) {
        switch (istatus) {
            case "PlayerWins":
                return "Won";
            case "PlayerLoses":
                return "Lost";
            case "Tie":
                return "Tie";
            case null:
                return "Ongoing";
            default:
                return "?";
        }
    },

    getXgamesFromDB: function(page, size) {
        return new Promise(
            function(resolve, reject) {
                let metalState = metalModel.getState();
                if (metalState === null || metalState.keyHash === null) {
                    resolve([]);
                } else {
                    utils.xhr(parameters.apiEndpoint, "/rps-games/" + metalState.keyHash + "?page=" + page + "&size=" + size).then(function(res) {
                        if (res.status === 200) { // post request succeded
                            let resp = JSON.parse(res.response);
                            result = [];
                            resp.forEach(e => {
                                let status = rpsModel.__statusOfInternalStatus(e.status_);
                                let g = {
                                    game_id: e.game_id_,
                                    player_move: e.player_choice_,
                                    oracle_move: (status === "Ongoing") ? "Not revealed yet" : e.oracle_choice_,
                                    game_status: status,
                                    game_block: e.game_block_level_,
                                    prev_game: -1, // useless
                                    op_played: e.op_played_,
                                    op_revealed: e.op_revealed_,
                                    failed: e.failed_,
                                    reward: (e.reward_ === null) ? '?' : e.reward_
                                };
                                result.push(g);
                            });
                            resolve(result);
                        } else {
                            reject(res.response);
                        }
                    }, function(err) { reject(utils.networkError(err)) });
                }
            });
    },

    getHistoryPage: function(hpage) {
        return new Promise(
            function(resolve, reject) {
                if (hpage !== undefined) {
                    rpsModel.state.current_history_page = hpage;
                }
                rpsModel.getXgamesFromDB(hpage - 1, 10).then(function(result) { //TODO put parameters
                    resolve(result);
                }, function(err) {
                    reject(err);
                });
            });
    },

    nbHistoryPages: function() {
        return Math.ceil(rpsModel.state.user_nb_games / 10);
    },

    swapPayFees: function() {
        rpsModel.state.do_pay_fees = !rpsModel.state.do_pay_fees;
    }

};
