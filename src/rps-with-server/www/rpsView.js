/* global utils */

var rpsView = {

    __colorOfStatus: function(status) {
        switch (status) {
            case "Won":
                return "#cfc";
            case "Lost":
                return "#fcc";
            case "Tie":
                return "#dde";
            case "Waiting for inclusion":
                return "#fc6";
            default:
                return "#ff6";
        }
    },

    updatePlayed: function(data) {
        // player stats
        utils.setHTML('play-self-finished', data.user_nb_games);
        utils.setHTML('play-self-nb-won', data.user_nb_won);
        utils.setHTML('play-self-success-rate', data.user_success);
        // global stats
        utils.setHTML('play-global-finished', data.global_last_revealed);
        utils.setHTML('play-global-ongoing', data.global_last_played - data.global_last_revealed);
        utils.setHTML('play-global-open', data.global_last_commit - data.global_last_played);

        var play_msg = '';
        var next_msg = '';
        if (data.last_move_position !== null) {
            next_msg += 'Latest injected choice';
            if (data.last_move_hash !== null) {
                let op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                next_msg += ' (hash ' + op_hash + ')';
            }
            if (data.last_move_position < 0) {
                next_msg += ' is waiting for inclusion.';
            } else {
                next_msg += ' has been included ' + data.last_move_position + ' block(s) ago.';
            }
            next_msg = '<p class="pp" style="margin:0 80px">' + next_msg + '</p>';
            if (data.last_move_position >= data.params.lock_player_x_blocks) {
                play_msg += '<h2>You can play again! Make another choice...</h2><p class="pp">Click on one of the icons below:</p>';
                document.getElementById('play-area').classList.remove('disabled-div');
                document.getElementById('rock').style.cursor = 'pointer';
                document.getElementById('paper').style.cursor = 'pointer';
                document.getElementById('scissors').style.cursor = 'pointer';
            } else {
                play_msg += '<h2>Wait for your last choice to be included...</h2>';
                document.getElementById('play-area').classList.add('disabled-div');
                document.getElementById('rock').style.cursor = 'not-allowed';
                document.getElementById('paper').style.cursor = 'not-allowed';
                document.getElementById('scissors').style.cursor = 'not-allowed';
            }
        } else {
            document.getElementById('play-area').classList.remove('disabled-div');
            document.getElementById('rock').style.cursor = 'pointer';
            document.getElementById('paper').style.cursor = 'pointer';
            document.getElementById('scissors').style.cursor = 'pointer';
            play_msg += '<h2>Make Your Choice</h2><p class="pp">Click on one of the icons below:</p>';
        }
        utils.setHTML('play-move-status', next_msg);
        utils.setHTML('play-message', play_msg);
    },

    pendingAnimation: function() {
        return ('<img src="images/loader.gif" style="height:30px"></img>');
    },

    addRow: function(rows, g) {
        if (g.failed) {
            color = rpsView.__colorOfStatus(g.game_status);
            let player_move = (g.op_played == null) ? g.player_move : utils.explorerLink(g.op_played, g.player_move, true);
            rows = rows + "<tr class='pp' style='background-color:#ccc'>";
            rows += "<td>-</td>";
            rows += "<td>" + player_move + "</td>";
            rows += "<td>-</td>";
            rows += "<td>Operation failed</td>";
            rows += "<td>-</td>";
            rows += "</tr>";
        } else {
            color = rpsView.__colorOfStatus(g.game_status);
            let player_move = (g.op_played == null) ? g.player_move : utils.explorerLink(g.op_played, g.player_move, true);
            rows = rows + "<tr class='pp'>";
            rows += "<td>" + g.game_id + "</td>";
            rows += "<td>" + player_move + "</td>";
            if (g.oracle_move === 'Not revealed yet') {
                rows += "<td>" + (rpsView.pendingAnimation()) + "</td>";
            } else {
                let oracle_move = (g.op_revealed == null) ? g.oracle_move : utils.explorerLink(g.op_revealed, g.oracle_move, true);
                rows += "<td>" + oracle_move + "</td>";
            }
            rows += "<td style=\"background-color:" + color + "\">" + g.game_status + "</td>";
            rows += "<td>" + g.reward + " DGG</td>";
            rows += "</tr>";
        }
        return rows;
    },

    showPendingAsRow: function(rows, data, dt) {
        if (data.last_move_hash === null || data.current_history_page > 1 ||
            dt.some(e => { return (e.op_played === data.last_move_hash) })) {
            return rows;
        } else {
            // even if the operation is included, keep showing it as 'waiting for inclusion'
            // until indexer sees it. This is different from serverless version
            let g = {
                game_id: rpsView.pendingAnimation(),
                oracle_move: ' ',
                player_move: data.last_move_value,
                reward: '?',
                op_played: data.last_move_hash,
                game_status: 'Waiting for inclusion'
            };
            return (rpsView.addRow(rows, g));
        }
    },

    updatePage: function(data, dt, hpage) {
        rpsView.updatePlayed(data);
        var rows = "";
        rows = rpsView.showPendingAsRow(rows, data, dt);
        for (i = 0; i < dt.length; i++) {
            let g = dt[i];
            rows = rpsView.addRow(rows, g);
        }
        // default
        //document.getElementById('reveal-pubkey').style.display = 'none';
        document.getElementById('pay-fees').style.display = 'block';
        document.getElementById('get-free-duns').style.display = 'none';
        document.getElementById('collectCall').checked = false;
        document.getElementById('collectCall').disabled = true;

        if (data.can_pay_fees) {
            document.getElementById('collectCall').checked = data.do_pay_fees;
            document.getElementById('collectCall').disabled = false;
        }
        let metalState = metalModel.getState();
        if (metalState === null || !metalState.revealed || metalState.dun_balance === 0) {
            document.getElementById('get-free-duns').style.display = 'inline-block';
        }
        utils.setHTML('rps-history-games-list', rows);
        utils.setHTML('history-page-number', 'Page ' + hpage);

    },

    updatePlayedChoice: function(op) {
        return new Promise(
            function(resolve, reject) {
                if (op.ok && op.msg !== undefined) {
                    console.log(JSON.stringify(op));
                    let op_hash = utils.explorerLink(op.msg, op.msg, true);
                    var msg = '';
                    msg += '<p class="pp" style="margin:0 80px">Latest choice injected with hash ' + op_hash + '</p>';
                    utils.setHTML('play-move-status', msg);
                }
                resolve();
            });
    }
};