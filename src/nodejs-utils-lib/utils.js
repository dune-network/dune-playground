const Pool = require('pg').Pool;
const exec = require('child_process').exec;
const tmp = require('tmp');
const fs = require('fs');
const axios = require('axios');


mk_request_prefix = function(info) {
    let prefix = (info.https) ? "https" : "http";
    return `${prefix}://${info.addr}:${info.port}`;
};

mk_request_url = function(info, req) {
    let pr = mk_request_prefix(info);
    return `${pr}/${req}`;
};

mk_client_cmd = function(config) {
    let node = config.node_info;
    let ssl = (node.https) ? "-S " : "";
    let dune_config = `DUNE_CONFIG=${config.dune_config}`;
    return `${dune_config} ${config.dune_network_directory}/dune-client -d ${config.client_base_dir} -A ${node.addr} -P ${node.port} ${ssl}`;
};


shell_cmd = function(cmd) {
    return new Promise((resolve, reject) => {
        exec(cmd, function(error, stdout, stderr) {
            if (error === null) {
                resolve({ failed: false, stdout: stdout, stderr: stderr })
            } else {
                reject({ failed: true, stdout: stdout, stderr: stderr })
            }
        });
    });
};

sleep = function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};

pg_query_promise = function(pool, req, args) {
    return new Promise((resolve, reject) => {
        pool.query(req, args, (error, result) => {
            if (error) {
                return (reject(error));
            } else {
                return (resolve(result));
            }
        });
    });
};


randInt = function(max) {
    return Math.floor(Math.random() * max);
}


add_option = function(option, value) {
    if (typeof value === 'undefined') {
        return '';
    } else {
        return ` --${option} ${value}`
    }
}

// TODO use something else (lib) rather than CLI
// op is an object that should have fields src, dest and amount, and may optionnally have:
// entrypoint, arg, fee, storage_limit, burn_cap, gas_limit, wait,
// The default behavior is to wait until the operation is included in a block.
inject_transaction = async function(config, op) {
    let cli = mk_client_cmd(config);
    const tmpobj = tmp.fileSync({ mode: 0o644, prefix: 'playground__', postfix: '__dune-client.in' });
    let op_hash_in = tmpobj.name;
    let op_hash_out = op_hash_in.slice(0, -3); // remove .in extension
    fs.writeFileSync(op_hash_in, '${operation_hash}');
    let subst = ` --subst ${op_hash_in}`;
    let wait = (typeof op.wait === 'undefined') ? ' --wait 0' : add_option('wait', op.wait);
    let base_cmd = `${cli}${subst}${wait} transfer ${op.amount} from ${op.src} to ${op.dest}`;
    let entrypoint = add_option('entrypoint', op.entrypoint);
    let arg = add_option('arg', op.arg);
    let burn_cap = add_option('burn-cap', op.burn_cap);
    let gas_limit = add_option('gas-limit', op.gas_limit);
    let storage_limit = add_option('storage-limit', op.storage_limit);
    let fee = add_option('fee', op.fee);
    let full_cmd = base_cmd + entrypoint + arg + burn_cap + fee + gas_limit + storage_limit; // + ' --dry-run';
    console.log('[inject_transaction] I will exec the following command: ' + full_cmd);
    try {
        let cmd_res = await shell_cmd(full_cmd);
        let op_hash = fs.readFileSync(op_hash_out);
        console.log(`transaction injected with hash: ${op_hash}`);
        fs.unlink(op_hash_in, _e => {});
        fs.unlink(op_hash_out, _e => {});
        if (op_hash.length !== 51) {
            let err = `bad hash length: ${op_hash}`;
            console.log(err);
            throw (new Error(err));
        }
        return op_hash;
    } catch (error) {
        try {
            let op_hash = fs.readFileSync(op_hash_out);
            if (op_hash.length !== 51) {
                console.log(`bad hash length: ${op_hash}`);
                throw (new Error(error));
            }
            console.log(`transaction injected with hash: '${op_hash}' (but some error occurred: ${JSON.stringify(error)})`);
            fs.unlink(op_hash_in, _e => {});
            fs.unlink(op_hash_out, _e => {});
            return op_hash;
        } catch (_e) {
            console.log('Error in inject_transaction (and no hash read): ' + JSON.stringify(error));
            fs.unlink(op_hash_in, _e => {});
            fs.unlink(op_hash_out, _e => {});
            throw (new Error(error));
        }
    }
};

current_level = async function(config) {
    let req = mk_request_url(config.node_info, '/chains/main/blocks/head/header');
    let hd = await axios.get(req);
    return hd.data.level;
};

module.exports = {
    mk_request_prefix,
    mk_request_url,
    mk_client_cmd,
    sleep,
    pg_query_promise,
    shell_cmd,
    randInt,
    inject_transaction,
    current_level
};