require('log-timestamp');
const configLib = require('config-lib');
const minimist = require('minimist');
const crawler = require('generic-crawler-lib');
const raffle = require('spice-raffle-lib');

var entrypoints = {
    play: {
        decoder: function(e, generic_vals) {
            let tmp = raffle.play_entry_args_decode(e);
            let gid = tmp[0];
            let com = tmp[1] === null ? 'null' : `'${tmp[1]}'`;
            let res = [
                gid, com, generic_vals.sender,
                generic_vals.op_hash, generic_vals.bl_hash, generic_vals.op_level
            ];
            return `(${res})`
        },
        fields: {
            game_id_: 'INT',
            commit_: 'VARCHAR(150)',
            player_: 'CHAR(36)'
        },
        indexes: [
            "game_id_",
            "player_",
            "game_id_, player_"
        ]
    },
    player_reveal: {
        decoder: function(e, generic_vals) {
            let tmp = raffle.player_reveal_entry_args_decode(e);
            let gid = tmp[0];
            let sec = tmp[1][0];
            let nonce = tmp[1][1];
            let res = [
                gid, sec, nonce, generic_vals.sender,
                generic_vals.op_hash, generic_vals.bl_hash, generic_vals.op_level
            ];
            return `(${res})`
        },
        fields: {
            game_id_: 'INT',
            secret_: 'BIGINT',
            nonce_: 'BIGINT',
            player_: 'CHAR(36)'
        },
        indexes: [
            "game_id_",
            "player_"
        ]
    },
    reimburse_players: {
        decoder: function(e, generic_vals) {
            let tmp = raffle.reimburse_players_entry_args_decode(e);
            let gid = tmp[0];
            let set = tmp[1];
            var res = "";
            first = true
            set.forEach(elt => {
                let u = [
                    gid, `'${elt}'`,
                    generic_vals.op_hash, generic_vals.bl_hash, generic_vals.op_level
                ];
                if (first) {
                    res = `(${u})`
                    first = false
                } else {
                    res += `, (${u})`
                }
            });
            return `${res}`
        },
        fields: {
            game_id_: 'INT',
            refunded_player_: 'CHAR(36)'
        },
        indexes: [
            "game_id_"
        ]
    },
    /* no need to crawle it
    organizer_insert_new_raffle: {
        decoder : raffle.reimburse_players_entry_args_decode,
        fields : {
            game_id_: 'INT',
            commit_: 'VARCHAR(150)',
            player_: 'CHAR(36)'
        }
    },*/

    organizer_reveal: {
        decoder: function(e, generic_vals) {
            let tmp = raffle.organizer_reveal_entry_args_decode(e);
            let gid = tmp[0];
            let sec = tmp[1][0];
            let nonce = tmp[1][1];
            let tmp2 = tmp[2];
            let nb_regular = parseInt(tmp2.nb_regular);
            let nb_committed = parseInt(tmp2.nb_committed);
            let nb_revealed_commits = parseInt(tmp2.nb_revealed_commits);
            let inserted_on = parseInt(tmp2.inserted_on);
            let tmp_status = tmp2.state;
            //let tmp_status = tmp[2];
            let status = `'${tmp_status.constr}'`;
            switch (tmp_status.constr) {
                case 'End':
                    winner = tmp_status.args[0] === null ? 'NULL' : `'${tmp_status.args[0]}'`;
                    break;
                case 'Compromised':
                case 'Expired':
                    winner = 'NULL';
                    break;

                default:
                    let err = `organizer_reveal decoder ${status} should happen (invariant)`;
                    console.log(err);
                    throw (new exception(err));
            }
            let res = [
                gid, sec, nonce, winner, status,
                nb_regular, nb_committed, nb_revealed_commits, inserted_on,
                generic_vals.op_hash, generic_vals.bl_hash, generic_vals.op_level
            ];
            return `(${res})`
        },
        fields: {
            game_id_: 'INT',
            secret_: 'BIGINT',
            nonce_: 'BIGINT',
            winner_: 'CHAR(36)',
            result_: 'raffle_final_status',
            nb_regular_: 'INT',
            nb_committed_: 'INT',
            nb_revealed_commits_: 'INT',
            inserted_on_: 'BIGINT'
        },
        indexes: [
            "game_id_"
        ]
    },
    denounce_organizer: {
        decoder: function(e, generic_vals) {
            let tmp = raffle.denounce_organizer_entry_args_decode(e);
            let gid = tmp[0];
            let tmp2 = tmp[1];
            let nb_regular = parseInt(tmp2.nb_regular);
            let nb_committed = parseInt(tmp2.nb_committed);
            let nb_revealed_commits = parseInt(tmp2.nb_revealed_commits);
            let inserted_on = parseInt(tmp2.inserted_on);
            let res = [
                gid,
                nb_regular, nb_committed, nb_revealed_commits, inserted_on,
                generic_vals.op_hash, generic_vals.bl_hash, generic_vals.op_level
            ];
            return `(${res})`
        },
        fields: {
            game_id_: 'INT',
            nb_regular_: 'INT',
            nb_committed_: 'INT',
            nb_revealed_commits_: 'INT',
            inserted_on_: 'BIGINT'
        },
        indexes: [
            "game_id_"
        ]
    }
}

var config = configLib.parseArgs();

let extra_prog_args = config.programs_extra_args;
var raffle_extra_args = extra_prog_args === undefined ? "" : extra_prog_args.raffle_crawler === undefined ? "" : extra_prog_args.raffle_crawler;
raffle_extra_args = raffle_extra_args.split(' ');

let all_args = process.argv.slice(2).concat(raffle_extra_args);
var args = minimist(all_args, {
    string: ['head'],
    integer: ['stop'],
    string: ['db']
});

var fromHead = args.head === undefined ? 'head' : args.head;
var stopLevel = args.stop === undefined ? 0 : parseInt(args.stop);
var db = args.db === undefined ? false : (args.db === "true");

console.log('  --head: ' + fromHead);
console.log('  --stop: ' + stopLevel);
console.log('  --db: ' + db);

let raffles = config.contracts.filter(e => { return (e.family === "raffle") });

console.log('Found ' + raffles.length + ' contracts that belong to raffle family');

raffles.forEach(raffle => {
    raffle.entrypoints = entrypoints;
});

var extra_db = "DROP TYPE raffle_final_status CASCADE;\n\n";
extra_db += "CREATE TYPE raffle_final_status AS ENUM ('End', 'Compromised', 'Expired');\n\n";
extra_db += "DROP TABLE raffle_seen_blocks CASCADE;\n\n";
extra_db += "CREATE TABLE raffle_seen_blocks (hash_ CHAR(53) PRIMARY KEY, level_ BIGINT NOT NULL UNIQUE);\n\n";
extra_db += "CREATE INDEX raffle_seen_blocks_IndexOn_level_ ON raffle_seen_blocks(level_);\n\n";

let sql_infos = prepare_sql_info(raffles, 'autocrawled', extra_db);

if (db) {
    console.log(`\n\n-- <begin: crawling tables for raffle automatically generated by raffle-crawler.js>\n\n${sql_infos.mk_sql_tables}\n-- </end: crawling tables for raffle automatically generated by raffle-crawler.js>`);
} else {
    let info = {
        blocks_table: 'raffle_seen_blocks',
        init_block_hash: fromHead,
        stop_level: stopLevel
    };
    crawler.main(config, sql_infos.contracts_info, info);
}