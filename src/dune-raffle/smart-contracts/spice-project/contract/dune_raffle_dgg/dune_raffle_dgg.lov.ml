#love

(* TODO!!!: protect against the organizer not revealing their secret (or
   having no valid secret or lost it*)

type index = nat
type secret = nat
type nonce = nat
type secretandnonce = secret * nonce
type commitment = bytes
type dgg = nat (* or dun, but nat is better to avoid confusion *)

type winner = address

type state =
  | PlayersRegistration
  | PlayersRevelation
  | FinalDraw
  | End of winner option
  | Compromised
  | Expired (* another kind of compromission *)

type game_id = int

type bytes_or_secret = Commitment of bytes | Revealed (* of secret *)

type game = {
    id : game_id;
    opened_date : timestamp;
    organizer : address;
    organizer_hash : commitment;
    regular_players : (index,address) bigmap;
    commitment_players : (address,bytes_or_secret) bigmap;
    revealed_commitment_players : (index,address) bigmap;
    organizer_secret : secret option;
    current_xor : secret option;
    winning_ticket : index option;
    (* revealed_commitments : secrets; *) (* (index,secret) bigmap *)
    (* block of start of raffle *)
    game_start_level : nat;
    (* index of last registered regular player *)
    last_reg : index;
    (* index of last revealed commitment *)
    last_rev_comm : index;
    number_committed : nat;
    (* state in the automaton: we don't fully trust it and recompute
       it through a view (because not every state change happens
       through an entrypoint), but it's a good basis: for example
       Compromised and End are sinks *)
    last_known_state : state;


    (* In case of compromise, these three fields enable us to reimburse
       players. *)
    remaining_beneficiaries : nat;
    compensation : dun;
    has_withdrawn : (address,bool) bigmap;
    deposits : dun;
  }

type player_info = {
  last : game_id;
  index : index option;
  total : nat;
  won : nat;
}

type players = (address,player_info) bigmap

type params =
  {
    tokens_addr : address;
    (* owner of this contract *)
    owner : address;

    org_is_owner: bool; (* true <-> organizer should be the owner *)
    (* security deposit for adding randomness *)
    player_security_deposit : dun;
    (* security deposit for starting a raffle *)
    organizer_security_deposit : dun;
    (* delay for organizer to reveal after the end of player reveal
       period *)
    organizer_reveal_delay : nat;
    (* equivalent amount of tickets for players who commited a random
       nonce *)
    commitment_bonus_factor : nat;
    (* time alloted for  *)
    registration_time : nat;
    (* allotted time for revelation by players *)
    revelation_time : nat;
    (* (\* address of contract holding tokens *\)
     * tokens_addr : address; *)
    (* reward for winner *)
    winner_reward : dgg;
    (* reward for organizer *)
    organizer_reward : dgg;
    bytes_size : nat; (* Size in bytes of commitments and nonces: smaller values
                         allow to pay less fees and storage, but they are easier
                         to bruteforce. *)
  }

type storage =
  {
    (** Maps of players of commitments **)
    game : game;
    players : players;
    last_raffle : game_id;
    params : params
  }

(* data inserted by organizer when revealing to ease crawling *)
type reveal_data = {
  nb_regular : nat;
  nb_committed : nat;
  nb_revealed_commits : nat;
  inserted_on : timestamp;
  state : state
}


val init_game (hash : commitment) (gid : game_id) : game =
  {
    id = gid;
    opened_date = Current.time();
    organizer = Current.sender();
    organizer_hash = hash;
    regular_players = BigMap.empty [:index] [:address];
    commitment_players = BigMap.empty [:address] [:bytes_or_secret];
    revealed_commitment_players = BigMap.empty [:index] [:address];
    (* revealed_commitments = init_secrets; *)
    organizer_secret = None [:secret];
    current_xor = None [:secret];
    winning_ticket = None [:index];
    game_start_level = Current.level();
    last_reg = 0p;
    last_rev_comm = 0p;
    number_committed = 0p;
    last_known_state = PlayersRegistration;

    remaining_beneficiaries = 0p;
    compensation = 0DUN;
    has_withdrawn = BigMap.empty [:address] [:bool];
    deposits = 0DUN;
  }

val shrink_commit (bytes_size: nat) (commit : bytes) : bytes =
  match Bytes.slice 0p bytes_size commit with
  | None ->
    failwith [:string * nat]
      ("bad commit size. Should be at least ", bytes_size)[:bytes]
  | Some c ->
    c  (*shrink to params.bytes_size bytes*)

val%init storage (params : params) =
  let dummy_com = 0xb4ae78c8f60989223a2ca50240b3324572a1d9a83781e7513700f5a867e0738f02be415606a817e043ec44d335648ea3ced8fdbce2346ba473ba1596ea9500a9 in
  let dummy_com = shrink_commit params.bytes_size dummy_com in
  let game = init_game dummy_com 0 in
  let game = { game with last_known_state = End (None [:address]) } in (* for init only *)
  {
    game = game;
    players = BigMap.empty[:address] [:player_info] ;
    last_raffle = 0;
    params
  }

val get_nat (i : int) : nat =
  match Nat.of_int (i) with
  | None -> failwith [:string * int]
              ("Should not happen, i is negative: ",
               i) [:nat]
  | Some n -> n

contract type Token = sig
  val%entry transfer : (address * nat * bytes option)
end

val transfer_dgg
    (tokens_addr: address) (dest : address) (amount : dgg) (ops : operation list)
  : operation list =
    if amount =[:dgg] 0p then ops
    else
      let tokens_opt = Contract.at<:Token> tokens_addr in
      match tokens_opt with
      | None ->
        failwith [:string * address] ("No Token contract found at ", tokens_addr)
          [:operation list]
      | Some (contract TK : Token)->
        (TK.transfer 0DUN (dest, amount, (None [:bytes]))) :: ops

val transfer_dun
    (deposit : dun) (sender : address) (ops: operation list): operation list =
  if deposit =[:dun] 0DUN then ops
  else (Account.transfer sender deposit) :: ops

val can_register (storage : storage) : bool =
  Current.level ()
    <[:nat]
       storage.game.game_start_level ++ storage.params.registration_time

val can_reveal (storage : storage) : bool =
  let game_start_level = storage.game.game_start_level in
  let reg_time = storage.params.registration_time in
  let rev_time = storage.params.revelation_time in
  let not_too_old =
    Current.level () < [:nat] game_start_level ++ reg_time ++ rev_time in
  not (can_register storage) && not_too_old

val something_to_reveal (storage: storage) : bool =
  storage.game.last_rev_comm <[:nat] storage.game.number_committed

val fail_compromised (_ : unit) : unit =
  failwith [:string]
    "The raffle has been compromised; the organizer's secret was revealed ahead of time. The raffle is canceled and all players can retrieve their funds" [:unit]

val fail_expired (_ : unit) : unit =
  failwith [:string]
    "The raffle has expired; the organizer didn't reveal in time and was denounced. The raffle is canceled and all players can retrieve their funds" [:unit]

val fail_too_late (msg: string) : unit =
  failwith [:string * string]
    ("Too late to perform this operation:", msg) [:unit]

val fail_too_early (msg: string) : unit =
  failwith [:string * string]
    ("Too early to perform this operation:", msg) [:unit]

val check_not_compromised_or_expired (dyn_state : state) : unit =
  match dyn_state with
  | Compromised -> fail_compromised ()
  | Expired -> fail_expired ()
  | _ -> ()

val check_reg_period (dyn_state : state) : unit =
  match dyn_state with
  | PlayersRegistration -> ()
  | Compromised -> fail_compromised ()
  | Expired -> fail_expired ()
  | _ -> fail_too_late "register"

val check_rev_period (dyn_state : state) : unit =
  match dyn_state with
  | PlayersRegistration -> fail_too_early "reveal"
  | PlayersRevelation -> ()
  | Compromised -> fail_compromised ()
  | Expired -> fail_expired ()
  | _ -> fail_too_late "reveal"


val check_last_raffle (storage : storage) (gid : game_id) : unit =
  if not (storage.last_raffle =[:game_id] gid) then
    failwith [:string * game_id * game_id]
      ("Error: you are likely trying to interact with a previous (or future) raffle: (previous, current)",
       gid,storage.last_raffle) [:unit]

(* Check that the sender is not a contract (because otherwise its
   reaction would not be predictable when sending rewards *)
val check_not_a_contract (sender : address) : unit =
  match (Keyhash.of_address sender) with
  | None -> failwith [:string *address]
              ("We're sorry, but only \
                non-contract addresses may \
                play this raffle.",sender) [:unit]
  | Some _ -> ()

val%view check_not_already_played (storage : storage) (gid : game_id) (sender : address) : unit =
  let player_info =
    BigMap.find
      [:address]
      [:player_info]
      sender
      storage.players in
  match player_info with
  | None -> ()
  | Some player_info ->
     if not(player_info.last < [:game_id] gid) then
       failwith [:string * game_id] ("This player has already played in this game of index: ", gid) [:unit]

val check_sufficient_deposit_player (params : params) : unit =
  if Current.amount () < [:dun] params.player_security_deposit then
    failwith [:string * dun * dun]
      ("Amount should be at least the player security deposit ",
       Current.amount (), params.player_security_deposit) [:unit]

(* check that the sender is the owner of the contract *)
val check_is_owner (storage : storage) : unit =
  if  (Current.sender ()) <>[:address] storage.params.owner then
    failwith [:string * address] ("Sender not owner", Current.sender ()) [:unit]

(* check that the sender is the organizer of the current game *)
val check_is_organizer (storage : storage) : unit =
  if  (Current.sender ()) <>[:address] storage.game.organizer then
    failwith [:string * address] ("Sender not organizer", Current.sender ()) [:unit]


val%view compute_state (storage : storage) (_ : unit) : state =
  match storage.game.last_known_state with
  | Compromised -> Compromised
  | Expired -> Expired
  | End w -> End w
  | FinalDraw -> FinalDraw
  | PlayersRevelation ->
    if can_reveal storage && something_to_reveal storage then PlayersRevelation
    else FinalDraw
  | PlayersRegistration ->
    if can_register storage then PlayersRegistration
    else if can_reveal storage && something_to_reveal storage then PlayersRevelation
    else FinalDraw

val update_game_state (storage : storage) : storage =
 { storage with game = {storage.game with last_known_state = compute_state storage ()}}


val create_or_update_player (gid : game_id) (index : index option) (player : address) (players : players) : players =
  match
    BigMap.find
      [:address]
      [:player_info]
      player
      players
  with
  | None ->
     BigMap.add [:address] [:player_info] player
     {last = gid; total = 1p; won = 0p; index = index} players
  | Some pi ->
     BigMap.add [:address] [:player_info] player
     {pi with last = gid; total = pi.total ++ 1p; index = index} players

val update_player_index (index : index option) (player : address) (players : players) : players =
  match
    BigMap.find
      [:address]
      [:player_info]
      player
      players
  with
  | None ->
    failwith [:string] "invariant: not possible" [:players]

  | Some pi ->
     BigMap.add [:address] [:player_info] player
     {pi with index = index} players


val add_regular_player (storage : storage) (player : address) (gid : game_id) : index * storage =
  let game = storage.game in
  let new_index = game.last_reg ++ 1p in
  let new_reg_players =
    BigMap.add [:index] [:address]
      new_index
      player
      game.regular_players
  in
  let new_game = {game with last_reg = new_index; regular_players = new_reg_players} in
  new_index, { storage with game = new_game }

val sub_dun (deposit : dun) (delta : dun) : dun =
  match deposit -$ delta with
  | None ->
    failwith[:string*dun*dun]
      ("invariant: this DUN substraction should not fail. (deposit, delta)=", deposit, delta)[:dun]
  | Some res -> res

val%view available_balance (storage : storage) (() : unit) : dun =
  sub_dun (Current.balance()) storage.game.deposits

val%entry play storage d ((gid,comm_opt) : game_id * bytes option) =
  [%fee (* This entrypoint may pay for fees *)
    if available_balance storage () <[:dun] 1DUN then
      (0DUN, 0p)
    else
      match BigMap.find [:address] [:player_info] (Current.sender()) storage.players with
      | None -> (0.040DUN, 300p)
      | Some pinfo ->
        if pinfo.last =[:int] storage.last_raffle then (0DUN, 0p)
        else (0.040DUN, 150p)
  ]
    check_last_raffle storage gid;
  let storage = update_game_state storage in
  check_reg_period storage.game.last_known_state;
  let sender = Current.sender () in
  check_not_a_contract sender;
  check_not_already_played storage gid sender;
  let ops = transfer_dgg storage.params.tokens_addr sender 1p ([][:operation]) in
  match comm_opt with
  | None ->
    let index, storage = add_regular_player storage sender gid in
    let new_players = create_or_update_player gid (Some index [:index]) sender storage.players in
    let storage = {storage with players = new_players} in
    ops, storage
  | Some comm ->
     check_sufficient_deposit_player storage.params;
     (* at this stage we are sure the play is valid *)
     let new_players = create_or_update_player gid (None [:index]) sender storage.players in
     let storage = {storage with players = new_players} in
     let comm = shrink_commit storage.params.bytes_size comm in
     let game =
       { storage.game with
         deposits = storage.game.deposits +$ storage.params.player_security_deposit }
     in
     let new_commitment_players =
       BigMap.add [:address] [:bytes_or_secret]
         sender
         (Commitment comm)
         game.commitment_players
     in
     let new_game =
       {game with
         commitment_players = new_commitment_players;
         number_committed = game.number_committed ++ 1p} in
     ops,{storage with game = new_game }

val%view compute_hash (st : storage) (v : secretandnonce) : bytes =
  let h = Crypto.sha512 (Bytes.pack [:secretandnonce] v) in
  shrink_commit st.params.bytes_size h

val add_revealed_commitment_player (game : game) (player : address) : index * game =
  let new_index = game.last_rev_comm ++ 1p in
  let new_rev_comm_players =
    BigMap.add [:index] [:address]
      new_index
      player
      game.revealed_commitment_players
  in
  new_index, { game with
    last_rev_comm = new_index;
    revealed_commitment_players = new_rev_comm_players }

val add_secret (game : game) (sender : address) (s : secret) : game =
  let new_commitment_players =
    BigMap.add
      [:address] [:bytes_or_secret]
      sender Revealed game.commitment_players in
  { game with commitment_players = new_commitment_players }

val xor_secret (a : secret) (b : secret) : secret =
  a nlxor b

val xor_secret_opt (a : secret option) (b : secret) : secret =
  match a with
  | None -> b
  | Some a -> a nlxor b

val%entry player_reveal storage d ((gid,v) : game_id * secretandnonce) =
  [%fee (* This entrypoint may pay for fees *)
    if available_balance storage () <[:dun] 1DUN then
      (0DUN, 0p)
    else
      match BigMap.find [:address] [:player_info] (Current.sender()) storage.players with
      | None -> (0.0DUN, 0p)
      | Some pinfo ->
        if pinfo.last =[:int] storage.last_raffle then (0.040DUN, 150p)
        else (0DUN, 0p)
  ]
  check_last_raffle storage gid;
  let storage = update_game_state storage in
  check_rev_period storage.game.last_known_state;
  let sender = Current.sender () in
  let hash = compute_hash storage v in
  let game = storage.game in
  match BigMap.find
          [:address]
          [:bytes_or_secret]
          sender
          game.commitment_players with
  | None -> failwith [:string * address * game_id] ("No registration with commitment was found for (address,game_id)", sender, gid) [:operation list * storage]
  | Some Revealed ->
     failwith [:string * address * game_id] ("Player has already revealed (player,game_id)", sender, gid) [:operation list * storage]
  | Some (Commitment comm) ->
     if hash =[:bytes] comm then
       let secret, nonce = v in
       let new_index, game = add_revealed_commitment_player game sender in
       let game = add_secret game sender secret in
       let game = {game with deposits = sub_dun game.deposits storage.params.player_security_deposit} in
       let new_xor = xor_secret_opt game.current_xor secret in
       let storage = {storage with game = {game with current_xor = Some new_xor [:secret]}} in

       let storage = {storage with players = update_player_index (Some new_index [:index]) sender storage.players } in
       let ops = transfer_dun storage.params.player_security_deposit sender ([][:operation]) in
       ops,
       { storage with
         game = {storage.game with last_known_state = PlayersRevelation} }
     else
       (* slashing *)
       (* [] [:operation],fund_reward storage (Current.amount () +$ storage.params.player_security_deposit) *)
       failwith [:string * bytes * secretandnonce*bytes] ("secret and nonce do not match commitment: (real hash, values, commitment) = ", hash,v,comm) [:operation list * storage]

val organizer_slash (storage : storage) (gid : game_id) (bad_state: state) : storage =
  let game = storage.game in
  let num_players = game.last_reg ++ game.number_committed in
  match storage.params.organizer_security_deposit /$+ num_players with
  | None ->
    { storage with
      game = {game with last_known_state = bad_state; deposits=0DUN} }

  | Some (qdun,_) ->
    (* accept to have an over-approximated 'deposits'.
       Automatically fixed when a new game is inserted *)
(*
    let remaining_deposits =
      match bad_state with
      | Compromised -> game.deposits
      | Expired ->
        let unrevealed_comm = game.number_committed -+ game.last_rev_comm in
        if (unrevealed_comm <[:int] 0) then
          failwith [:string * nat * nat]
            ("invariant: nb inserted commits >= nb revealed commits:",
             game.number_committed, game.last_rev_comm) [:dun]
        else
          sub_dun
            game.deposits
            ((get_nat unrevealed_comm) *+$ storage.params.player_security_deposit)
      | _ -> failwith [:string * state] ("Not a bad state", bad_state) [:dun]
    in*)
    let new_game =
      {game with compensation = qdun;
                 (*deposits = remaining_deposits;*)
                 remaining_beneficiaries = num_players;
                 last_known_state = bad_state} in
    {storage with game = new_game}

val%view can_denounce_organizer (storage:storage) : bool =
  match storage.game.last_known_state with
  | Compromised | Expired | End _ -> false
  | _ ->
    let params = storage.params in
    Current.level() > [:nat]
      storage.game.game_start_level ++
      params.registration_time ++
      params.revelation_time ++
      params.organizer_reveal_delay


val check_is_expected_state (game : game) (rev_data : reveal_data) (computed : state) : unit =
  if rev_data.state <>[:state] computed ||
     rev_data.nb_regular <>[:nat] game.last_reg ||
     rev_data.nb_committed <>[:nat] game.number_committed ||
     rev_data.nb_revealed_commits <>[:nat] game.last_rev_comm ||
     rev_data.inserted_on <>[:timestamp] game.opened_date;
  then
    failwith [:string * reveal_data * state * nat * nat * nat * timestamp]
      ("Expected state is different from computed one:",
       rev_data,
       computed,
       game.last_reg,
       game.number_committed,
       game.last_rev_comm,
       game.opened_date
      )
      [:unit]

val%entry denounce_organizer storage d ((gid, rev_data) : game_id * reveal_data) =
  [%fee (* This entrypoint may pay for fees *)
    check_not_compromised_or_expired storage.game.last_known_state;
    if available_balance storage () >=[:dun] 1DUN &&
       can_denounce_organizer storage
    then
      (0.040DUN, 50p)
    else
      (0.0DUN, 0p)
  ]
  (*let storage = update_game_state storage in not needed*)
  check_not_compromised_or_expired storage.game.last_known_state;
  check_last_raffle storage gid;
  [] [:operation],
  begin
    check_is_expected_state storage.game rev_data Expired;
    if can_denounce_organizer storage then
      organizer_slash storage gid Expired
    else
      failwith
        [:string]
        "Conditions are not met to slash the organizer"
        [: storage]
  end


val check_compromised_or_expired (dyn_state : state) (message : string) : bool =
  match dyn_state with
  | Compromised -> true
  | Expired -> false
  | _ -> failwith [:string * string * state]
           ("This entrypoint may \
             only be called if the \
             current game is in a \
             compromised state, but \
             state is: ",message, dyn_state) [:bool]

val find_player (player : address) (players : players) : player_info =
  match
    BigMap.find
      [:address]
      [:player_info]
      player
      players
  with
  | None -> failwith [:string * address] ("No player found with address, ", player) [:player_info]
  | Some pi -> pi

(* TODO: also reimburse deposits when compromised *)
val%entry reimburse_players storage d ((gid,laddrs) : game_id * address set) =
  [%fee (* This entrypoint may pay for fees *)
    if available_balance storage () >=[:dun] 1DUN &&
       ((storage.game.last_known_state =[:state] Expired ||
         storage.game.last_known_state =[:state] Compromised))
    then
      (0.040DUN, 150p)
    else
      (0.0DUN, 0p)
  ]
  let storage = update_game_state storage in (* not needed *)
  let org_revealed_too_soon = check_compromised_or_expired storage.game.last_known_state
      "Reimbursement may only happen in a 'Compromised' or 'Expired' state" in
  let game = storage.game in
  let new_has_withdrawn =
    Set.fold [:address] [:(address,bool) bigmap]
      (fun (a : address) (map : (address,bool) bigmap) ->
        begin
          match
            BigMap.find [:address] [:bool] a map with
          | None ->
             let pi = find_player a storage.players in
             (* check that this address actually participated in this
                  raffle *)
             if pi.last <> [:game_id] gid then
               failwith [:string * address]
                 ("player did not participate \
                   in this compromised lottery",a)
                 [:(address,bool) bigmap]
             else
               BigMap.add [:address] [:bool] a true map
          | Some b ->
             failwith
               [:string * address]
               ("player has already been compensated",a)
               [:(address,bool) bigmap]
        end)
      laddrs
      game.has_withdrawn in
  let player_sec_deposit =
    if org_revealed_too_soon then storage.params.player_security_deposit
    else 0DUN (* we don't refund players security deposit if they didn't reveal
                 and revletion period expired *)
  in
  let operations, card, deposits =
    Set.fold [:address] [:operation list * nat * dun]
      (fun (a : address) ((ops,card, deposits) : operation list * nat * dun) ->
         let refund_amount =
           match
             BigMap.find [:address] [:bytes_or_secret] a game.commitment_players with
           | Some (Commitment _) -> game.compensation +$ player_sec_deposit
           | _ -> game.compensation
         in
         transfer_dun refund_amount a ops, card ++ 1p, sub_dun deposits refund_amount
      ) laddrs ([] [:operation], 0p, game.deposits) in
  let new_remaining_beneficiaries =
    get_nat (game.remaining_beneficiaries -+ (card)) in
  let new_storage =
    {storage with game =
       {game with
        deposits = deposits;
        has_withdrawn = new_has_withdrawn;
        remaining_beneficiaries = new_remaining_beneficiaries}
    }
  in
  operations,new_storage


val unsafe_sub (a : nat) (b : nat) : nat =
  match Nat.of_int (a -+ b) with
  | None -> failwith [:string * nat * nat]
              ("Should not happen, a - b is negative with (a,b) = ",
               a,b) [:nat]
  | Some res -> res

val check_sufficient_deposit_organizer (params : params) : unit =
  if Current.amount () < [:dun] params.organizer_security_deposit then
    failwith [:string * dun * dun]
      ("Amount should be at least the organizer security deposit",
       Current.balance (),
       params.organizer_security_deposit) [:unit]


val update_player_winner (players : players) (gid : game_id) (player : address)  : players =
  match
    BigMap.find
      [:address]
      [:player_info]
      player
      players
  with
  | None -> failwith [:string * address] ("This is a serious bug: no player found with address, ", player) [:players]
  | Some pi ->
    BigMap.add
      [:address]
      [:player_info]
      player
      {pi with won = pi.won ++ 1p}
      players

(* computes the quotient and remainder (q,r) of the euclidean division
   of a by b, minimizing bureaucracy *)
val euclides (a : nat) (b : nat) : nat * nat =
  match (a /+ b) with
  | None -> failwith [:string * nat * nat]
                        ("Should not happen: modulo failed on a mod b with (a,b) = ",a,b) [:nat * nat]
  | Some (q,r) -> (q,r)

val%view compute_winner (storage : storage) ((org_s, gid) : (secret * game_id))
  : address option * secret * index option =
  let game = storage.game in
  let final_secret = match game.current_xor with
    | None -> org_s
    | Some secret -> xor_secret secret org_s in
  if (game.last_reg =[:nat] 0p && game.last_rev_comm =[:nat] 0p) then
    (* case when no one played, no winner *)
    None [:winner], final_secret, None [:index]
  else
    let nb_lazy = game.last_reg in
    (* Here we are about to do some arithmetic. Say there are l+1 (as
        in lazy) regular players, and z+1 (as in zealous) irregular
        players who revealed their secrets, with b (as in bonus) as a bonus factor. Then we
        compute r := final_secret modulo (l+1) + (z+1)*b. If r < l, then
        regular player of index r wins the raffle. Otherwise, the
        winner is the committed player of index the quotient of (r -
        (l+1)) by b. *)
    begin
      let zealous = game.last_rev_comm in
      let parts_to_distribute = nb_lazy ++ (zealous *+ storage.params.commitment_bonus_factor) in
      let (_q, old_modulo) = euclides final_secret (parts_to_distribute) in
      let modulo = old_modulo ++ 1p in (* to get winner between 1 and max, instead of [0, max - 1 ] *)
      if (modulo <=[:nat] nb_lazy) then
        begin
          match BigMap.find [:index] [:address] modulo game.regular_players with
          | None -> failwith [:string * nat]
                      ("Should not happen: there is no uncommited player of index ind = ",modulo)
                      [:address option * secret * index option]
          | Some addr ->
            Some addr [:address], final_secret, Some modulo [:index]
        end
      else
        begin
          let new_modulo = unsafe_sub old_modulo nb_lazy in
          let (q,_) = euclides new_modulo storage.params.commitment_bonus_factor in
          let q = q ++ 1p in (* indexes start at 1p *)
          begin
            match BigMap.find [:index] [:address] q game.revealed_commitment_players with
            | None -> failwith [:string * nat]
                        ("Should not happen: there is no committed \
                          player of index ind = ", q)
                        [:address option * secret * index option]
            | Some addr -> Some addr [:address], final_secret, Some modulo [:index]
          end
        end
    end


val can_create_new_raffle (storage : storage) : bool =
  match storage.game.last_known_state with (* no need to update state *)
  | PlayersRegistration | PlayersRevelation | FinalDraw -> false
  | End w -> true
  | Compromised | Expired -> storage.game.remaining_beneficiaries =[:nat] 0p

val%view next_available_game_id (storage : storage) (() : unit) : game_id =
  storage.last_raffle + 1

val%entry organizer_insert_new_raffle storage d ((gid,commit) : (game_id * commitment)) =
  let params = storage.params in
  check_sufficient_deposit_organizer params;
  if (params.org_is_owner && Current.sender() <>[:address] params.owner) then
    failwith [:string]
      "The owner is the only authorized to organize raffles with the current params!"
      [:unit];
  let next = next_available_game_id storage () in
  (* This check is added for safety: the organizer should know which
     id it is creating, for internal accounting purposes *)
  if not(gid = [:game_id] next) then
    failwith [: string * game_id * game_id] ("(Wrong id creating new raffle: id, it should be (storage.last_raffle + 1))",gid,next) [:unit];
  if not(can_create_new_raffle storage) then
    failwith [:string] "cannot create new raffle while previous one is not over" [: unit];
  let commit = shrink_commit params.bytes_size commit in
  let game = init_game commit gid in
  let game = {game with deposits = game.deposits +$ params.organizer_security_deposit } in
  [][:operation], {storage with last_raffle = gid; game = game}


val%entry organizer_reveal storage d ((gid,v, rev_data) : (game_id * secretandnonce * reveal_data)) =
  check_last_raffle storage gid;
  check_is_organizer storage;
  let storage = update_game_state storage in
  check_not_compromised_or_expired storage.game.last_known_state;
  let secret, _ = v in
  let params = storage.params in
  let game = match storage.game.organizer_secret with
    | Some _ ->
      failwith [:string] "invariant: cannot have an organizer secret before its revelation" [:game]
    | None ->
      { storage.game with organizer_secret = Some secret [:secret] }
  in
  let storage = { storage with game = game} in
  if storage.game.last_known_state <> [:state] FinalDraw then
    let () = check_is_expected_state storage.game rev_data Compromised in
    let storage = organizer_slash storage gid Compromised in
    [] [:operation],storage
  else
    begin
      let hash = compute_hash storage v in
      if hash =[:bytes] game.organizer_hash then
        let winner, final_secret, winning_ticket = compute_winner storage (secret,gid) in
        let () = check_is_expected_state game rev_data (End winner) in
        let game = {game with
                    deposits = sub_dun game.deposits storage.params.organizer_security_deposit;
                    last_known_state = End winner;
                    current_xor = Some final_secret [:secret];
                    winning_ticket = winning_ticket;
                   } in
        let storage = { storage with game = game} in
        let organizer_reward = params.organizer_reward in
        let ops =
          transfer_dun params.organizer_security_deposit game.organizer ([] [:operation]) in
        let ops =
          transfer_dgg params.tokens_addr game.organizer organizer_reward ops in
        match winner with
        | None -> ops, storage
        | Some winner ->
          let players = update_player_winner storage.players gid winner in
          let storage = {storage with players = players} in
           let winner_reward = params.winner_reward in
           let ops = transfer_dgg params.tokens_addr winner winner_reward ops in
           ops, storage
      else
        failwith [:string * bytes * secretandnonce * bytes]
          ("secret and nonce do not match commitment: (real hash, values, commitment) = ",
           hash,v,game.organizer_hash)
          [:operation list * storage]

    end



(* Withdraw the given amount without compromising organizers or players' legit
   deposits *)
val%entry owner_withdraw_leftover storage d (amount : dun) =
  check_is_owner storage;
  if Current.balance () <[:dun] amount +$ storage.game.deposits then
    failwith [:string * dun * dun * dun]
      ("The owner cannot withdraw the provided amount because it exceeds maximum \
        authorized. (amount, current balance, frozen deposits) = ",
       amount, Current.balance(), storage.game.deposits) [:unit];
  transfer_dun amount (Current.sender ()) ([] [:operation]), storage

val%entry deposit storage d (_ : unit) =
  [][:operation], storage

(* NOT USED

val%view get_regular_players (storage : storage) (gid : game_id) : address list =
  let game = storage.game in
  let i = 0 in
  let j = game.last_reg in
  if j =[:index] -1 then [] [:address] else
  let _, res =
    Loop.loop [:index *
                  address list]
      (fun
         ((i, acc) :
            (int *
               address list)) ->
        (i <[:int] j,
         (i + 1,
          match BigMap.find [:index] [:address] i game.regular_players with
          | None -> failwith [:string * int] ("there should be no holes in the bigmap regular players",i) [:address list]
          | Some a -> a::acc))
      ) (i, [] [:address]) in
  res

val%view get_current_level (st : storage) (() : unit) : nat = Current.level ()


*)
