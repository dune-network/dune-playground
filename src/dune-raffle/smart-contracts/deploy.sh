#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

##
# An alternative to deploy is it use spice + scenario
# spice scenario --nickname dune_raffle_dgg --scenario ./contract/dune_raffle_dgg/scenarios/dune_raffle_minimal.ini
##

called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

# bytes_size = 10 means 80 bits, harder to bruteforce than an int64
FILE=spice-project/contract/dune_raffle_dgg/dune_raffle_dgg.lov.ml

function deploy(){
    name="$1"
    init="$2"

    $DUNE_PLAYGROUND_CLIENT_CMD originate contract ${name}_raffle \
                                transferring 0 from playground_manager \
                                running file:$FILE \
                                --init '#love:'${init}'' \
                                --burn-cap 20 --force --no-locs
}

init_hourly="{registration_time=45p;revelation_time=10p;organizer_reveal_delay=5p;player_security_deposit=0DUN;organizer_security_deposit=5DUN;winner_reward=100p;organizer_reward=0p;commitment_bonus_factor=4p;bytes_size=10p;tokens_addr=\${contract:dgg_token};owner=\${pkh:hourly_raffle_manager};org_is_owner=true}"
init_daily="{registration_time=1200p;revelation_time=210p;organizer_reveal_delay=30p;player_security_deposit=0DUN;organizer_security_deposit=50DUN;winner_reward=2000p;organizer_reward=0p;commitment_bonus_factor=8p;bytes_size=12p;tokens_addr=\${contract:dgg_token};owner=\${pkh:daily_raffle_manager};org_is_owner=true}"
init_weekly="{registration_time=8880p;revelation_time=1080p;organizer_reveal_delay=120p;player_security_deposit=0DUN;organizer_security_deposit=500DUN;winner_reward=20000p;organizer_reward=0p;commitment_bonus_factor=16p;bytes_size=14p;tokens_addr=\${contract:dgg_token};owner=\${pkh:weekly_raffle_manager};org_is_owner=true}"

deploy "hourly" "$init_hourly"
deploy "daily" "$init_daily"
deploy "weekly" "$init_weekly"
