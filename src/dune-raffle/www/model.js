/* global parameters */
/* global nodeRPC */
/*  global metal, utils */

var model_pre = {
    init_state: function(kind, addr) {
        return {

            last_game: null,
            params: null,
            players: null,
            current_bc_lvl: null,
            dynamic_status: null,
            has_played: null,
            has_been_compensated: null,
            has_committed: null,

            players_of_current_game: null,

            user_nb_games: 0,
            user_nb_won: 0,
            user_success: 100,
            user_last_game: -1,
            current_history_page: 1,

            last_move_source: null,
            last_move_hash: null,
            last_move_position: null,
            last_move_entrypoint: null,
            last_move_game_id: null,

            last_commitment: null,
            last_secret: null,
            last_nonce: null,
            last_dn1: null,

            insert_secret: false, // /!\

            inited: false,
            contract_available_balance: 0,
            contract_can_pay_fees: false,
            do_pay_fees: true,

            raffle_address: addr, // initial
            raffle_kind: kind,
            notify_phases_change: false,
            state_change_message: "",
            only_show_my_history: true,

            xhr_info: {
                kt1: addr, // initial
                url: parameters.node,
                block: 'head',
                mk_request: async function(url_prefix, req, appType, data) {
                    let dt = data === null ? null : JSON.stringify(data);
                    let res = await utils.xhr(url_prefix, req, appType, dt, JSON.parse);
                    return res.response;
                }
            },

            ticket_index: null,
        };
    }
}
var model = {

    states: {
        "hourly": model_pre.init_state('hourly', parameters.hourly_raffle),
        "daily": model_pre.init_state('daily', parameters.daily_raffle),
        "weekly": model_pre.init_state('weekly', parameters.weekly_raffle)
    },

    state: null,


    update_user_stats: async function(metalState) {
        var st = null;
        if (metalState !== null && metalState.keyHash !== null) {
            st = await model.state.players.getter(model.state.xhr_info, address_encode, metalState.keyHash);
        }
        if (st === null) {
            model.state.user_last_game = null;
            model.state.user_nb_games = 0;
            model.state.user_nb_won = 0;
            model.state.user_success = 100;
            model.state.has_played = false;
        } else {
            model.state.user_last_game = parseInt(st.last);
            model.state.user_nb_games = parseInt(st.total);
            model.state.user_nb_won = parseInt(st.won);
            model.state.ticket_index = st.index === null ? null : parseInt(st.index);
            let success =
                (model.state.user_nb_games === 0) ?
                100 :
                (model.state.user_nb_won * 100 / model.state.user_nb_games);
            model.state.user_success = success.toFixed(2);
            model.state.has_played = (model.state.user_last_game === model.state.last_game.id);
        }
    },

    update_has_been_compensated: async function(metalState) {
        if (metalState !== null && metalState.keyHash !== null) {
            let has_withdrawn =
                await model.state.last_game.has_withdrawn.getter(model.state.xhr_info, address_encode, metalState.keyHash);
            model.state.has_been_compensated = (has_withdrawn !== null ? has_withdrawn : false);
        } else {
            model.state.has_been_compensated = null;
        }
    },

    update_has_committed: async function(metalState) {
        if (metalState !== null && metalState.keyHash !== null) {
            let res =
                await model.state.last_game.commitment_players.getter(model.state.xhr_info, address_encode, metalState.keyHash);
            if (res === null) {
                model.state.has_committed = { yes: false, has_revealed: true }
            } else {
                switch (res.constr) {
                    case 'Commitment':
                        if (model.state.last_commitment !== res.args[0]) {
                            model.state.last_commitment = res.args[0];
                            model.state.last_secret = null;
                            model.state.last_nonce = null;
                        }
                        model.state.has_committed = { yes: true, has_revealed: false }
                        break;
                    case 'Revealed':
                        model.state.has_committed = { yes: true, has_revealed: true }
                        break;
                    default:
                        model.state.has_committed = { yes: false, has_revealed: true }
                        break;
                }
            }
        } else {
            model.state.has_committed = null;
        }
    },

    __extract_last_move_game_id: function(op_param, op_epoint) {
        var res = 0;
        switch (op_epoint) {
            case 'play':
                res = play_entry_args_decode(op_param);
                return res[0];
            case 'player_reveal':
                res = player_reveal_entry_args_decode(op_param);
                return res[0];
            case 'reimburse_players':
                res = reimburse_players_entry_args_decode(op_param);
                return res[0];
            case 'denounce_organizer':
                res = denounce_organizer_entry_args_decode(op_param);
                return res[0];
            default:
                return 0
        }
    },

    updateStatusOfRecentOp: async function(metalState) {
        if (model.state.last_move_position !== null || !model.state.inited) {
            try {
                // look in mempool, otherwise, OK ? also look in head ?
                //let trackDepth = Math.max(4, model.state.params.lock_player_x_blocks + 2);
                if (metalState === null || metalState.keyHash === null) {
                    throw 'Metal not connected'
                }
                let trackDepth = 3; // XXX
                let last_op = await nodeRpcHelpers.trackOperation(parameters.node, model.state.last_move_hash,
                    metalState.keyHash, model.state.raffle_address, trackDepth);
                let move_game_id = model.__extract_last_move_game_id(last_op.parameter, last_op.entrypoint);
                if (move_game_id !== model.state.last_game.id) {
                    throw 'ignoring operation of old games';
                } else {
                    model.state.last_move_position = last_op.position;
                    model.state.last_move_entrypoint = last_op.entrypoint;
                    model.last_move_game_id = move_game_id;
                    if (model.state.last_move_hash == null) {
                        model.state.last_move_hash = last_op.hash;
                    }
                }
            } catch (error) {
                if (model.state.last_move_source !== model.state.last_dn1) {
                    model.state.last_move_hash = null;
                    model.state.last_move_position = null;
                    model.state.last_move_last_move_game_id = null;
                    model.state.last_move_entrypoint = null;
                }
            }
        }
    },

    contract_available_balance: async function() {
        let bal = await nodeRPC.balance(model.state.raffle_address, parameters.node);
        var avb = (parseInt(bal.response) - model.state.last_game.deposits) / 1000000;
        avb = Math.floor(avb * 100) / 100;
        return avb;
    },

    __state_change_message: function(new_state) {
        switch (new_state) {
            case 'PlayersRegistration':
                return "A new game started. Do you want to participate ?";

            case 'PlayersRevelation':
                return "It's time for players with commitments to reveal their secrets!";

            case 'FinalDraw':
                return "It's time for final draw. Waiting for organizer's secret!";

            case 'End':
                return "Current game just ended!";


            case 'Compromised':
                return "Current game is compromised!";

            case 'Expired':
                return "Current game has expired!";

            default:
                return "unexpected game state";
        }
    },

    light_refresh_all_raffles: async function(current_state) {
        model.state.state_change_message = "";
        if (model.state.xhr_info.block === 'head') {
            let keys = Object.keys(model.states);
            await keys.reduce(async function(acc, kind) {
                await acc;
                var s = model.states[kind];
                s = (s.raffle_kind === current_state.raffle_kind) ? current_state : s;
                if (s.params === null) {
                    let s_storage = await get_storage(s.xhr_info);
                    s.params = s_storage.params; // because we need winner_reward of all games to show them in <select>
                }
                if (s.notify_phases_change) {
                    let old_state = s.dynamic_status;
                    s.dynamic_status = await compute_state_view(s.xhr_info);
                    let is_new_phase = (old_state !== null && s.dynamic_status.constr !== old_state.constr);
                    if (is_new_phase) {
                        let _Kind = kind.charAt(0).toUpperCase() + kind.slice(1);
                        current_state.state_change_message += _Kind + ' Raffle: ' + model.__state_change_message(s.dynamic_status.constr) + "\n\n";
                    }
                }
                model.states[kind] = s;
                return Promise.resolve();
            }, Promise.resolve());
        }
    },

    canPayFees: async function(metalState) {
        model.state.contract_available_balance = await model.contract_available_balance();
        return ((metalState === null || metalState.revealed) && model.state.contract_available_balance >= 2);
    },

    refreshModel: async function() {
        try {
            let metalState = await metalModel.getStateAsync(false);
            await model.light_refresh_all_raffles(model.state);
            let storage = await get_storage(model.state.xhr_info);
            model.state.last_game = storage.game;
            model.state.params = storage.params;
            model.state.players = storage.players;
            model.state.dynamic_status = await compute_state_view(model.state.xhr_info);
            model.state.current_bc_lvl = parseInt((await nodeRPC.head(parameters.node)).response.header.level);
            await model.update_user_stats(metalState);
            await model.update_has_been_compensated(metalState);
            await model.update_has_committed(metalState); // do it before generateSecret because it can setup last_commit
            let new_dn1 = (metalState === null) ? null : metalState.keyHash;
            let force_new_secret = model.state.last_dn1 !== null && (model.state.last_dn1 !== new_dn1);
            await model.generateSecret(force_new_secret); // without forcing if not necessary
            model.state.last_dn1 = new_dn1;
            model.state.players_of_current_game = await model.getPlayersOfGame(model.state.last_game.id);
            await model.updateStatusOfRecentOp(metalState);
            model.state.contract_can_pay_fees = await model.canPayFees(metalState);

            model.state.inited = true;
            return (JSON.parse(JSON.stringify(model.state)));
        } catch (error) {
            throw ('Error: ' + error);
        }
    },

    callEntrypoint: function(metalState, args) {
        return new Promise(
            function(resolve, reject) {
                if (typeof metal === 'undefined' || metal === undefined) {
                    reject(utils.metalNotDetected);
                } else {
                    metal.isEnabled(function(res) {
                        if (!res) {
                            reject(utils.metalNotConfigured);
                        }
                        metal.getAccount(function(r) {
                            let op = {
                                dst: model.state.raffle_address,
                                amount: args.amount,
                                currency: args.currency,
                                network: parameters.network,
                                entrypoint: args.entrypoint,
                                parameter: args.param,
                                collect_call: metalState.revealed && model.state.contract_can_pay_fees && model.state.do_pay_fees,
                                gas_limit: args.gas_limit,
                                storage_limit: args.storage_limit,
                                //type: 'batch',
                                cb: function(res) {
                                    console.log(res);
                                    if (res.ok && res.msg !== 'added to batch') {
                                        model.state.last_move_source = model.state.last_dn1;
                                        model.state.last_move_hash = res.msg;
                                        model.state.last_move_position = -1; // mempool
                                        model.state.last_move_entrypoint = args.entrypoint;
                                    }
                                    resolve(res);
                                }
                            };
                            metal.send(op);
                        });
                    });
                }
            }
        );
    },

    playChoice: async function() {
        let metalState = await metalModel.getStateAsync(true);
        model.saveSecretToLocalStorage();
        let par = model.state.insert_secret ? 'Some 0x' + model.state.last_commitment : 'None';
        let param = '#love:(' + model.state.last_game.id + ',' + par + ')';
        let args = {
            amount: model.state.insert_secret ? `${model.state.params.player_security_deposit}` : '0',
            currency: 'mudun',
            entrypoint: 'play',
            param: param,
            gas_limit: '360000',
            storage_limit: '280'
        };
        res = await model.callEntrypoint(metalState, args);
        return res;
    },

    revealSecret: async function() {
        let metalState = await metalModel.getStateAsync(true);
        let _x = await model.refreshModel(); // to be sure we have the right secret / nonce
        let par = '(' + model.state.last_secret + 'p,' + model.state.last_nonce + 'p)';
        let param = '#love:(' + model.state.last_game.id + ',' + par + ')';
        let args = {
            amount: '0',
            currency: 'mudun',
            entrypoint: 'player_reveal',
            param: param,
            gas_limit: '360000',
            storage_limit: '100'
        };
        res = await model.callEntrypoint(metalState, args);
        return res;
    },

    compensate: async function() {
        let metalState = await metalModel.getStateAsync(true);
        let par = '{' + metalState.keyHash + '}';
        let param = '#love:(' + model.state.last_game.id + ',' + par + ')';
        let args = {
            amount: '0',
            currency: 'mudun',
            entrypoint: 'reimburse_players',
            param: param,
            gas_limit: '360000',
            storage_limit: '100'
        };
        res = await model.callEntrypoint(metalState, args);
        return res;
    },

    denounce: async function() {
        let metalState = await metalModel.getStateAsync(true);
        let last_game = model.state.last_game;
        let nb_regular = parseInt(last_game.last_reg);
        let nb_committed = parseInt(last_game.number_committed);
        let nb_revealed_commits = parseInt(last_game.last_rev_comm);
        let inserted_on = last_game.opened_date;
        //let inserted_on = new Date(null);
        //inserted_on.setUTCSeconds(parseInt(last_game.opened_date));
        //let date = inserted_on.toISOString().split('.')[0] + "Z";
        //let par = `{state=Expired;nb_regular=${nb_regular}p;nb_committed=${nb_committed}p;nb_revealed_commits=${nb_revealed_commits}p;inserted_on=${date}}`;
        //let param = '#love:(' + model.state.last_game.id + ',' + par + ')';
        // Using the JSON param version because there is a bug currently in the parser of timestamps in Metal
        let param = `{
            "dune_expr": {
                "tuple": [{ "int": "${last_game.id}" },
                    {
                        "record": {
                            "state": { "constr": ["Expired", null] },
                            "nb_regular": { "nat": "${nb_regular}" },
                            "nb_committed": { "nat": "${nb_committed}" },
                            "nb_revealed_commits": { "nat": "${nb_revealed_commits}" },
                            "inserted_on": { "time": "${inserted_on}" }
                        }
                    }
                ]
            }
        }`;
        let args = {
            amount: '0',
            currency: 'mudun',
            entrypoint: 'denounce_organizer',
            param: param,
            gas_limit: '360000',
            storage_limit: '0'
        };
        res = await model.callEntrypoint(metalState, args);
        return res;
    },

    getGamesFromDB: function(page, size) {
        return new Promise(
            function(resolve, reject) {
                var addr = 'all'; // we assume we'll show all history
                if (model.state.only_show_my_history) {
                    let metalState = metalModel.getState();
                    if (metalState === null || metalState.keyHash === null) {
                        resolve([]);
                    }
                    addr = metalState.keyHash; // update with my keyHash
                }
                utils.xhr(parameters.apiEndpoint, "/raffle-games/" + addr + "?page=" + page + "&size=" + size + "&kind=" + model.state.raffle_kind).then(function(res) {
                        if (res.status === 200) { // post request succeded
                            let resp = JSON.parse(res.response);
                            result = [];
                            resp.forEach(e => {
                                let g = {
                                    block_hash: e.block_hash_,
                                    game_id: e.game_id_,
                                    result: e.result_ === 'End' ? 'Ended' : e.result_, // quick fix before fixing in smart contract
                                    winner: e.winner_, // because null is stored as string in DB
                                    nb_regular: e.nb_regular_,
                                    nb_committed: e.nb_committed_,
                                    nb_revealed_commits: e.nb_revealed_commits_,
                                    inserted_on: e.inserted_on_
                                };
                                result.push(g);
                            });
                            resolve(result);
                        } else {
                            reject(res.response);
                        }
                    },
                    function(err) { reject(utils.networkError(err)) });
            });
    },

    getPlayersOfGame: function(game_id) {
        return new Promise(
            function(resolve, reject) {
                utils.xhr(parameters.apiEndpoint, "/raffle-players/" + game_id + "?kind=" + model.state.raffle_kind).then(function(res) {
                    if (res.status === 200) { // post request succeded
                        let resp = JSON.parse(res.response);
                        result = [];
                        resp.forEach(e => {
                            let g = {
                                player: e.player_,
                                commit: e.commit_,
                                secret: e.secret_
                            };
                            result.push(g);
                        });
                        resolve(result);
                    } else {
                        reject(res.response);
                    }
                }, function(err) { reject(utils.networkError(err)) });
            });
    },

    getHistoryPage: function(hpage) {
        return new Promise(
            function(resolve, reject) {
                if (hpage !== undefined) {
                    model.state.current_history_page = hpage;
                }
                model.getGamesFromDB(model.state.current_history_page - 1, 10).then(function(result) { //TODO put parameters
                    resolve(result);
                }, function(err) {
                    reject(err);
                });
            });
    },

    nbHistoryPages: function() {
        if (model.state.only_show_my_history) {
            return Math.ceil(model.state.user_nb_games / 10);
        } else {
            return Math.ceil(model.state.last_game.id / 10);
        }
    },

    swapInsertSecret: async function() {
        model.state.insert_secret = !model.state.insert_secret;
        return;
    },

    mk_storage_key(whichCommit, keyHash) {
        let game_id = model.state.last_game.id;
        return (whichCommit + '-' + game_id + "-" + model.state.raffle_address + '-' + keyHash);
    },

    saveSecretToLocalStorage: function() {
        localStorage.setItem(model.state.last_commitment, `${model.state.last_secret}:${model.state.last_nonce}`);
    },

    __forceGenerateSecret: async function() {
        let max = Date.now();
        model.state.last_secret = Math.floor((Math.random()) * max);
        model.state.last_nonce = Math.floor(Math.random() * max);
        model.state.last_commitment = await compute_hash_view(model.state.xhr_info, [model.state.last_secret, model.state.last_nonce]);
        //model.saveSecretToLocalStorage(); do not systymatically save secrets to avoid memory/space leak
    },

    __restoreSecretFromStorage: async function() {
        if (model.state.last_commitment === null || model.state.last_commitment === undefined) {
            return false;
        }
        let res = localStorage.getItem(model.state.last_commitment);
        try {
            let arr = res.split(':');
            if (arr.length === 2 && arr[0] !== undefined && arr[1] !== undefined) {
                model.state.last_secret = arr[0];
                model.state.last_nonce = arr[1];
            }
            return (model.state.last_commitment !== null &&
                model.state.last_nonce !== null &&
                model.state.last_secret !== null);
        } catch (error) {
            return false;
        }
    },

    generateSecret: async function(force) {
        if (force) {
            await model.__forceGenerateSecret();
        } else {
            if (model.state.last_commitment !== null &&
                model.state.last_nonce !== null &&
                model.state.last_secret !== null) {
                return;
            } else {
                let restored = await model.__restoreSecretFromStorage();
                if (restored) {
                    return;
                } else {
                    await model.__forceGenerateSecret();

                }
            }
        }
    },

    swapPayFees: function() {
        model.state.do_pay_fees = !model.state.do_pay_fees;
        localStorage.setItem(model.state.raffle_kind + "-do-pay-fees", model.state.do_pay_fees);
        return;
    },

    swapNotifyPhasesChange: function() {
        model.state.notify_phases_change = !model.state.notify_phases_change;
        localStorage.setItem(model.state.raffle_kind + "-notify-phases-change", model.state.notify_phases_change);
        return;
    },

    changeRaffleKind: function(kind) {
        if (model.state !== null) {
            if (model.state.raffle_kind === kind) {
                return;
            }
            model.states[model.state.raffle_kind] = model.state; // save old state
        }
        localStorage.setItem("dune-raffle-last-kind", kind);
        model.state = model.states[kind]; // save old state
        model.state.xhr_info.kt1 = model.state.raffle_address;
    },

    changeHistoryFilter: function(history_filter) {
        if (history_filter === 'all') {
            model.state.only_show_my_history = false;
        } else {
            model.state.only_show_my_history = true;
        }
        localStorage.setItem(model.state.raffle_kind + "-" + 'only-show-my-history', model.state.only_show_my_history);
    },

    setDesiredGame: function(blockHash) {
        model.state.xhr_info.block = blockHash;
    },

    __bool_of_string: function(b, deflt) {
        switch (b) {
            case 'true':
                return true
            case 'false':
                return false
            default:
                return deflt;
        }
    }
}

__toplevel__raffle__model__init__ = function() {
    ['hourly', 'daily', 'weekly'].forEach(kind => {
        var do_pay_fees = localStorage.getItem(kind + "-do-pay-fees");
        var alertchanges = localStorage.getItem(kind + "-notify-phases-change");
        var only_show_my_history = localStorage.getItem(kind + "-" + 'only-show-my-history');
        do_pay_fees = model.__bool_of_string(do_pay_fees, true);;
        alertchanges = model.__bool_of_string(alertchanges, false);
        only_show_my_history = model.__bool_of_string(only_show_my_history, true);
        let s = model.states[kind];
        s.notify_phases_change = alertchanges;
        s.do_pay_fees = do_pay_fees;
        s.only_show_my_history = only_show_my_history;
        model.states[kind] = s;
    });
    var kind = localStorage.getItem("dune-raffle-last-kind");
    kind = (kind !== 'daily' && kind !== 'weekly') ? "hourly" : kind;
    model.changeRaffleKind(kind);
}

var __do__toplevel__raffle__model__init__ = __toplevel__raffle__model__init__()