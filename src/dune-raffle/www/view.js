/* global utils */


var view = {

    pendingAnimation: function() {
        return ('<img src="images/loader.gif" style="height:30px"></img>');
    },

    set_nb_tickets_per_commit: function(data) {
        let nb_tickets = parseInt(data.params.commitment_bonus_factor);
        utils.setHTML('nb-tickets-per-commit-1', nb_tickets);
        utils.setHTML('nb-tickets-per-commit-2', nb_tickets);
    },

    highlight_my_addr: function(data, addr) {
        return (metalModel.isMine(addr)) ? ('<span style="width:20px;margin: 0px; padding: 2px 5px" class="alert alert-success">' + addr + '</span>') : addr;
    },

    list_of_participants: function(data) {
        let res = data.players_of_current_game;
        //utils.setHTML('number-of-participants', res.length);
        if (res.length === 0) {
            utils.setHTML('list-of-participants', '');
        } else {
            var msg = "";
            res.forEach(function(row) {
                var player = view.highlight_my_addr(data, row.player);
                player = utils.explorerLink(row.player, player, true);
                msg += "<tr>";
                msg += "<td>" + player + " </td>";
                msg += "<td>" + (row.commit === null ? '--' : row.commit) + " </td>";
                if (row.commit) {
                    msg += "<td>" + ((row.secret === null) ? 'Not&nbsp;revealed' : row.secret) + " </td>";
                } else {
                    msg += "<td>--</td>";
                }
                msg += "</tr>";
            });
            utils.setHTML('list-of-participants', msg);
        }
    },

    ended_phase: function(p_id, error) {
        let pid = "phase-" + p_id;
        utils.setHTML(pid + "-info", "&nbsp;");
        var bar_id = pid + "-bar";
        var bar_elt = document.getElementById(bar_id);
        bar_elt.aria_valuenow = '100';
        bar_elt.style.width = '100%';
        utils.setHTML(bar_id, '100 %');
        bar_elt.classList.remove('progress-bar-animated');
        bar_elt.classList.remove('bg-info');
        bar_elt.classList.remove('bg-danger');
        bar_elt.classList.remove('bg-success');
        ended_class = error ? 'bg-danger' : 'bg-success';
        bar_elt.classList.add(ended_class);
        document.getElementById(pid + '-title').style.color = '#666';
        document.getElementById(pid).style.opacity = 1;
    },

    current_phase: function(p_id, percentage, details) {
        let pid = "phase-" + p_id;
        utils.setHTML(pid + "-info", details);
        var bar_id = pid + "-bar";
        var bar_elt = document.getElementById(bar_id);
        bar_elt.aria_valuenow = percentage;
        bar_elt.style.width = percentage + '%';
        utils.setHTML(bar_id, percentage + '%');
        bar_elt.classList.remove('bg-danger');
        bar_elt.classList.remove('bg-success');
        bar_elt.classList.add('progress-bar-animated');
        bar_elt.classList.add('bg-info');
        document.getElementById(pid + '-title').style.color = '#000';
        document.getElementById(pid).style.opacity = 1;

    },

    pending_phase: function(p_id) {
        let pid = "phase-" + p_id;
        utils.setHTML(pid + "-info", "&nbsp;");
        var bar_id = pid + "-bar";
        var bar_elt = document.getElementById(bar_id);
        bar_elt.aria_valuenow = '0';
        bar_elt.style.width = '0%';
        utils.setHTML(bar_id, '0%');
        bar_elt.classList.remove('bg-success');
        bar_elt.classList.remove('progress-bar-animated');
        bar_elt.classList.remove('bg-info');
        document.getElementById(pid + '-title').style.color = '#666';
        document.getElementById(pid).style.opacity = 0.4;
    },

    hidePhasesDetails: function() {
        let phases = [
            // Initial phase and its sub-divs/categories
            'game-initial-phase',
            'game-initial-phase-playing',
            'game-initial-phase-submitting',
            'game-initial-phase-waiting-1',
            'game-initial-phase-waiting-2',
            'game-initial-phase-waiting-3',

            // Reveal phase and its sub divs/categories
            'game-reveal-phase-revealing',
            'game-reveal-phase-submitting',
            'game-reveal-phase-waiting-1',
            'game-reveal-phase-waiting-2',
            'game-reveal-phase-waiting-3',
            // sub cases of Expired state
            'game-compromised-phase-no-compensation',
            'game-compromised-phase-to-compensate',
            'game-compromised-phase-to-compensate-not-played',
            'game-compromised-phase-compensating-1',
            'game-compromised-phase-compensating-2',
            'game-compromised-phase-compensated',
            'game-compromised-phase-early',
            'game-compromised-phase-late',
            // sub cases of end
            'game-ended-phase-with-winner',
            'game-ended-phase-no-winner',

            //finalize and its sub-cases
            'game-finalize-phase',
            'game-finalize-phase-to-denounce',
            'game-finalize-phase-denouncing-1',

            'game-reveal-phase',
            'game-ended-phase',
            'game-compromised-phase',
            'game-unexpected-phase'

        ];
        phases.forEach(utils.hideElt);
    },

    fill_secrets_fields: function(prefix, data) {
        if (data.last_commitment !== null && data.last_nonce !== null && data.last_secret !== null) {
            utils.setValue(prefix + 'playing-commitment', data.last_commitment);
            utils.setValue(prefix + 'playing-nonce', data.last_nonce);
            utils.setValue(prefix + 'playing-secret', data.last_secret);
        }
    },

    gameInitial: function(data, id) {
        if (data.last_move_position !== null && data.last_move_entrypoint === 'play') {
            if (data.last_move_position < 0) {
                console.assert(data.last_move_hash !== null);
                var op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                utils.setHTML('game-initial-phase-submitting-op-hash', op_hash);
                return 'game-initial-phase-submitting';
            } else {
                console.assert(data.last_move_hash !== null);
                var op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                utils.setHTML('game-initial-phase-waiting-1-op-hash', op_hash);
                console.assert(data.last_move_position !== null);
                utils.setHTML('game-initial-phase-waiting-1-block', view.computeETA(data.last_move_position));
                return 'game-initial-phase-waiting-1';
            }
        } else {
            if (data.has_played) {
                if (data.has_committed.yes) {
                    return 'game-initial-phase-waiting-3';
                } else {
                    return 'game-initial-phase-waiting-2';
                }
            } else {
                view.fill_secrets_fields('game-initial-phase-', data);
                if (data.insert_secret) {
                    document.getElementById('game-initial-phase-secret-explanation').style.display = 'block';
                    document.getElementById('insert-secret-input').checked = true;
                } else {
                    document.getElementById('game-initial-phase-secret-explanation').style.display = 'none';
                    document.getElementById('insert-secret-input').checked = false;
                }
                return 'game-initial-phase-playing';
            }
        }
    },

    gamePlayersReveal: function(data) {
        if (data.last_move_position !== null && data.last_move_entrypoint === 'player_reveal') {
            if (data.last_move_position < 0) {
                console.assert(data.last_move_hash !== null);
                var op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                utils.setHTML('game-reveal-phase-submitting-op-hash', op_hash);
                return 'game-reveal-phase-submitting';
            } else {
                console.assert(data.last_move_hash !== null);
                var op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                utils.setHTML('game-reveal-phase-waiting-1-op-hash', op_hash);
                console.assert(data.last_move_position !== null);
                utils.setHTML('game-reveal-phase-waiting-1-block', view.computeETA(data.last_move_position));
                return 'game-reveal-phase-waiting-1';
            }
        } else {
            if (data.has_played && data.has_committed.yes) {
                if (data.has_committed.has_revealed) {
                    return 'game-reveal-phase-waiting-2';
                } else {
                    view.fill_secrets_fields('game-reveal-phase-', data);
                    return 'game-reveal-phase-revealing';
                }
            } else {
                // player didn't play in this game, or played without commit
                return 'game-reveal-phase-waiting-3';
            }
        }
    },

    gameFinalize: function(data, id, expiring) {
        if (!expiring) {
            return null;
        } else {
            if (data.last_move_position !== null && data.last_move_entrypoint === 'denounce_organizer') {
                console.assert(data.last_move_hash !== null);
                var op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                utils.setHTML('game-finalize-phase-denouncing-1-op-hash', op_hash);
                return 'game-finalize-phase-denouncing-1';
            } else {
                return 'game-finalize-phase-to-denounce';
            }
        }
    },


    gameEnded: function(data, status, id, reward) {
        utils.setHTML('game-ended-reward', reward);
        let w = status.args[0];
        if (w === null) {
            return ('game-ended-phase-no-winner');
        } else {
            var winner_addr = null;
            if (metalModel.isMine(w)) {
                let winner = '<span style="width:20px;margin: 0px; padding: 2px 5px" class="alert alert-success">' + w + '</span>';
                winner_addr = utils.explorerLink(w, winner, true) + " (You)";
            } else {
                winner_addr = utils.explorerLink(w, w, true);
            }
            utils.setHTML('game-ended-winner', winner_addr);
            return ('game-ended-phase-with-winner');
        }
    },

    gameCompromised: function(data, expired) {
        var to_show = [];
        let compensation = parseInt(data.last_game.compensation) / 1000000;
        if (expired) {
            utils.setHTML('game-compromised-phase-deposits-refund-0', "");
            utils.setHTML('game-compromised-phase-deposits-refund-1', "");
            utils.setHTML('game-compromised-phase-deposits-refund-2', "");
            utils.setHTML('game-compromised-phase-deposits-refund-3', "");
            utils.setHTML('game-compromised-phase-deposits-refund-4', "");
            to_show.push('game-compromised-phase-late');

        } else {
            let extra = "<br/>Deposits of players who didn't reveal their secrets are also refunded.";
            utils.setHTML('game-compromised-phase-deposits-refund-0', extra);
            utils.setHTML('game-compromised-phase-deposits-refund-1', extra);
            utils.setHTML('game-compromised-phase-deposits-refund-2', extra);
            utils.setHTML('game-compromised-phase-deposits-refund-3', extra);
            utils.setHTML('game-compromised-phase-deposits-refund-4', extra);
            to_show.push('game-compromised-phase-early');
        }
        //utils.setHTML('game-compromised-reward', compensation);
        if (compensation === 0) {
            to_show.push('game-compromised-phase-no-compensation');
        } else {
            //return ('game-compromised-phase-to-compensate');
            if (data.last_move_position !== null && data.last_move_entrypoint === 'reimburse_players') {
                if (data.last_move_position < 0) {
                    console.assert(data.last_move_hash !== null);
                    var op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                    utils.setHTML('game-compromised-phase-compensating-1-reward', compensation);
                    utils.setHTML('game-compromised-phase-compensating-1-op-hash', op_hash);
                    to_show.push('game-compromised-phase-compensating-1');
                } else {
                    console.assert(data.last_move_hash !== null);
                    var op_hash = utils.explorerLink(data.last_move_hash, data.last_move_hash, true);
                    utils.setHTML('game-compromised-phase-compensating-2-reward', compensation);
                    utils.setHTML('game-compromised-phase-compensating-2-op-hash', op_hash);
                    console.assert(data.last_move_position !== null);
                    utils.setHTML('game-compromised-phase-compensating-2-block', view.computeETA(data.last_move_position));
                    to_show.push('game-compromised-phase-compensating-2');
                }
            } else {
                if (data.has_been_compensated) {
                    utils.setHTML('game-compromised-phase-compensated-reward', compensation);
                    to_show.push('game-compromised-phase-compensated');
                } else {
                    if (data.has_played) {
                        utils.setHTML('game-compromised-phase-to-compensate-reward', compensation);
                        to_show.push('game-compromised-phase-to-compensate');
                    } else {
                        utils.setHTML('game-compromised-phase-to-compensate-not-played-reward', compensation);
                        to_show.push('game-compromised-phase-to-compensate-not-played');

                    }
                }
            }
        }
        return to_show;
    },

    computeETA: function(nbBlocks) {
        let seconds = nbBlocks * parameters.secondsBetweenBlocks;
        if (seconds < 60) {
            if (seconds === 0) {
                return "a few seconds"
            } else {
                return seconds + " seconds"
            }
        } else {
            let minutes = Math.ceil(seconds / 60);
            if (minutes < 60) {
                return (minutes + " minute(s)");
            } else {
                let hours = Math.floor(minutes / 60);
                if (hours < 24) {
                    let rem_minutes = minutes - hours * 60;
                    return (hours + " h " + rem_minutes + " m");
                } else {
                    let days = Math.floor(hours / 24);
                    let rem_hours = hours - days * 24;
                    return (days + " d " + rem_hours + " h");
                }
            }
        }
    },


    addRow: function(data, rows, g) {
        rows = rows + "<tr>";
        var color = "";
        switch (g.result) {
            case 'Compromised':
                color = 'alert-danger';
                break;
            case 'Expired':
                color = 'alert-warning';
                break;
            case 'Ended':
                color = 'alert-success';
                break;
            default:
                break;
        }
        let result = '<span style="width:130px; margin: auto; padding: 2px 0px" class="alert ' + color + '">' + g.result + '</span>';
        let inserted_on = new Date(null);
        //inserted_on.setUTCSeconds(parseInt(g.inserted_on));
        inserted_on.setUTCSeconds(parseInt(g.inserted_on));
        let date = inserted_on.toISOString().split('.')[0];
        let tmp = date.split('T');
        //let day = tmp[0].replaceAll('-', '/'); This doesn't work on all browsers
        let day = tmp[0].replace(/-/g, '/');

        let times = tmp[1].split(':');
        let time = `${times[0]}:${times[1]}`;
        let tickets = g.nb_regular + data.params.commitment_bonus_factor * g.nb_revealed_commits;

        let game_id = `<a style="cursor: pointer" onclick="controller.showGame('${g.block_hash}');">${g.game_id}</a>`;
        rows += "<td>" + game_id + "</span></td>";
        rows += "<td style='letter-spacing: 0px;'>" + day + "&nbsp;@&nbsp;" + time + "UTC</td>";
        rows += "<td>" + result + "</td>";
        rows += "<td>" + (g.nb_regular + g.nb_committed) + "</td>";
        rows += "<td>" + g.nb_committed + " / " + g.nb_revealed_commits + "</td>";
        rows += "<td>" + tickets + "</td>";
        if (g.winner === null) {
            rows += "<td>No&nbsp;winner </td>";
        } else {
            let winner = view.highlight_my_addr(data, g.winner);
            let addr = utils.explorerLink(g.winner, winner, true);
            rows += "<td style='letter-spacing: 0px;'>" + addr + "</td>";
        }
        rows += "</tr>";

        return rows;
    },

    updatePageAux: function(data, dt, history_page) {
        // player stats
        utils.setHTML('play-self-finished', data.user_nb_games);
        utils.setHTML('play-self-nb-won', data.user_nb_won);
        utils.setHTML('play-self-success-rate', data.user_success);

        // default
        //document.getElementById('reveal-pubkey').style.display = 'none';
        document.getElementById('pay-fees').style.display = 'block';
        document.getElementById('get-free-duns').style.display = 'none';
        document.getElementById('notify-phases-change-checkbox').checked = data.notify_phases_change;
        document.getElementById('collectCall').checked = false;
        document.getElementById('collectCall').disabled = true;

        if (data.contract_can_pay_fees) {
            document.getElementById('collectCall').checked = data.do_pay_fees;
            document.getElementById('collectCall').disabled = false;
        }
        let metalState = metalModel.getState();
        if (metalState === null || !metalState.revealed || metalState.dun_balance === 0) {
            document.getElementById('get-free-duns').style.display = 'inline-block';
        }

        var rows = "";
        for (i = 0; i < dt.length; i++) {
            let g = dt[i];
            // skip the game if it has been aaded above as pending/ongoing
            rows = view.addRow(data, rows, g);

        }
        utils.setHTML('raffle-history-games-list', rows);
        utils.setHTML('history-page-number', 'Page ' + history_page);
    },

    updatePrizes: function() {
        // todo: do not access model directly
        Object.keys(model.states).forEach(kind => {
            let data = model.states[kind];
            var _Kind = data.raffle_kind.charAt(0).toUpperCase() + data.raffle_kind.slice(1);
            let txt = `${_Kind} Raffle &nbsp;&nbsp;- &nbsp;&nbsp; ${data.params.winner_reward} DGG prize`;
            utils.setHTML(kind + '-prize', txt);
        });
    },

    updatePage: function(data, dt, history_page) {
        view.updatePrizes();
        let last_game = data.last_game;
        let last_game_id = parseInt(last_game.id);
        let reward = data.params.winner_reward;
        let curr_lvl = data.current_bc_lvl;

        //let status = last_game.state; --> not accurate, it's better to use view
        let status = data.dynamic_status;
        var to_show = "";
        var phase_msg = "";
        var status_msg = "";
        var should_compensate = false;
        var ticket_color = 'bg-info';

        let live_game = (data.xhr_info.block === 'head');

        switch (status.constr) {
            case 'PlayersRegistration':
                phase_msg = "A new game started. Do you want to participate ?";
                var reg_time = parseInt(data.params.registration_time);
                var started = parseInt(last_game.game_start_level);
                var end = started + reg_time;
                var diff = end - curr_lvl;
                var perc = Math.round(Math.max(1, (reg_time - diff) * 100 / reg_time));
                view.current_phase(1, perc, "ETA: " + view.computeETA(diff));
                view.pending_phase(2);
                view.pending_phase(3);
                var sub_div = view.gameInitial(data, last_game_id);
                to_show = ['game-initial-phase', sub_div];
                status_msg = "Open for registration";
                break;

            case 'PlayersRevelation':
                phase_msg = "It's time for players with commitments to reveal their secrets!";
                var reveal_time = parseInt(data.params.revelation_time);
                var started = parseInt(last_game.game_start_level) + parseInt(data.params.registration_time);
                var end = started + reveal_time;
                var diff = end - curr_lvl;
                var perc = Math.round(Math.max(1, (reveal_time - diff) * 100 / reveal_time));
                view.ended_phase(1, false);
                view.current_phase(2, perc, "ETA: " + view.computeETA(diff));
                view.pending_phase(3);
                var sub_div = view.gamePlayersReveal(data);
                to_show = ['game-reveal-phase', sub_div];
                status_msg = "Players revelation time";
                break;

            case 'FinalDraw':
                phase_msg = "It's time for players with commitments to reveal their secrets!";
                var org_rev_delay = parseInt(data.params.organizer_reveal_delay);
                var started = parseInt(last_game.game_start_level) + parseInt(data.params.registration_time) + parseInt(data.params.revelation_time);
                var end = started + org_rev_delay;
                var diff = end - curr_lvl;
                var perc = Math.round(Math.max(1, (org_rev_delay - diff) * 100 / org_rev_delay));
                view.ended_phase(1, false);
                view.ended_phase(2, false);
                var sub_div = [];
                if (perc <= 100) {
                    view.current_phase(3, perc, "ETA: " + view.computeETA(diff));
                    sub_div = view.gameFinalize(data, last_game_id, false);
                } else {
                    view.current_phase(3, 100, "Expiring...");
                    sub_div = view.gameFinalize(data, last_game_id, true);
                }
                to_show = (sub_div === null) ? ['game-finalize-phase'] : ['game-finalize-phase', sub_div];
                status_msg = "Organizers revelation time";
                break;

            case 'End':
                phase_msg = "Current game just ended!";
                view.ended_phase(1, false);
                view.ended_phase(2, false);
                view.ended_phase(3, false);
                var sub_div = view.gameEnded(data, status, last_game_id, reward);
                to_show = ['game-ended-phase', sub_div];
                status_msg = "Successfully ended";
                if (metalModel.isMine(status.args[0])) {
                    ticket_color = 'bg-success';
                } else {
                    ticket_color = 'bg-danger';

                };
                break;

            case 'Compromised':
                phase_msg = "Current game is compromised!";
                view.ended_phase(1, true);
                view.ended_phase(2, true);
                view.ended_phase(3, true);
                var sub_divs = view.gameCompromised(data, false);
                sub_divs.push('game-compromised-phase');
                to_show = sub_divs;
                status_msg = "Compromised";
                should_compensate = true;
                break;

            case 'Expired':
                phase_msg = "Current game has expired!";
                view.ended_phase(1, true);
                view.ended_phase(2, true);
                view.ended_phase(3, true);
                var sub_divs = view.gameCompromised(data, true);
                sub_divs.push('game-compromised-phase');
                to_show = sub_divs;
                status_msg = "Expired";
                should_compensate = true;
                break;

            default:
                view.ended_phase(1, true);
                view.ended_phase(2, true);
                view.ended_phase(3, true);
                utils.setHTML('game-unexpected-game-state', JSON.stringify(status));
                to_show = ['game-unexpected-phase'];
                status_msg = "(Unexpected status)";
                break;
        }
        utils.setValue('raffle-kind', data.raffle_kind);
        utils.setValue('history-filter', data.only_show_my_history ? 'mine' : 'all');

        utils.setHTML('kt1-available-balance', '~ ' + data.contract_available_balance);
        view.hidePhasesDetails();
        to_show.forEach(e => utils.showElt(e));
        view.list_of_participants(data);
        view.updatePageAux(data, dt, history_page);

        if (data.state_change_message !== "") {
            alert(data.state_change_message);
        }
        let _Kind = data.raffle_kind.charAt(0).toUpperCase() + data.raffle_kind.slice(1);
        utils.setHTML('raffle-number', last_game.id);
        utils.setHTML('raffle-id', last_game.id);
        utils.setHTML('raffle-kind-2', _Kind);
        utils.setHTML('raffle-kind-3', _Kind);
        view.set_nb_tickets_per_commit(data);

        utils.setHTML('raffle-kind-params', _Kind);
        utils.setHTML("organizer-security-deposit", parseInt(data.params.organizer_security_deposit) / 1000000);

        utils.setHTML("player-security-deposit", parseInt(data.params.player_security_deposit) / 1000000);
        utils.setHTML("player-security-deposit-2", parseInt(data.params.player_security_deposit) / 1000000);

        let commitment_bonus_factor = parseInt(data.params.commitment_bonus_factor);

        utils.setHTML("nb-tickets-per-commit", commitment_bonus_factor);
        utils.setHTML("organizer-reward", data.params.organizer_reward);
        utils.setHTML("winner-reward", data.params.winner_reward);

        utils.setHTML("raffle-status", status_msg);
        let nb_regular = parseInt(last_game.last_reg);
        let nb_committed = parseInt(last_game.number_committed);
        let nb_revealed = parseInt(last_game.last_rev_comm);
        let nb_players = nb_regular + nb_committed;

        utils.setHTML('num-of-players', nb_players);
        utils.setHTML('regular-players', nb_regular);
        utils.setHTML('inserted-commits', nb_committed);
        utils.setHTML('revealed-commits', nb_revealed);
        utils.setHTML('num-of-tickets', nb_regular + nb_revealed * commitment_bonus_factor);
        if (should_compensate) {
            let remaining_beneficiaries = parseInt(last_game.remaining_beneficiaries);
            let num_of_compensated = nb_players - remaining_beneficiaries;
            utils.setHTML('num-of-compensated', num_of_compensated + ' / ' + nb_players);
            utils.showElt('current-game-compensated-column');
        } else {
            utils.setHTML('num-of-compensated', '--');
            utils.hideElt('current-game-compensated-column');
        }

        //alert(JSON.stringify(last_game));

        let org = utils.explorerLink(last_game.organizer, last_game.organizer, true);
        utils.setHTML('org-address', org);
        utils.setHTML('org-commit', last_game.organizer_hash);
        utils.setHTML('org-secret', last_game.organizer_secret === null ? 'Not&nbsp;revealed' : last_game.organizer_secret);
        utils.setHTML('combined-secret', last_game.current_xor === null ? '?' : last_game.current_xor);
        if (last_game.winning_ticket === null) {
            utils.setHTML('winning-ticket', should_compensate ? 'No winner' : 'No winner yet');
            utils.hideElt('winning-ticket-main-page');
        } else {
            utils.setHTML('winning-ticket', 'Number ' + last_game.winning_ticket);
            utils.setHTML('winning-ticket-main-page-value', 'Number ' + last_game.winning_ticket);
            utils.showElt('winning-ticket-main-page');
        }

        utils.hideElt('ticket-coupon');
        utils.setHTML('player-tickets', 'No tickets'); // Default = No tickets
        let ticket_div = document.getElementById('ticket-coupon-form');
        ['bg-success', 'bg-danger', 'bg-info'].forEach(element => {
            ticket_div.classList.remove(element);
        });
        ticket_div.classList.add(ticket_color);

        if (data.has_played) {
            if (data.has_committed.yes) {
                if (data.has_committed.has_revealed) {
                    utils.showElt('ticket-coupon');
                    utils.hideElt('ticket-coupon-single');
                    utils.showElt('ticket-coupon-multi');
                    let nb_tickets = parseInt(data.params.commitment_bonus_factor);
                    let max = parseInt(data.last_game.last_reg) + (data.ticket_index * nb_tickets);
                    let min = max - nb_tickets + 1;
                    utils.setHTML('ticket-coupon-multi-min', min);
                    utils.setHTML('ticket-coupon-multi-max', max);
                    utils.setHTML('player-tickets', `From ${min} to ${max}`);

                }
            } else {
                utils.showElt('ticket-coupon');
                utils.showElt('ticket-coupon-single');
                utils.hideElt('ticket-coupon-multi');
                utils.setHTML('ticket-coupon-single-value', data.ticket_index);
                utils.setHTML('player-tickets', `Number ${data.ticket_index}`);
            }
        }
        if (live_game) {
            utils.hideElt('back-to-live-button');
            [1, 2, 3].forEach(i => {
                utils.showElt('open-new-raffle-message-' + i);
                document.getElementById('game-summary').style.background = '#fff';
            });
        } else {
            utils.showElt('back-to-live-button');
            [1, 2, 3].forEach(i => {
                utils.hideElt('open-new-raffle-message-' + i);
                document.getElementById('game-summary').style.background = 'linear-gradient(#ECECEC, #FCFCFC)';
            });
        }
    },


    closePopUp: function() {
        utils.hideElt('popup-extra-info');
    },

    showPopUp: function(popup) {
        utils.showElt('popup-extra-info');
        ['raffle-parameters-popup', 'game-details-popup', 'history-popup'].forEach(e => {
            utils.hideElt(e);
        });
        utils.showElt(popup);
    }

};