// TODO: think long and hard about the type of numbers we want. I
// would be in favor of keeping numbers as strings as much as possible
// to avoid overflow, and having safe functions for arithmetics.

function unit_decode(u) {
    return null
}

function unit_encode(u) {
    return null
}

function string_encode(s) {
    return s
}

function string_decode(s) {
    return s
}

function bool_encode(b) {
    return b;
}

function bool_decode(b) {
    return b;
}

function bool_example() {
    var res = chooseFrom([true, false]);
    return res;
}

function int_encode(i) {
    //console.log('Attempting to encode int: ' + i)
    var num = i;
    return { "int": i.toString() }
}

function int_decode(i) {
    return parseInt(i.int)
}

function nat_encode(i) {
    //console.log('Attempting to encode nat: ' + i)
    var num = i;
    return { "nat": i.toString() }
}

function nat_decode(i) {
    return i.nat
}

function timestamp_encode(t) {
    return { "time": t }
}

function timestamp_decode(t) {
    return t.time
}


function dun_encode(d) {
    return { "mudun": d }
}

// Warning: the convention is to represent dun amounts in mudun =
// 10^{-6} dun.
function dun_decode(d) {
    return parseInt(d.mudun);
}

function address_encode(addr) {
    return { "addr": addr }
}

function address_decode(addr) {
    return addr.addr
}

function lovetup1_encode(enc) {
    return enc
}

function lovetup1_decode(dec) {
    return dec
}

function lovetup2_decode(dec1, dec2) {
    return function(obj) {
        return [dec1(obj["tuple"][0]), dec2(obj["tuple"][1])]
    }
}

function lovetup2_encode(enc1, enc2) {
    return function(obj) {
        if (obj.constructor === Array && obj.length === 2) {
            return { "tuple": [enc1(obj[0]), enc2(obj[1])] }
        } else {
            let err = 'The argument here ' + JSON.stringify(obj) + ' should be a pair constructed as [p1,p2]';
            console.log(err);
            throw (err);
        }
    }
}

function lovetuple_decode(decoders) {
    if (decoders.length === 1) {
        return decoders[0];
    } else {
        return function(obj) {
            let cpt = 0;
            return obj["tuple"].map(e => {
                let dec = decoders[cpt];
                cpt++;
                return (dec(e));
            });
        }
    }
}

function lovetuple_encode(encoders) {
    if (encoders.length === 1) {
        return (encoders[0]);
    } else {
        return function(obj) {
            if (obj.constructor === Array) {
                let cpt = 0;
                let arr = obj.map(e => {
                    let enc = encoders[cpt];
                    cpt++;
                    return (enc(e));
                });
                return { "tuple": arr }
            } else {
                let err = 'The argument here ' + JSON.stringify(obj) + ' should be a tuple constructed as [p1, ..., pn]';
                console.log(err);
                throw (err);
            }
        }
    }
}

function loveoption_decode(dec) {
    return function(obj) {
        switch (obj.constr[0]) {
            case 'None':
                return null; //
            case 'Some':
                // bad return { "some": dec(obj.constr[1][0]) };
                return dec(obj.constr[1][0]);
            default:
                console.log('DecodingError: Failing when decoding option ' + JSON.stringify(obj));
        }
    }
}

function loveoption_encode(enc) {
    return function(obj) {
        if (obj === null) {
            return { constr: ["None", null] }
        } else {
            return { constr: ["Some", [enc(obj)]] }
        }
    }
}

function lovelist_decode(dec) {
    return function(obj) {
        console.log('decoding list: ' + JSON.stringify(obj));
        var mylist = [];
        let l = obj.list;
        for (var i = 0; i < l.length; i++) {
            mylist = mylist.concat(dec(l[i]));
        }
        return mylist;
    }
}

function lovelist_encode(enc) {
    return function(l) {
        console.log('encoding list' + JSON.stringify(l));
        var reslist = [];
        for (var i = 0; i < l.length; i++) {
            reslist = reslist.concat(enc(l[i]));
        }
        var res = { "list": reslist };
        return res;
    }
}


function lovemap_decode(dec1, dec2) {
    return function(obj) {
        //console.log('decoding map: '+ JSON.stringify(obj));
        var mymap = new Object();
        let map = obj.map;
        if (map === null) return mymap;
        for (var i = 0; i < map.length; i++) {
            console.log('mymap[' + i + '] = ' + map[i]);
            key = dec1(map[i][0]);
            mymap[key] = dec2(map[i][1]);
        }
        return mymap;
    }
}

function loveset_decode(dec) {
    return function(obj) {
        console.log('decoding set: ' + JSON.stringify(obj));
        var myset = new Set();
        let set = obj.set;
        if (set === null) return myset;
        for (var i = 0; i < set.length; i++) {
            console.log('myset[' + i + '] = ' + set[i]);
            myset.add(dec(set[i]));
        }
        return myset;
    }
}

function lovemap_encode(enc1, enc2) {
    return function(obj) {
        console.log('encoding map: ' + JSON.stringify(obj));
        var resmap = [];
        for (var key in obj) {
            resmap.push([(enc1(key)), enc2(obj[key])]);
        };
        var res = { "map": resmap };
        console.log('returning encoded map: ' + JSON.stringify(res));
        return res;
    }
}

function loveset_encode(enc) {
    return function(set) {
        console.log('encoding set' + JSON.stringify(set));
        var resset = [];
        for (let item of set) { resset.push(enc(item)) }
        var res = { "set": resset };
        console.log('returning encoded set: ' + JSON.stringify(res));
        return res;
    }
}

function lovebigmap_encode(enc1, enc2) {
    return function(bm) {
        var res = {};
        res.bigmap = { "some": bm.bigmap };
        res.key_type = "unused field here";
        res.val_type = "unused field here";
        res.diff = null;
        return res;
    }
}

function lovebigmap_decode(dec1, dec2) {
    return function(bm) {
        if (bm.bigmap.some === undefined) {
            return {
                "bigmap": null,
                "getter": async function(info, key_enc, key) {
                    return null;
                }
            };
        } else {
            let id = parseInt(bm.bigmap.some);
            let bigmap_get = async function(info, key_enc, key) {
                let block = (info.block === undefined) ? 'head' : info.block;
                let res =
                    await info.mk_request(
                        info.url,
                        "/chains/main/blocks/" + block + "/context/big_maps/" + id,
                        "application/json",
                        dune_expr_encode(key_enc)(key)
                    );
                if (res === null) { // case where key not found in bigmap
                    return null;
                } else {
                    return (dune_expr_decode(dec2)(res))
                }
            };
            return {
                "bigmap": id,
                "getter": bigmap_get
            };
        }
    }
}

function bytes_encode(bytes) {
    return { "bytes": bytes }
}

function bytes_decode(bytes) {
    return bytes.bytes
}

function dune_expr_decode(dec) {
    return function(obj) {
        return dec(obj.dune_expr);
    }
}

function dune_expr_encode(enc) {
    return function(obj) {
        return { "dune_expr": enc(obj) };
    }
}

function exec_parameter_encode(enc) {
    return function(data, gas) {
        var res = {};
        res.expr = dune_expr_encode(enc)(data);
        res.gas = gas.toString();
        return res;
    }
}

async function get_storage(info) {
    let block = (info.block === undefined) ? 'head' : info.block;
    let res =
        await info.mk_request(
            info.url,
            "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 + "/storage",
            "application/json",
            null
        );
    if (res === null) {
        return null;
    } else {
        return (dune_expr_decode(storage_decode)(res));
    }
}

async function get_balance(info) {
    let block = (info.block === undefined) ? 'head' : info.block;
    let res =
        await info.mk_request(
            info.url,
            "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 + "/balance",
            "application/json",
            null
        );
    if (res === null) {
        return null;
    } else {
        return (dune_expr_decode(int_decode)(res));
    }
}


/* Examples */

function unit_example() { return null }

function string_example() { return "Dune Network" }

function random(mn, mx) {
    let r = Math.random() * (mx - mn) + mn;
    let res = (Math.floor(r));
    return res
}

function chooseFrom(l) {
    var list = l;
    var n = list.length;
    var index = random(0, n);
    var chosenFun = list[index];
    return chosenFun;
}

function int_example() {
    return random(0, 10);
}

function int64_example() {
    return random(0, 10);
}

function dun_example() {
    return int64_example();
}

function nat_example() {
    return int64_example();
}

function loveexample2(example1, example2) {
    let res1 = example1();
    let res2 = example2();
    let res = ([res1, res2]);
    return function() {
        return res;
    }
}

function loveexample3(example1, example2, example3) {
    let res1 = example1();
    let res2 = example2();
    let res3 = example3();
    let res = ([res1, res2, res3]);
    return function() {
        return res;
    }
}

function loveexample4(example1, example2, example3, example4) {
    let res1 = example1();
    let res2 = example2();
    let res3 = example3();
    let res4 = example4();
    let res = ([res1, res2, res3, res4]);
    return function() {
        return res;
    }
}

function make_list(f, k) {
    var res = new Array(k);
    for (var i = 0; i < res.length; i++) {
        res[i] = f();
    }
    return function() {
        return res;
    }
}

function make_dict(key_example, value_example) {
    var res = new Object();
    let k = random(0, 10);
    for (var i = 0; i < k; i++) {
        res[key_example()] = value_example();
    }
    return function() {
        return res;
    }

}

// TODO debug: always the same values in the map
function lovemap_example(f, g) {
    var res =
        make_dict(f, g)();
    return function() {
        return res;
    }
}

function lovebigmap_example(f, g) {
    return function() {
        return { "bigmap": int_example() };
    }
}

function loveset_example(f) {
    var res =
        new Set(make_list(
            f,
            int_example())());
    return function() {
        return res;
    }
}

function lovelist_example(f) {
    var res =
        make_list(
            f,
            int_example())();
    return function() {
        return res;
    }
}

// TODO: check how bytes are expected to be represented
function bytes_example() {
    return '0xff';
}

function timestamp_example() {
    return '2015-12-01T10:01:00+01:00';
}


function address_example() { return "dn1dYgL65EUkPYCsqqrPCAMXHdvniERPw7CA" }

function loveoption_example(f) {
    var optres = { "some": f() };
    return function() {
        return chooseFrom([function() { return null }, function() { return optres }])();
    }
}





// Type declaration
/*type index = nat*/
function index_decode(obj) { return nat_decode(obj) }

function index_encode(obj) {
    //console.log('Attempting to encode index: ' + JSON.stringify(obj));
    return nat_encode(obj)
}

function index_example() { return nat_example() }

// Type declaration
/*type secret = nat*/
function secret_decode(obj) { return nat_decode(obj) }

function secret_encode(obj) {
    //console.log('Attempting to encode secret: ' + JSON.stringify(obj));
    return nat_encode(obj)
}

function secret_example() { return nat_example() }

// Type declaration
/*type nonce = nat*/
function nonce_decode(obj) { return nat_decode(obj) }

function nonce_encode(obj) {
    //console.log('Attempting to encode nonce: ' + JSON.stringify(obj));
    return nat_encode(obj)
}

function nonce_example() { return nat_example() }

// Type declaration
/*type secretandnonce = (secret * nonce)*/
function secretandnonce_decode(obj) {
    return (lovetuple_decode([secret_decode,
        nonce_decode
    ]))(obj)
}

function secretandnonce_encode(obj) {
    //console.log('Attempting to encode secretandnonce: ' + JSON.stringify(obj));
    return (lovetuple_encode([secret_encode, nonce_encode]))(obj)
}

function secretandnonce_example() { return (loveexample2(secret_example, nonce_example))() }

// Type declaration
/*type commitment = bytes*/
function commitment_decode(obj) { return bytes_decode(obj) }

function commitment_encode(obj) {
    //console.log('Attempting to encode commitment: ' + JSON.stringify(obj));
    return bytes_encode(obj)
}

function commitment_example() { return bytes_example() }

// Type declaration
/*type dgg = nat*/
function dgg_decode(obj) { return nat_decode(obj) }

function dgg_encode(obj) {
    //console.log('Attempting to encode dgg: ' + JSON.stringify(obj));
    return nat_encode(obj)
}

function dgg_example() { return nat_example() }

// Type declaration
/*type winner = address*/
function winner_decode(obj) { return address_decode(obj) }

function winner_encode(obj) {
    //console.log('Attempting to encode winner: ' + JSON.stringify(obj));
    return address_encode(obj)
}

function winner_example() { return address_example() }

// Type declaration
/*type state =
     PlayersRegistration
   | PlayersRevelation
   | FinalDraw
   | End of winner option
   | Compromised
   | Expired*/
function state_decode(obj) { //console.log('Attempting to decode state: ' + JSON.stringify(obj));
    switch (obj.constr[0]) {
        case 'PlayersRegistration':
            return { "constr": "PlayersRegistration", "args": [] }; //
        case 'PlayersRevelation':
            return { "constr": "PlayersRevelation", "args": [] }; //
        case 'FinalDraw':
            return { "constr": "FinalDraw", "args": [] }; //
        case 'End':
            var decoded_args = loveoption_decode(winner_decode)(obj.constr[1][0]);
            return { "constr": "End", "args": [decoded_args] }; //
        case 'Compromised':
            return { "constr": "Compromised", "args": [] }; //
        case 'Expired':
            return { "constr": "Expired", "args": [] };
        default:
            console.log('DecodingError: Failing when decoding state');
    }
}

function state_encode(obj) {
    //console.log('Attempting to encode state: ' + JSON.stringify(obj));
    switch (obj.constr) {
        case 'PlayersRegistration':
            return { "constr": ["PlayersRegistration", null] }; //
        case 'PlayersRevelation':
            return { "constr": ["PlayersRevelation", null] }; //
        case 'FinalDraw':
            return { "constr": ["FinalDraw", null] }; //
        case 'End':
            var encoded_args = loveoption_encode(winner_encode)(obj.args[0]);
            return { "constr": ["End", [encoded_args]] }; //
        case 'Compromised':
            return { "constr": ["Compromised", null] }; //
        case 'Expired':
            return { "constr": ["Expired", null] };
        default:
            console.log('EncodingError: Failing when encoding state');
    }
}

function state_example() {
    let i = random(0, 6);
    switch (i) {
        case 0:
            { return { "constr": "PlayersRegistration", args: [] } };
        case 1:
            { return { "constr": "PlayersRevelation", args: [] } };
        case 2:
            { return { "constr": "FinalDraw", args: [] } };
        case 3:
            { return { "constr": "End", args: [(loveoption_example(winner_example))()] } };
        case 4:
            { return { "constr": "Compromised", args: [] } };
        case 5:
            { return { "constr": "Expired", args: [] } }
    }
}


// Type declaration
/*type game_id = int*/
function game_id_decode(obj) { return int_decode(obj) }

function game_id_encode(obj) {
    //console.log('Attempting to encode game_id: ' + JSON.stringify(obj));
    return int_encode(obj)
}

function game_id_example() { return int_example() }

// Type declaration
/*type bytes_or_secret =
     Commitment of bytes
   | Revealed*/
function bytes_or_secret_decode(obj) { //console.log('Attempting to decode bytes_or_secret: ' + JSON.stringify(obj));
    switch (obj.constr[0]) {
        case 'Commitment':
            var decoded_args = bytes_decode(obj.constr[1][0]);
            return { "constr": "Commitment", "args": [decoded_args] }; //
        case 'Revealed':
            return { "constr": "Revealed", "args": [] };
        default:
            console.log('DecodingError: Failing when decoding bytes_or_secret');
    }
}

function bytes_or_secret_encode(obj) {
    //console.log('Attempting to encode bytes_or_secret: ' + JSON.stringify(obj));
    switch (obj.constr) {
        case 'Commitment':
            var encoded_args = bytes_encode(obj.args[0]);
            return { "constr": ["Commitment", [encoded_args]] }; //
        case 'Revealed':
            return { "constr": ["Revealed", null] };
        default:
            console.log('EncodingError: Failing when encoding bytes_or_secret');
    }
}

function bytes_or_secret_example() {
    let i = random(0, 2);
    switch (i) {
        case 0:
            { return { "constr": "Commitment", args: [bytes_example()] } };
        case 1:
            { return { "constr": "Revealed", args: [] } }
    }
}


// Type declaration
/*game  : {
    id : game_id,
    opened_date : timestamp,
    organizer : address,
    organizer_hash : commitment,
    regular_players : (index, address) bigmap,
    commitment_players : (address, bytes_or_secret) bigmap,
    revealed_commitment_players : (index, address) bigmap,
    organizer_secret : secret option,
    current_xor : secret option,
    winning_ticket : index option,
    game_start_level : nat,
    last_reg : index,
    last_rev_comm : index,
    number_committed : nat,
    last_known_state : state,
    remaining_beneficiaries : nat,
    compensation : dun,
    has_withdrawn : (address, bool) bigmap,
    deposits : dun
  }*/
function game_decode(obj) {
    //console.log('Attempting to decode game: ' + JSON.stringify(obj));
    obj = obj.record;
    //console.log('after projection:' + JSON.stringify(obj));
    return { "id": game_id_decode(obj.id), "opened_date": timestamp_decode(obj.opened_date), "organizer": address_decode(obj.organizer), "organizer_hash": commitment_decode(obj.organizer_hash), "regular_players": lovebigmap_decode(index_decode, address_decode)(obj.regular_players), "commitment_players": lovebigmap_decode(address_decode, bytes_or_secret_decode)(obj.commitment_players), "revealed_commitment_players": lovebigmap_decode(index_decode, address_decode)(obj.revealed_commitment_players), "organizer_secret": loveoption_decode(secret_decode)(obj.organizer_secret), "current_xor": loveoption_decode(secret_decode)(obj.current_xor), "winning_ticket": loveoption_decode(index_decode)(obj.winning_ticket), "game_start_level": nat_decode(obj.game_start_level), "last_reg": index_decode(obj.last_reg), "last_rev_comm": index_decode(obj.last_rev_comm), "number_committed": nat_decode(obj.number_committed), "last_known_state": state_decode(obj.last_known_state), "remaining_beneficiaries": nat_decode(obj.remaining_beneficiaries), "compensation": dun_decode(obj.compensation), "has_withdrawn": lovebigmap_decode(address_decode, bool_decode)(obj.has_withdrawn), "deposits": dun_decode(obj.deposits) }
}

function game_encode(obj) {
    //console.log('Attempting to encode game: ' + JSON.stringify(obj));
    return {
        "record": {
            "id": game_id_encode(obj.id),
            "opened_date": timestamp_encode(obj.opened_date),
            "organizer": address_encode(obj.organizer),
            "organizer_hash": commitment_encode(obj.organizer_hash),
            "regular_players": lovebigmap_encode(index_encode,
                address_encode)(obj.regular_players),
            "commitment_players": lovebigmap_encode(address_encode,
                bytes_or_secret_encode)(obj.commitment_players),
            "revealed_commitment_players": lovebigmap_encode(index_encode,
                address_encode)(obj.revealed_commitment_players),
            "organizer_secret": loveoption_encode(secret_encode)(obj.organizer_secret),
            "current_xor": loveoption_encode(secret_encode)(obj.current_xor),
            "winning_ticket": loveoption_encode(index_encode)(obj.winning_ticket),
            "game_start_level": nat_encode(obj.game_start_level),
            "last_reg": index_encode(obj.last_reg),
            "last_rev_comm": index_encode(obj.last_rev_comm),
            "number_committed": nat_encode(obj.number_committed),
            "last_known_state": state_encode(obj.last_known_state),
            "remaining_beneficiaries": nat_encode(obj.remaining_beneficiaries),
            "compensation": dun_encode(obj.compensation),
            "has_withdrawn": lovebigmap_encode(address_encode,
                bool_encode)(obj.has_withdrawn),
            "deposits": dun_encode(obj.deposits)
        }
    }
}

function game_example() {
    return {
        "id": (game_id_example()),
        "opened_date": (timestamp_example()),
        "organizer": (address_example()),
        "organizer_hash": (commitment_example()),
        "regular_players": ((lovebigmap_example((index_example), (address_example)))()),
        "commitment_players": ((lovebigmap_example((address_example), (bytes_or_secret_example)))()),
        "revealed_commitment_players": ((lovebigmap_example((index_example), (address_example)))()),
        "organizer_secret": ((loveoption_example(secret_example))()),
        "current_xor": ((loveoption_example(secret_example))()),
        "winning_ticket": ((loveoption_example(index_example))()),
        "game_start_level": (nat_example()),
        "last_reg": (index_example()),
        "last_rev_comm": (index_example()),
        "number_committed": (nat_example()),
        "last_known_state": (state_example()),
        "remaining_beneficiaries": (nat_example()),
        "compensation": (dun_example()),
        "has_withdrawn": ((lovebigmap_example((address_example), (bool_example)))()),
        "deposits": (dun_example())
    }
}

// Type declaration
/*player_info  : {
    last : game_id,
    index : index option,
    total : nat,
    won : nat
  }*/
function player_info_decode(obj) {
    //console.log('Attempting to decode player_info: ' + JSON.stringify(obj));
    obj = obj.record;
    //console.log('after projection:' + JSON.stringify(obj));
    return { "last": game_id_decode(obj.last), "index": loveoption_decode(index_decode)(obj.index), "total": nat_decode(obj.total), "won": nat_decode(obj.won) }
}

function player_info_encode(obj) {
    //console.log('Attempting to encode player_info: ' + JSON.stringify(obj));
    return { "record": { "last": game_id_encode(obj.last), "index": loveoption_encode(index_encode)(obj.index), "total": nat_encode(obj.total), "won": nat_encode(obj.won) } }
}

function player_info_example() {
    return {
        "last": (game_id_example()),
        "index": ((loveoption_example(index_example))()),
        "total": (nat_example()),
        "won": (nat_example())
    }
}

// Type declaration
/*type players = (address, player_info) bigmap*/
function players_decode(obj) { return lovebigmap_decode(address_decode, player_info_decode)(obj) }

function players_encode(obj) {
    //console.log('Attempting to encode players: ' + JSON.stringify(obj));
    return lovebigmap_encode(address_encode,
        player_info_encode)(obj)
}

function players_example() { return (lovebigmap_example((address_example), (player_info_example)))() }

// Type declaration
/*params  : {
    tokens_addr : address,
    owner : address,
    player_security_deposit : dun,
    organizer_security_deposit : dun,
    organizer_reveal_delay : nat,
    commitment_bonus_factor : nat,
    registration_time : nat,
    revelation_time : nat,
    winner_reward : dgg,
    organizer_reward : dgg,
    bytes_size : nat
  }*/
function params_decode(obj) {
    //console.log('Attempting to decode params: ' + JSON.stringify(obj));
    obj = obj.record;
    //console.log('after projection:' + JSON.stringify(obj));
    return { "tokens_addr": address_decode(obj.tokens_addr), "owner": address_decode(obj.owner), "player_security_deposit": dun_decode(obj.player_security_deposit), "organizer_security_deposit": dun_decode(obj.organizer_security_deposit), "organizer_reveal_delay": nat_decode(obj.organizer_reveal_delay), "commitment_bonus_factor": nat_decode(obj.commitment_bonus_factor), "registration_time": nat_decode(obj.registration_time), "revelation_time": nat_decode(obj.revelation_time), "winner_reward": dgg_decode(obj.winner_reward), "organizer_reward": dgg_decode(obj.organizer_reward), "bytes_size": nat_decode(obj.bytes_size) }
}

function params_encode(obj) {
    //console.log('Attempting to encode params: ' + JSON.stringify(obj));
    return { "record": { "tokens_addr": address_encode(obj.tokens_addr), "owner": address_encode(obj.owner), "player_security_deposit": dun_encode(obj.player_security_deposit), "organizer_security_deposit": dun_encode(obj.organizer_security_deposit), "organizer_reveal_delay": nat_encode(obj.organizer_reveal_delay), "commitment_bonus_factor": nat_encode(obj.commitment_bonus_factor), "registration_time": nat_encode(obj.registration_time), "revelation_time": nat_encode(obj.revelation_time), "winner_reward": dgg_encode(obj.winner_reward), "organizer_reward": dgg_encode(obj.organizer_reward), "bytes_size": nat_encode(obj.bytes_size) } }
}

function params_example() {
    return {
        "tokens_addr": (address_example()),
        "owner": (address_example()),
        "player_security_deposit": (dun_example()),
        "organizer_security_deposit": (dun_example()),
        "organizer_reveal_delay": (nat_example()),
        "commitment_bonus_factor": (nat_example()),
        "registration_time": (nat_example()),
        "revelation_time": (nat_example()),
        "winner_reward": (dgg_example()),
        "organizer_reward": (dgg_example()),
        "bytes_size": (nat_example())
    }
}

// Type declaration
/*storage  : {
    game : game,
    players : players,
    last_raffle : game_id,
    params : params
  }
//let has_storage = true //TODO: add get_storage to code generation
*/
function storage_decode(obj) {
    //console.log('Attempting to decode storage: ' + JSON.stringify(obj));
    obj = obj.record;
    //console.log('after projection:' + JSON.stringify(obj));
    return { "game": game_decode(obj.game), "players": players_decode(obj.players), "last_raffle": game_id_decode(obj.last_raffle), "params": params_decode(obj.params) }
}

function storage_encode(obj) {
    //console.log('Attempting to encode storage: ' + JSON.stringify(obj));
    return { "record": { "game": game_encode(obj.game), "players": players_encode(obj.players), "last_raffle": game_id_encode(obj.last_raffle), "params": params_encode(obj.params) } }
}

function storage_example() {
    return {
        "game": (game_example()),
        "players": (players_example()),
        "last_raffle": (game_id_example()),
        "params": (params_example())
    }
}

// Type declaration
/*reveal_data  : {
    nb_regular : nat,
    nb_committed : nat,
    nb_revealed_commits : nat,
    inserted_on : timestamp,
    state : state
  }*/
function reveal_data_decode(obj) {
    //console.log('Attempting to decode reveal_data: ' + JSON.stringify(obj));
    obj = obj.record;
    //console.log('after projection:' + JSON.stringify(obj));
    return { "nb_regular": nat_decode(obj.nb_regular), "nb_committed": nat_decode(obj.nb_committed), "nb_revealed_commits": nat_decode(obj.nb_revealed_commits), "inserted_on": timestamp_decode(obj.inserted_on), "state": state_decode(obj.state) }
}

function reveal_data_encode(obj) {
    //console.log('Attempting to encode reveal_data: ' + JSON.stringify(obj));
    return { "record": { "nb_regular": nat_encode(obj.nb_regular), "nb_committed": nat_encode(obj.nb_committed), "nb_revealed_commits": nat_encode(obj.nb_revealed_commits), "inserted_on": timestamp_encode(obj.inserted_on), "state": state_encode(obj.state) } }
}

function reveal_data_example() {
    return {
        "nb_regular": (nat_example()),
        "nb_committed": (nat_example()),
        "nb_revealed_commits": (nat_example()),
        "inserted_on": (timestamp_example()),
        "state": (state_example())
    }
}



/*val%init __init_storage : params -> storage*/













check_not_already_played_view = async function(info, game_id_0 /*game_id*/ , address_1 /*address*/ ) {
    let block = (info.block === undefined) ? 'head' : info.block;
    var q = await info.mk_request(
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/check_not_already_played",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([game_id_encode, address_encode])))(game_id_0, address_1, 100000)));
    var res = dune_expr_decode(unit_decode)(q);
    return res
}



compute_state_view = async function(info, unit_0 /*unit*/ ) {
    let block = (info.block === undefined) ? 'head' : info.block;
    var q = await info.mk_request(
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/compute_state",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([unit_encode])))(unit_0, 100000)));
    var res = dune_expr_decode(state_decode)(q);
    return res
}





available_balance_view = async function(info, unit_0 /*unit*/ ) {
    let block = (info.block === undefined) ? 'head' : info.block;
    var q = await info.mk_request(
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/available_balance",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([unit_encode])))(unit_0, 100000)));
    var res = dune_expr_decode(dun_decode)(q);
    return res
}

function play_entry_args_decode(args) {
    return (dune_expr_decode((lovetuple_decode([game_id_decode,
        loveoption_decode(bytes_decode)
    ])))(args))
}
async function play_entry(info, game_id_0 /*: game_id*/ , option_1 /*: bytes option*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint play");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'play',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([game_id_encode, loveoption_encode(bytes_encode)]))))
                            (([game_id_0, option_1]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in play' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = play_entry(kt1,game_id_example (),(loveoption_example(bytes_example)) ());




compute_hash_view = async function(info, secretandnonce_0 /*secretandnonce*/ ) {
    let block = (info.block === undefined) ? 'head' : info.block;
    var q = await info.mk_request(
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/compute_hash",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([secretandnonce_encode])))(secretandnonce_0, 100000)));
    var res = dune_expr_decode(bytes_decode)(q);
    return res
}





function player_reveal_entry_args_decode(args) {
    return (dune_expr_decode((lovetuple_decode([game_id_decode,
        secretandnonce_decode
    ])))(args))
}
async function player_reveal_entry(info, game_id_0 /*: game_id*/ , secretandnonce_1 /*: secretandnonce*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint player_reveal");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'player_reveal',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([game_id_encode, secretandnonce_encode]))))
                            (([game_id_0, secretandnonce_1]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in player_reveal' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = player_reveal_entry(kt1,game_id_example (),secretandnonce_example ());


can_denounce_organizer_view = async function(info, unit_0 /*unit*/ ) {
    let block = (info.block === undefined) ? 'head' : info.block;
    var q = await info.mk_request(
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/can_denounce_organizer",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([unit_encode])))(unit_0, 100000)));
    var res = dune_expr_decode(bool_decode)(q);
    return res
}


function denounce_organizer_entry_args_decode(args) {
    return (dune_expr_decode((lovetuple_decode([game_id_decode,
        reveal_data_decode
    ])))(args))
}
async function denounce_organizer_entry(info, game_id_0 /*: game_id*/ , reveal_data_1 /*: reveal_data*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint denounce_organizer");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'denounce_organizer',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([game_id_encode, reveal_data_encode]))))
                            (([game_id_0, reveal_data_1]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in denounce_organizer' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = denounce_organizer_entry(kt1,game_id_example (),reveal_data_example ());




function reimburse_players_entry_args_decode(args) {
    return (dune_expr_decode((lovetuple_decode([game_id_decode,
        loveset_decode(address_decode)
    ])))(args))
}
async function reimburse_players_entry(info, game_id_0 /*: game_id*/ , set_1 /*: address set*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint reimburse_players");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'reimburse_players',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([game_id_encode, loveset_encode(address_encode)]))))
                            (([game_id_0, set_1]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in reimburse_players' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = reimburse_players_entry(kt1,game_id_example (),(loveset_example(address_example)) ());





compute_winner_view = async function(info, x_0 /*(secret * game_id)*/ ) {
    let block = (info.block === undefined) ? 'head' : info.block;
    var q = await info.mk_request(
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/compute_winner",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([(lovetuple_encode([secret_encode, game_id_encode]))])))(x_0, 100000)));
    var res = dune_expr_decode((lovetuple_decode([loveoption_decode(address_decode),
        secret_decode,
        loveoption_decode(index_decode)
    ])))(q);
    return res
}

next_available_game_id_view = async function(info, unit_0 /*unit*/ ) {
    let block = (info.block === undefined) ? 'head' : info.block;
    var q = await info.mk_request(
        info.url,
        "/chains/main/blocks/" + block + "/context/contracts/" + info.kt1 +
        "/exec_fun/next_available_game_id",
        "application/json",
        (exec_parameter_encode((lovetuple_encode([unit_encode])))(unit_0, 100000)));
    var res = dune_expr_decode(game_id_decode)(q);
    return res
}

function organizer_insert_new_raffle_entry_args_decode(args) {
    return (dune_expr_decode((lovetuple_decode([game_id_decode,
        commitment_decode
    ])))(args))
}
async function organizer_insert_new_raffle_entry(info, game_id_0 /*: game_id*/ , commitment_1 /*: commitment*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint organizer_insert_new_raffle");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'organizer_insert_new_raffle',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([game_id_encode, commitment_encode]))))
                            (([game_id_0, commitment_1]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in organizer_insert_new_raffle' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = organizer_insert_new_raffle_entry(kt1,game_id_example (),commitment_example ());


function organizer_reveal_entry_args_decode(args) {
    return (dune_expr_decode((lovetuple_decode([game_id_decode,
        secretandnonce_decode,
        reveal_data_decode
    ])))(args))
}
async function organizer_reveal_entry(info, game_id_0 /*: game_id*/ , secretandnonce_1 /*: secretandnonce*/ , reveal_data_2 /*: reveal_data*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint organizer_reveal");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'organizer_reveal',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([game_id_encode, secretandnonce_encode, reveal_data_encode]))))
                            (([game_id_0, secretandnonce_1, reveal_data_2]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in organizer_reveal' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = organizer_reveal_entry(kt1,game_id_example (),secretandnonce_example (),reveal_data_example ());


function owner_withdraw_leftover_entry_args_decode(args) { return (dune_expr_decode((lovetuple_decode([dun_decode])))(args)) }
async function owner_withdraw_leftover_entry(info, dun_0 /*: dun*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint owner_withdraw_leftover");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'owner_withdraw_leftover',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([dun_encode]))))
                            (([dun_0]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in owner_withdraw_leftover' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = owner_withdraw_leftover_entry(kt1,dun_example ());


function deposit_entry_args_decode(args) { return (dune_expr_decode((lovetuple_decode([unit_decode])))(args)) }
async function deposit_entry(info, unit_0 /*: unit*/ ,
    tr_amount = '0') {
    console.log("Calling entrypoint deposit");
    if (typeof metal === 'undefined' || metal === undefined) {
        reject('Metal Browser Extension not installed or not detected!');
    } else {
        let ok = false;
        metal.isEnabled(function(res) {
            if (!res) {
                reject('It seems Metal Browser Extension is not configured! Please, create or import a wallet.');
            } else {
                metal.getAccount(function(r) {
                    console.log('account: ' + r);
                    let op = {
                        dst: info.kt1,
                        amount: tr_amount,
                        entrypoint: 'deposit',
                        parameter: JSON.stringify(
                            (dune_expr_encode((lovetuple_encode([unit_encode]))))
                            (([unit_0]))),
                        cb: function(res) {
                            if (res.ok) {
                                console.log('res in deposit' + res);
                            }
                            return (res);
                        }
                    };
                    console.log('op' + JSON.stringify(op));
                    metal.send(op);
                })
            }
        })
    }
}
//res = deposit_entry(kt1,unit_example ());




/*
module.exports = {

    deposit_entry_args_decode,
    deposit_entry,
    owner_withdraw_leftover_entry_args_decode,
    owner_withdraw_leftover_entry,
    organizer_reveal_entry_args_decode,
    organizer_reveal_entry,
    organizer_insert_new_raffle_entry_args_decode,
    organizer_insert_new_raffle_entry,
    next_available_game_id_view,
    compute_winner_view,
    reimburse_players_entry_args_decode,
    reimburse_players_entry,
    denounce_organizer_entry_args_decode,
    denounce_organizer_entry,
    can_denounce_organizer_view,
    player_reveal_entry_args_decode,
    player_reveal_entry,
    compute_hash_view,
    play_entry_args_decode,
    play_entry,
    available_balance_view,
    compute_state_view,
    check_not_already_played_view,
    reveal_data_example,
    reveal_data_decode,
    reveal_data_encode,
    storage_example,
    storage_decode,
    storage_encode,
    params_example,
    params_decode,
    params_encode,
    players_example,
    players_decode,
    players_encode,
    player_info_example,
    player_info_decode,
    player_info_encode,
    game_example,
    game_decode,
    game_encode,
    bytes_or_secret_example,
    bytes_or_secret_decode,
    bytes_or_secret_encode,
    game_id_example,
    game_id_decode,
    game_id_encode,
    state_example,
    state_decode,
    state_encode,
    winner_example,
    winner_decode,
    winner_encode,
    dgg_example,
    dgg_decode,
    dgg_encode,
    commitment_example,
    commitment_decode,
    commitment_encode,
    secretandnonce_example,
    secretandnonce_decode,
    secretandnonce_encode,
    nonce_example,
    nonce_decode,
    nonce_encode,
    secret_example,
    secret_decode,
    secret_encode,
    index_example,
    index_decode,
    index_encode,
    get_storage,
    get_balance

};*/
