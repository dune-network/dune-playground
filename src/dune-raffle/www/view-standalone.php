<link rel="stylesheet" href="../../assets/css/main.css">
<link rel="stylesheet" href="../../dapps/dapps.css">

<?php
require_once 'view.html';
?>

<style>

#game-initial-phase,
#game-reveal-phase,
#game-finalize-phase,
#game-ended-phase,
#game-compromised-phase,
#game-compromised-phase,
#game-unexpected-phase {
    display: block !important;
    border: 5px solid #a66;
    padding: 10px; 
    margin: 30px;
    background-color: #eef;
}
#game-initial-phase-playing,
#game-initial-phase-secret-explanation,
#game-initial-phase-submitting,
#game-initial-phase-waiting-1,
#game-initial-phase-waiting-2,
#game-initial-phase-waiting-3,

#game-reveal-phase-revealing,
#game-reveal-phase-submitting,
#game-reveal-phase-waiting-1,
#game-reveal-phase-waiting-2,
#game-reveal-phase-waiting-3,

#game-ended-phase-with-winner,
#game-ended-phase-no-winner,

#game-compromised-phase-no-compensation,
#game-compromised-phase-to-compensate,
#game-compromised-phase-to-compensate-not-played,
#game-compromised-phase-compensating-1,
#game-compromised-phase-compensating-2,
#game-compromised-phase-compensated,
#game-compromised-phase-early,
#game-compromised-phase-late,

#game-finalize-phase-to-denounce,
#game-finalize-phase-denouncing-1

{
    display: block !important;
    border: 5px solid #aaa;
    padding: 10px; 
    margin: 30px;
    background-color: #fff;

}


</style>