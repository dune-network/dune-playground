require('log-timestamp');
const axios = require('axios').default;
const Pool = require('pg').Pool;
const configLib = require('config-lib');
const utils = require('utils-lib');
const raffle = require('spice-raffle-lib');

random_commit = async function(kt1_info) {
    let nonce = utils.randInt(Number.MAX_SAFE_INTEGER);
    let secret = utils.randInt(Number.MAX_SAFE_INTEGER);
    let secretandnonce_0 = [secret, nonce];
    let bytes = await raffle.compute_hash_view(kt1_info, secretandnonce_0);
    let elt = {
        nonce: nonce,
        secret: secret,
        commit: bytes
    };
    return (elt);
};


inject_commit = async function(config, info, next_game_id, com, organizer_security_deposit) {
    let comm = '0x' + com.commit;
    let op_hash = await utils.inject_transaction(config, {
        src: info.manager,
        dest: info.contract_addr,
        amount: organizer_security_deposit,
        arg: `'#love:(${next_game_id}, ${comm})'`,
        entrypoint: 'organizer_insert_new_raffle',
        burn_cap: 2,
        wait: 0
    });
    return op_hash;
}

insert_new_raffle = async function(config, pool, info, last_commit, kt1_info, organizer_security_deposit) {
    let curr_level = await utils.current_level(config);
    let res = await utils.pg_query_promise(pool, `SELECT COALESCE(MAX(id_),-1) from ${info.table}`);
    let last_commit_from_db = parseInt(res.rows[0].coalesce);
    // We know that all commits with id_ <= last_commit are included in the chain
    await utils.pg_query_promise(pool,
        `UPDATE ${info.table} set included_ = TRUE WHERE included_ is NULL and id_ <= ${last_commit}`);
    // Select commits that have not been included yet
    var not_included =
        await utils.pg_query_promise(pool,
            `SELECT id_, commit_, secret_, nonce_, inserted_level_, op_hash_ FROM ${info.table} WHERE included_ is NULL ORDER BY id_ ASC;`);
    if (last_commit_from_db <= last_commit) { // <= instead of === because there may be other organizers
        if (not_included.rows.length > 0) {
            console.log('Invariant broken: All inserted commits are supposed to be included');
            process.exit(0);
        }
        // generate fresh commit
        let com = await random_commit(kt1_info);
        console.log('New commit to insert: ' + JSON.stringify(com));
        let next_commit = last_commit + 1;
        // TODO:insert into DB first
        let req =
            `INSERT INTO ${info.table}(id_, commit_, secret_, nonce_, inserted_level_)
                   VALUES (${next_commit}, '${com.commit}', ${com.secret}, ${com.nonce}, ${curr_level})`;
        await utils.pg_query_promise(pool, req);
        let op_hash = await inject_commit(config, info, next_commit, com, organizer_security_deposit);
        let reqUp = `UPDATE ${info.table} set op_hash_ = '${op_hash}' where id_ = ${next_commit} AND commit_ = '${com.commit}'`;
        await utils.pg_query_promise(pool, reqUp);
        return;
    } else {
        if (not_included.rows.length > 1) {
            console.log('Invariant broken: Should not have more than one inserted but not included commit');
            process.exit(0);
        }
        let com = not_included.rows[0]; // pick one commit
        let delta = config.operations_ttl - (curr_level - com.inserted_level_);
        if (delta >= 0) {
            console.log(`non included commits already exist! Operation will be outdated in ${delta} blocks`);
            return;
        } else {
            console.log(`Error: outdated operation ${com.op_hash_} never included`);
            process.exit(1);
        }
    }

}

organizer_reveal_commit = async function(config, pool, info, kt1_info, last_game, last_commit) {
    let req = `SELECT id_, secret_, nonce_, commit_ FROM ${info.table} where id_ >= ${last_commit}`;
    let res = await utils.pg_query_promise(pool, req);
    if (res.rows.length === 0) {
        console.log(`Commit deleted from DB, not saved, or inserted by another organizer. I skip (I have nothing to reveal)`);
        return;
    } else {
        if (res.rows.length !== 1) {
            console.log(`Unexpected error: should have exactly ONE commit to reveal with ID ${last_commit} but got ${res.rows.length} from DB!`);
            process.exit(1);
        }
        let e = res.rows[0];
        console.log("revealing: " + JSON.stringify(e));
        let comp_win = await raffle.compute_winner_view(kt1_info, [e.secret_, e.id_]);
        let win = comp_win[0];
        let winner = win === null ? 'None' : `Some(${win})`;
        let state = `End(${winner})`;
        let nb_regular = parseInt(last_game.last_reg);
        let nb_committed = parseInt(last_game.number_committed);
        let nb_revealed_commits = parseInt(last_game.last_rev_comm);
        let inserted_on = new Date(null);
        inserted_on.setUTCSeconds(parseInt(last_game.opened_date));
        let date = inserted_on.toISOString().split('.')[0] + "Z";
        let rev_data = `{state=${state};nb_regular=${nb_regular}p;nb_committed=${nb_committed}p;nb_revealed_commits=${nb_revealed_commits}p;inserted_on=${date}}`;
        let hash = await utils.inject_transaction(config, {
            src: info.manager,
            dest: info.contract_addr,
            amount: 0,
            arg: `'#love:(${e.id_}, (${e.secret_}p,${e.nonce_}p), ${rev_data})'`,
            //arg: `'#love:(${e.id_}, (${e.secret_}p,${e.nonce_}p), Compromised)'`, // for debug, to compromise a game
            entrypoint: 'organizer_reveal',
            burn_cap: 1,
            wait: 0
        });
        // should do something with the hash if --wait is not none !!
        console.log(`operation of revealing the latest commit injected with hash ${hash}...`);
        console.log("remove this exit once debbugged well");
        return hash
    }
}

compensate_players = async function(config, pool, info, last_commit) {
    let COMPENSATE_THRESHOLD_PER_OP = 10;
    let play_table = `${info.contract_name}__play__autocrawled`;
    let reimburse_table = `${info.contract_name}__reimburse_players__autocrawled`;
    let req = `
    SELECT ${play_table}.player_
    FROM ${play_table}
    WHERE 
        ${play_table}.game_id_ = ${last_commit} AND 
        (${play_table}.player_ NOT IN \
            (select refunded_player_ as player_ from ${reimburse_table} where game_id_ = ${last_commit}))
    LIMIT ${COMPENSATE_THRESHOLD_PER_OP};
  `;
    let res = await utils.pg_query_promise(pool, req);
    let rows = res.rows;
    let first = true;
    let set = rows.reduce((acc, e) => {
        if (first) {
            first = false;
            return e.player_
        } else {
            return (`${acc}; ${e.player_}`)
        }
    }, "");
    try {
        let hash = await utils.inject_transaction(config, {
            src: info.manager,
            dest: info.contract_addr,
            amount: 0,
            arg: `'#love:(${last_commit}, {${set}})'`,
            entrypoint: 'reimburse_players',
            burn_cap: 5,
            wait: 0
        });
        console.log(`Compensate players ${set}. Op injected with hash ${hash}`);
        return;
    } catch (error) {
        // XXX ignore error in case latest injected compensation has not yet been crawled. Should implement a better solution.
        return;
    }
}

round = async function(raffle_info, config, pool) {
    console.log('Round with raffle ' + raffle_info.name);
    console.log('Contract address: ' + raffle_info.addr);

    let kt1_info = {
        kt1: raffle_info.addr,
        url: utils.mk_request_prefix(config.node_info),
        mk_request: async function(url_prefix, req, appType, data) {
            let q = `${url_prefix}/${req}`;
            let headers = { headers: { 'Content-Type': appType } };
            let xhr_func = (data === null) ? axios.get : axios.post;
            let res = await xhr_func(q, JSON.stringify(data), headers);
            return res.data;
        }
    };
    let info = {
        manager: raffle_info.name + '_manager',
        contract_name: raffle_info.name,
        table: raffle_info.name + '_commits',
        contract_addr: raffle_info.addr
    };

    let storage = await raffle.get_storage(kt1_info);
    console.log('\n\ncontract storage is: ' + JSON.stringify(storage) + '\n');
    let last_game_id = storage.last_raffle;
    let last_game = storage.game;
    let organizer_security_deposit = parseInt(storage.params.organizer_security_deposit) / 1000000; // in DUN
    if (last_game_id < 0) {
        await insert_new_raffle(config, pool, info, last_game_id, kt1_info, organizer_security_deposit);
    } else {
        let status = await raffle.compute_state_view(kt1_info);
        //await organizer_reveal_commit(config, pool, info, kt1_info, last_game, last_game_id); // to debug, force reveal and compromise current game
        switch (status.constr) {
            case 'PlayersRegistration':
                console.log(`status = ${status.constr}: nothing to do for monitor`);
                break;

            case 'PlayersRevelation':
                console.log(`status = ${status.constr}: nothing to do for monitor`);
                break;

            case 'FinalDraw':
                console.log(`status = ${status.constr}: organizer will reveal`);
                await organizer_reveal_commit(config, pool, info, kt1_info, last_game, last_game_id)
                break;

            case 'End':
                console.log(`status = ${status.constr}: insert new game`);
                await insert_new_raffle(config, pool, info, last_game_id, kt1_info, organizer_security_deposit);
                break;

            case 'Compromised':
            case 'Expired':
                console.log(`status = ${status.constr}. Will compensate people and/or insert a new game`);
                if (parseInt(last_game.remaining_beneficiaries) === 0) {
                    await insert_new_raffle(config, pool, info, last_game_id, kt1_info, organizer_security_deposit);
                } else {
                    await compensate_players(config, pool, info, last_game_id);
                }
                break;

            default:
                console.log(`status = ${status.constr} is unknown!`);
                process.exit(1);
                break;
        }
    }
}

loop = async function(raffles, config, pool, sleep_ms) {
    //Promise.all(raffles.map(raff => { return round(raff, config, pool) }));
    await raffles.reduce(async function(accu, raff) {
        try {
            await accu;
            return round(raff, config, pool);
        } catch (error) {
            console.log('Error inside loop/reduce: ' + JSON.stringify(error));
            process.exit(1);
        }
    }, Promise.resolve());
    console.log('\n-- loop ended -----------------------------------\n\n');
    await utils.sleep(sleep_ms);
    await loop(raffles, config, pool, sleep_ms);
}

main = async function() {
    const config = configLib.parseArgs();
    const pool = new Pool(config.database);
    const sleep_ms = (config.time_between_blocks * 1000) / 2;

    let raffles = config.contracts.filter(e => { return (e.family === "raffle") });
    console.log('Found ' + raffles.length + ' contracts that belong to raffle family');
    loop(raffles, config, pool, sleep_ms).then(function() {
        console.log('Done!');
        process.exit(0);
    }, function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    }).catch(function(err) {
        console.log('\nFailure!');
        console.log(err);
        process.exit(1);
    });
};

main();