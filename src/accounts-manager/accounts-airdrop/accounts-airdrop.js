require('log-timestamp');
const axios = require('axios').default;
const configLib = require('config-lib');
const utils = require('utils-lib');

get_balance = async function(addr, balance_checker_view) {
    if (balance_checker_view === null || balance_checker_view === undefined || balance_checker_view === "") {
        let endpoint = `/chains/main/blocks/head/context/contracts/${addr}/balance`;
        let req = utils.mk_request_url(config.node_info, endpoint);
        let balance = await axios.get(req);
        return (parseInt(balance.data) / 1000000);
    } else {
        let req = utils.mk_request_url(config.node_info, `/chains/main/blocks/head/context/contracts/${addr}/exec_fun/${balance_checker_view}`);
        let data = { expr: { dune_expr: null }, gas: "800000" }
        let headers = { headers: { 'Content-Type': 'application/json' } };
        let balance = await axios.post(req, JSON.stringify(data), headers);
        return (parseInt(balance.data.dune_expr.mudun) / 1000000);
    }
};

airdrop_account_with_duns = async function(config, cli, entrypoint, account) {
    let addr = account.addr;
    let name = account.name;
    let balance_checker = account.balance_checker;
    let dun_airdrop = account.dun_airdrop;
    let failed = { 'name': name, 'failed': true };
    let succeeded = { 'name': name, 'failed': false };
    if (addr === 'unencrypted:edsk' || addr === '' || addr === 'KT1XXX' || addr === 'dn1XXX' || dun_airdrop === 0) {
        console.log(`Skip ${name}: not DUN-airdroppable!`);
        return (succeeded);
    } else {
        let bal = await get_balance(addr, balance_checker);
        if (bal > dun_airdrop / 2) {
            console.log(`Skip ${name}: sufficient balance! (${bal} DUN)`);
            return (succeeded);
        } else {
            console.log(`Will airdrop ${name}: low balance! (${bal} DUN)`);
            try {
                let op = {
                    src: 'playground_manager',
                    dest: addr,
                    amount: dun_airdrop,
                    burn_cap: 1,
                    wait: 0
                };
                if (entrypoint !== "") {
                    op.entrypoint = entrypoint;
                }
                let hash = await utils.inject_transaction(config, op);
                // should do something with the hash if --wait is not none !!
                console.log(`airdrop_account_with_duns to ${name} injected with hash ${hash}...`);
                return (succeeded);
            } catch (error) {
                console.log(`airdrop_account_with_duns to ${name}: failed...`);
                return (failed);
            }
        }
    }
};


airdrop_account_with_dgg = async function(config, cli, dgg_bigmap_id, account) {
    let addr = account.addr;
    let name = account.name;
    let dgg_airdrop = account.dgg_airdrop;
    let failed = { 'name': name, 'failed': true };
    let succeeded = { 'name': name, 'failed': false };
    if (addr === '' || addr === 'KT1XXX' || dgg_airdrop === 0) {
        console.log(`Skip ${name}: not DGG-airdroppable!`);
        return (succeeded);
    } else {
        let endpoint = `/chains/main/blocks/head/context/big_maps/${dgg_bigmap_id}`;
        let post_data = JSON.stringify({ dune_expr: { address: addr } });
        let req = utils.mk_request_url(config.node_info, endpoint);
        let res = await axios.post(req, post_data, { headers: { 'Content-Type': 'application/json' } });
        bal = (res.data === null) ? 0 : parseInt(res.data.dune_expr.nat);
        if (bal > dgg_airdrop / 2) {
            console.log(`Skip ${name}: sufficient balance! (${bal} DGG)`);
            return (succeeded);
        } else {
            try {
                let op = {
                    src: 'playground_manager',
                    dest: 'dgg_token',
                    amount: 0,
                    entrypoint: 'mint',
                    burn_cap: 1,
                    arg: `'#love:[${addr}, ${dgg_airdrop}p]'`,
                    wait: 0
                };
                let hash = await utils.inject_transaction(config, op);
                // should do something with the hash if --wait is not none !!
                console.log(`airdrop_account_with_dgg to ${name} injected with hash ${hash}...`);
                return (succeeded);
            } catch (error) {
                console.log(`airdrop_account_with_dgg to ${name}: failed...`);
                return (failed);
            }
        }
    }
};

airdrop_accounts = async function(accounts, airdrop_one_account) {
    var ok = true;
    res = await accounts.reduce(async function(prevPr, nextAccount) {
        res = await prevPr;
        console.log("");
        ok = ok && !res.failed;
        return airdrop_one_account(nextAccount);
    }, Promise.resolve({ failed: false }));
    ok = ok && !res.failed;
    if (!ok) {
        throw (new Error("At least one airdrop failed!!"));
    }
    return;
};


loop = async function(config, cli, sleep_ms, do_loop, dgg_bigmap_id) {
    console.log("+-+-+-+-+");
    console.log("Airdropping dn1 addresses ...");
    await airdrop_accounts(config.wallets, ac => airdrop_account_with_duns(config, cli, "", ac));

    console.log("+-+-+-+-+");
    console.log("Airdropping KT1 addresses ...");
    await airdrop_accounts(config.contracts, ac => airdrop_account_with_duns(config, cli, "deposit", ac));

    if (dgg_bigmap_id !== null) {
        console.log("+-+-+-+-+");
        console.log("Airdropping DGGs to KT1 addresses ...");
        await airdrop_accounts(config.contracts, ac => airdrop_account_with_dgg(config, cli, dgg_bigmap_id, ac));
    }
    console.log('-- round ended -----------------------------------\n');
    if (!do_loop) {
        return;
    }
    await utils.sleep(sleep_ms);
    await loop(config, cli, sleep_ms, do_loop, dgg_bigmap_id);
}

do_loop = process.argv.some(e => {
    return (e === 'loop')
});

const config = configLib.parseArgs();
const sleep_ms = (config.time_between_blocks * 1000) * 2;
const cli = utils.mk_client_cmd(config);

dgg_token_bigmap = async function(config) {
    let dgg_token = config.contracts.find(e => e.name === 'dgg_token');
    if (dgg_token === null || dgg_token === undefined) {
        console.log("'dgg_token' not found in config file");
        process.exit(1);
    }
    let endpoint = `/chains/main/blocks/head/context/contracts/${dgg_token.addr}/storage`;
    let req = utils.mk_request_url(config.node_info, endpoint);
    try {
        let res = await axios.get(`${req}`);
        return (parseInt(res.data.dune_expr.record.accounts.bigmap.some));
    } catch (error) {
        console.log("Not able to get 'dgg_token' contract info. I'll skip DGG airdrop!");
        return null;
    }

}

dgg_token_bigmap(config).then(function(dgg_bigmap_id) {
    loop(config, cli, sleep_ms, do_loop, dgg_bigmap_id).then(function() {
        console.log('Done!');
        process.exit(0);
    }, function(err) {
        console.log('\nSome airdrops failed!');
        console.log(err);
        process.exit(1);
    }).catch(function(err) {
        console.log('\nSome airdrops failed!');
        console.log(err);
        process.exit(1);
    });

})