#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

npm install .

node accounts-airdrop.js --config ../../../global-config.json $@
