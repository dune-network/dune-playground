require('log-timestamp');
const configLib = require('config-lib');
const utils = require('utils-lib');

importAccount = async function(name, value, kind, cmd) {
    if (value === 'unencrypted:edsk' || value === '' || value === 'KT1XXX') {
        let msg = `Will not import ${kind} ${name}. No key given`;
        console.log(msg);
        return (msg);
    } else {
        let msg = `Will attempt to import ${kind} ${name}`;
        console.log(msg);
        //console.log(`cmd is ${cmd}`);
        cmd_res = await utils.shell_cmd(cmd);
        if (cmd_res.error) {
            let res = `Importing ${kind} ${name} failed!\n`;
            console.log(res);
            console.log(JSON.stringify(cmd_res));
            throw (new Error(res));
        } else {
            let res = `Importing ${kind} ${name} succeeded!\n`;
            console.log(res);
            return res;
        }
    }
};

importAccounts = async function(arr, mk_cmd, kind) {
    await arr.reduce(async function(prevPr, nextAccount) {
        await prevPr;
        let cmd = mk_cmd(nextAccount);
        value = nextAccount.skey === undefined ? nextAccount.addr : nextAccount.skey;
        return (importAccount(nextAccount.name, value, kind, cmd));
    }, Promise.resolve());
    return
};

main = async function(config) {
    let cli = utils.mk_client_cmd(config);
    let mk_import = function(account) {
        return `${cli} import secret key ${account.name} ${account.skey} --force`;
    };
    let mk_remember = function(account) {
        return `${cli} remember contract ${account.name} ${account.addr} --force`;
    };
    console.log("Importing wallets ...");
    await importAccounts(config.wallets, mk_import, "wallet");
    console.log("Importing contracts ...");
    await importAccounts(config.contracts, mk_remember, "contracts");
    return;
};

var config = configLib.parseArgs();

main(config).then(function() {
    console.log('Done!');
    process.exit(0);
}, function(err) {
    console.log('\nSome imports failed 1!');
    console.log(err);
    process.exit(1);
}).catch(function(err) {
    console.log('\nSome imports failed 2!');
    console.log(err);
    process.exit(1);
});
