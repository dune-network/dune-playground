#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

npm install .

node import-addresses.js --config ../../../global-config.json
