#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

#######

NAME=./parse-params.sh
DEFAULT_GLOBAL_CONFIG_NAME="global-config.json"

DOC="$NAME: Parses the (needed) parameters of the given JSON file and exports them
as environment variables.

- option -p=<dir> (or --prefix-dir==<dir>) allows to set the dir from which this
script is called. Default value is ./

- option -c=<file> (or --config=<file>) allows to set a JSON config file. If the
  option is not set, this script will attempt to set it to the value associated
  to field 'config_file' read from JSON global config file
  <prefix-dir>/$DEFAULT_GLOBAL_CONFIG_NAME.
"

#######

printf "\n=+=+=+ call to $NAME +=+=++\n"

CONFIG_FILE=""
PREFIX_DIR=""

for arg in "$@"
do
    case $arg in
        -c=*|--config=*)
            CONFIG_FILE="${arg#*=}"
            shift
            ;;
        -p=*|--prefix-dir=*)
            PREFIX_DIR="${arg#*=}"
            shift
            ;;
        -h|--help)
            printf "\nUsage: $NAME -c=<json-config-file> -n=<network> -p=<prefix-dir>\n"
            printf "\n$DOC\n"
            exit 1
            ;;
        *)
            printf "$NAME: ignoring unknown argument $arg\n"
    esac
done

PREFIX_DIR=`pwd`/$PREFIX_DIR

check_field_found (){
    print_success=$4
    # 0 ==> no | 1 ==> partial | 2 ==> full
    if [ "$1" = "null" ] ; then
        printf "Not able to get value of field \"$2\" from file $3\n"
        exit 1
    else
        case $print_success in
            0)
                return
                ;;
            1)
                printf "Field \"$2\" found in $3.\n"
                return
                ;;
            *)
                printf "Field \"$2\" found in $3: Its value is \"$1\"\n"
                return
                ;;
        esac
    fi
}

printf "prefix-dir is $PREFIX_DIR\n"

if [ "$CONFIG_FILE" = "" ]; then
    GLOB=$PREFIX_DIR/$DEFAULT_GLOBAL_CONFIG_NAME
    printf "Config file not given. I'll try to read its value from default global config file $GLOB\n"
    CF=`jq .config_file $GLOB | cut -d '"' -f2`
    check_field_found "$CF" "config_file" "$GLOB" 2;
    CONFIG_FILE=$PREFIX_DIR/$CF
    printf "OK. I will use default config file: $CONFIG_FILE\n"
else
    CONFIG_FILE="$PREFIX_DIR/$CONFIG_FILE"
    printf "Provided config file: $CONFIG_FILE\n"
fi

###############################################################################

NETWORK_NAME=`jq .name $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$NETWORK_NAME" "name" "$CONFIG_FILE" 2;

BACK_DEBUG_LEVEL=`jq .back_debug_level $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$BACK_DEBUG_LEVEL" "back_debug_level" "$CONFIG_FILE" 2;

FRONT_DEBUG_LEVEL=`jq .front_debug_level $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$FRONT_DEBUG_LEVEL" "front_debug_level" "$CONFIG_FILE" 2;

DUNE_NETWORK_DIR=`jq .dune_network_directory $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$DUNE_NETWORK_DIR" "dune_network_directory" "$CONFIG_FILE" 2;

LOVE_STANDALONE_DIR=`jq .love_standalone_directory $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$LOVE_STANDALONE_DIR" "love_standalone_directory" "$CONFIG_FILE" 2;

CLIENT_BASE_DIR=`jq .client_base_dir $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$CLIENT_BASE_DIR" "client_base_dir" "$CONFIG_FILE" 2;
CLIENT_BASE_DIR=`eval "echo -e $CLIENT_BASE_DIR"`

NODE_ADDR=`jq .node_info.addr $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$NODE_ADDR" "node_info.addr" "$CONFIG_FILE" 2;

NODE_RPC_PORT=`jq .node_info.port $CONFIG_FILE`
check_field_found "$NODE_RPC_PORT" "node_info.port" "$CONFIG_FILE" 2;

NODE_HTTPS=`jq .node_info.https $CONFIG_FILE`
check_field_found "$NODE_HTTPS" "node_info.https" "$CONFIG_FILE" 2;

PUBLIC_NODE_ADDR=`jq .public_node_info.addr $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$PUBLIC_NODE_ADDR" "public_node_info.addr" "$CONFIG_FILE" 2;

PUBLIC_NODE_RPC_PORT=`jq .public_node_info.port $CONFIG_FILE`
check_field_found "$PUBLIC_NODE_RPC_PORT" "public_node_info.port" "$CONFIG_FILE" 2;

PUBLIC_NODE_HTTPS=`jq .public_node_info.https $CONFIG_FILE`
check_field_found "$PUBLIC_NODE_HTTPS" "public_node_info.https" "$CONFIG_FILE" 2;

EXPLORER_ADDR=`jq .explorer_info.addr $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$EXPLORER_ADDR" "explorer_info.addr" "$CONFIG_FILE" 2;

EXPLORER_PORT=`jq .explorer_info.port $CONFIG_FILE`
check_field_found "$EXPLORER_PORT" "explorer_info.port" "$CONFIG_FILE" 2;

EXPLORER_HTTPS=`jq .explorer_info.https $CONFIG_FILE`
check_field_found "$EXPLORER_HTTPS" "explorer_info.https" "$CONFIG_FILE" 2;


PUBLIC_API_ADDR=`jq .public_api_server.addr $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$PUBLIC_API_ADDR" "public_api_server.addr" "$CONFIG_FILE" 2;

PUBLIC_API_PORT=`jq .public_api_server.port $CONFIG_FILE`
check_field_found "$PUBLIC_API_PORT" "public_api_server.port" "$CONFIG_FILE" 2;

PUBLIC_API_HTTPS=`jq .public_api_server.https $CONFIG_FILE`
check_field_found "$PUBLIC_API_HTTPS" "public_api_server.https" "$CONFIG_FILE" 2;

TIME_BETWEEN_BLOCKS=`jq .time_between_blocks $CONFIG_FILE`
check_field_found "$TIME_BETWEEN_BLOCKS" "time_between_blocks" "$CONFIG_FILE" 2;

NETWORK_CONFIG_FLAG=`jq .dune_config $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$NETWORK_CONFIG_FLAG" "dune_config" "$CONFIG_FILE" 2;

LOCAL_NODE_ENABLED=`jq .start_local_node_info.enabled $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$LOCAL_NODE_ENABLED" "start_local_node_info.enabled" "$CONFIG_FILE" 2;

LOCAL_NODE_ADDR=`jq .start_local_node_info.addr $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$LOCAL_NODE_ADDR" "start_local_node_info.addr" "$CONFIG_FILE" 2;

LOCAL_NODE_P2P_PORT=`jq .start_local_node_info.p2p_port $CONFIG_FILE`
check_field_found "$LOCAL_NODE_P2P_PORT" "start_local_node_info.p2p_port" "$CONFIG_FILE" 2;

LOCAL_NODE_RPC_PORT=`jq .start_local_node_info.rpc_port $CONFIG_FILE`
check_field_found "$LOCAL_NODE_RPC_PORT" "start_local_node_info.rpc_port" "$CONFIG_FILE" 2;

LOCAL_NODE_BOOTSTRAP_PEERS=`jq .start_local_node_info.bootstrap_peers $CONFIG_FILE | jq .[] | cut -d'"' -f2 | tr '\n' ' ' | xargs`
check_field_found "$LOCAL_NODE_BOOTSTRAP_PEERS" "start_local_node_info.bootstrap_peers" "$CONFIG_FILE" 2;

LOCAL_NODE_DATA_DIR=`jq .start_local_node_info.data_dir $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$LOCAL_NODE_DATA_DIR" "start_local_node_info.data_dir" "$CONFIG_FILE" 2;

LOCAL_NODE_DUNE_CONTEXT_STORAGE=`jq .start_local_node_info.dune_context_storage $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$LOCAL_NODE_DUNE_CONTEXT_STORAGE" "start_local_node_info.dune_context_storage" "$CONFIG_FILE" 2;

ALERT_EMAIL_ADDRESSES=`jq .alert_email_addresses $CONFIG_FILE | jq .[] | cut -d'"' -f2 | tr '\n' ' ' | xargs | tr ' ' ', '`
check_field_found "$ALERT_EMAIL_ADDRESSES" "alert_email_addresses" "$CONFIG_FILE" 2;

contracts=`jq .contracts $CONFIG_FILE`
check_field_found "$ALERT_EMAIL_ADDRESSES" "contracts" "$CONFIG_FILE" 1;

CONTRACTS_BASE64=""
for row in $(echo "${contracts}" | jq -r '.[] | @base64'); do
    CONTRACTS_BASE64="$CONTRACTS_BASE64 $row"
done

DB_USER=`jq .database.user $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$DB_USER" "database.user" "$CONFIG_FILE" 2;

DB_NAME=`jq .database.database $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$DB_NAME" "database.database" "$CONFIG_FILE" 2;

DB_HOST=`jq .database.host $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$DB_HOST" "database.host" "$CONFIG_FILE" 2;

DB_PORT=`jq .database.port $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$DB_PORT" "database.port" "$CONFIG_FILE" 2;

UNIX_USER=`jq .unix_user $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$UNIX_USER" "unix_user" "$CONFIG_FILE" 2;

UNIX_GROUP=`jq .unix_group $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$UNIX_GROUP" "unix_group" "$CONFIG_FILE" 2;

ROOT_PATH=`jq .root_path $CONFIG_FILE | cut -d'"' -f2`
check_field_found "$ROOT_PATH" "root_path" "$CONFIG_FILE" 2;

###############################################################################

https="-S"
[ "$NODE_HTTPS" = true ] || https=""

dbg=""
[ "$BACK_DEBUG_LEVEL" -eq "0" ] || dbg="-l"

CLIENT_CMD="$DUNE_NETWORK_DIR/dune-client $dbg -d $CLIENT_BASE_DIR -A $NODE_ADDR -P $NODE_RPC_PORT $https "

printf "Client CMD is: \"$CLIENT_CMD\"\n"

###############################################################################

declare -A tab

tab[DUNE_PLAYGROUND_PARSE_PARAMS_DONE]=true
tab[DUNE_PLAYGROUND_CONFIG_FILE]="$CONFIG_FILE"

tab[DUNE_PLAYGROUND_NETWORK_NAME]="$NETWORK_NAME"
tab[DUNE_PLAYGROUND_NODE_ADDR]="$NODE_ADDR"
tab[DUNE_PLAYGROUND_NODE_RPC_PORT]="$NODE_RPC_PORT"
tab[DUNE_PLAYGROUND_NODE_HTTPS]="$NODE_HTTPS"

tab[DUNE_PLAYGROUND_PUBLIC_NODE_ADDR]="$PUBLIC_NODE_ADDR"
tab[DUNE_PLAYGROUND_PUBLIC_NODE_RPC_PORT]="$PUBLIC_NODE_RPC_PORT"
tab[DUNE_PLAYGROUND_PUBLIC_NODE_HTTPS]="$PUBLIC_NODE_HTTPS"

tab[DUNE_PLAYGROUND_TIME_BETWEEN_BLOCKS]=$TIME_BETWEEN_BLOCKS

tab[DUNE_PLAYGROUND_EXPLORER_ADDR]="$EXPLORER_ADDR"
tab[DUNE_PLAYGROUND_EXPLORER_PORT]="$EXPLORER_PORT"
tab[DUNE_PLAYGROUND_EXPLORER_HTTPS]="$EXPLORER_HTTPS"


tab[DUNE_PLAYGROUND_PUBLIC_API_ADDR]="$PUBLIC_API_ADDR"
tab[DUNE_PLAYGROUND_PUBLIC_API_PORT]="$PUBLIC_API_PORT"
tab[DUNE_PLAYGROUND_PUBLIC_API_HTTPS]="$PUBLIC_API_HTTPS"

tab[DUNE_PLAYGROUND_DB_USER]="$DB_USER"
tab[DUNE_PLAYGROUND_DB_NAME]="$DB_NAME"
tab[DUNE_PLAYGROUND_DB_HOST]="$DB_HOST"
tab[DUNE_PLAYGROUND_DB_PORT]=$DB_PORT

tab[DUNE_PLAYGROUND_DUNE_NETWORK_DIR]="$DUNE_NETWORK_DIR"
tab[DUNE_PLAYGROUND_LOVE_STANDALONE_DIR]="$LOVE_STANDALONE_DIR"

tab[DUNE_PLAYGROUND_CLIENT_CMD]="$CLIENT_CMD"
tab[DUNE_PLAYGROUND_ALERT_EMAIL_ADDRESSES]="$ALERT_EMAIL_ADDRESSES"
tab[DUNE_PLAYGROUND_NETWORK_CONFIG_FLAG]="$NETWORK_CONFIG_FLAG"

tab[DUNE_PLAYGROUND_LOCAL_NODE_ENABLED]="$LOCAL_NODE_ENABLED"
tab[DUNE_PLAYGROUND_LOCAL_NODE_ADDR]="$LOCAL_NODE_ADDR"
tab[DUNE_PLAYGROUND_LOCAL_NODE_P2P_PORT]="$LOCAL_NODE_P2P_PORT"
tab[DUNE_PLAYGROUND_LOCAL_NODE_RPC_PORT]="$LOCAL_NODE_RPC_PORT"
tab[DUNE_PLAYGROUND_LOCAL_NODE_BOOTSTRAP_PEERS]="$LOCAL_NODE_BOOTSTRAP_PEERS"
tab[DUNE_PLAYGROUND_LOCAL_NODE_DATA_DIR]="$LOCAL_NODE_DATA_DIR"
tab[DUNE_PLAYGROUND_LOCAL_NODE_DUNE_CONTEXT_STORAGE]="$LOCAL_NODE_DUNE_CONTEXT_STORAGE"
tab[DUNE_PLAYGROUND_BACK_DEBUG_LEVEL]="$BACK_DEBUG_LEVEL"
tab[DUNE_PLAYGROUND_FRONT_DEBUG_LEVEL]="$FRONT_DEBUG_LEVEL"
tab[DUNE_PLAYGROUND_CONTRACTS_BASE64]="$CONTRACTS_BASE64"

tab[DUNE_PLAYGROUND_UNIX_USER]="$UNIX_USER"
tab[DUNE_PLAYGROUND_UNIX_GROUP]="$UNIX_GROUP"
tab[DUNE_PLAYGROUND_ROOT_PATH]="$ROOT_PATH"

printf '\n'
for VAR in "${!tab[@]}"
do
    VALUE="${tab[$VAR]}"
    printf "> exporting $VAR=\"$VALUE\"\n"
    export $VAR="$VALUE"
done

printf "\n=+=+=+ exit $NAME +=+=++\n\n"

return
