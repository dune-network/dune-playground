#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

INIT="(\${pkh:playground_manager},\${contract:dgg_token},20p)"

echo INIT = $INIT

$DUNE_PLAYGROUND_CLIENT_CMD originate contract dgg_token_exchange \
                            transferring 0 from playground_manager \
                            running file:contract.lov.ml \
                            --init "#love:$INIT" --burn-cap 5 --force
