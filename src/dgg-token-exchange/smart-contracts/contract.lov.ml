#love

type storage = {
  owner : address;
  token_addr : address;
  conv_ratio : nat;
  next_div_ratio : dun;
}

val%init storage ((owner, token_addr, init_conv_ratio)  : (address * address * nat)) =
  if init_conv_ratio =[:nat] 0p then
    failwith [:string] ("Init conversion ratio is null.") [:unit];
  {
    owner = owner;
    token_addr = token_addr;
    conv_ratio = init_conv_ratio;
    next_div_ratio = 0dun (* will be initialized by an initial deposit *)
  }

contract type UNIT = sig
  val%entry default : unit
end

contract type TOKEN = sig
  val%entry burn : nat
end

val divide_by_two (bal : dun) : dun =
  match bal /$+ 2p with
  | None -> failwith [:string] "Invariant: not possible" [:dun]
  | Some (result, _rem) -> result

val rec auto_update_ratio (storage : storage) (bal : dun) : storage =
  if bal > [:dun] storage.next_div_ratio || bal =[:dun] 0DUN then
    storage
  else
    let storage =
      { storage with
        conv_ratio = storage.conv_ratio *+ 2p;
        next_div_ratio = divide_by_two storage.next_div_ratio }
    in
    auto_update_ratio storage bal

val%entry receiveTokens storage d ((from, amount, _) : (address * nat * bytes option)) =
  if Current.sender() <>[:address] storage.token_addr then
    failwith [:string * address * address]
      ("receiveToken can only be called by Token contract. (expected, got) = ",
       storage.token_addr, (Current.sender()))
      [:unit];
  let dun_amount =
    match (amount *+$ 1DUN) /$+ storage.conv_ratio with
    | None -> failwith [:string] "conversion ratio is null. Is the exchange initialized ?" [:dun]
    | Some (r, _) -> r
  in
  let bal1 = Current.balance () in
  match bal1  -$ dun_amount with
  | None -> (* bal1 < dun_amount *)
    failwith [:string * dun * dun]
      ("Requested DUNs amount is greater than current balance: ", dun_amount, bal1)
      [:operation list * storage];
  | Some bal2 ->
    (* maybe update ratio for next calls *)
    let storage = auto_update_ratio storage bal2 in
    let token = Contract.at <:TOKEN> storage.token_addr in
    match token with
    | None ->
      failwith [:string] "token address doesn't have burn entrypoint"
        [:operation list * storage]
    | Some (contract Token : TOKEN) ->
      [
        Account.transfer from dun_amount;
        Contract.call [:nat]  Token.burn 0DUN amount][:operation],
      storage

val%entry withdraw storage d (_ : unit) =
  if Current.sender() <>[:address] storage.owner then
    failwith [:string] "Can only be called by owner" [:unit];
  [ Account.transfer storage.owner (Current.balance()) ][:operation], storage

val%entry set_delegate storage d (delegate : address) =
  if Current.sender() <>[:address] storage.owner then
    failwith [:string] "Can only be called by owner" [:unit];
  let delegate = match Keyhash.of_address delegate with
      None -> failwith [:string * address] ("Not a valid keyhash", delegate) [:keyhash]
    | Some kh -> kh
  in
  [Contract.set_delegate (Some delegate [:keyhash])][:operation],
  storage

val%entry deposit storage d (_ : unit) =
  if storage.next_div_ratio <> [:dun] 0DUN then
    failwith [:string] ("Initial deposit already made.") [:unit];
  [][:operation], { storage with next_div_ratio = divide_by_two (Current.balance ()) }
