 /* global utils, parameters */

 var dggView = {

     updateView: function(data) {
         dgg_balance = (data.dgg_exchange_balance === null) ? '?' : data.dgg_exchange_balance;
         utils.setHTML('dgg-exchange-balance', dgg_balance);

         var next_msg = '';
         if (data.last_transfer_position !== null) {
             next_msg += 'Transfer operation';
             if (data.last_transfer_hash !== null) {
                 let op_hash = utils.explorerLink(data.last_transfer_hash, data.last_transfer_hash, true);
                 next_msg += ' (hash ' + op_hash + ')';
             }
             if (data.last_transfer_position < 0) {
                 next_msg += ' is waiting for inclusion.';
             } else {
                 next_msg += ' has been included ' + data.last_transfer_position + ' block(s) ago.';
             }
             next_msg = '<p class="pp" style="margin:0 80px">' + next_msg + '</p>';
         } else {
             next_msg += '';
         }
         utils.setHTML('dgg-send-status', next_msg);
         utils.setHTML('dgg-token-burner', parameters.dgg_token_exchange);
         utils.setHTML('dgg-exchange-ratio', data.dgg_exchange_ratio);
     },

     updateSentDGG: function(op) {
         if (op.ok && op.msg !== undefined) {
             var msg = '';
             let op_hash = utils.explorerLink(op.msg, op.msg, true);
             msg += '<p class="pp" style="margin:0 80px">Transfer operation injected with hash ' + op_hash + '</p>';
             utils.setHTML('dgg-send-status', msg);
         }
     },

     setExchangeAsDestTransfer: function() {
         utils.setValue('dgg-destination', parameters.dgg_token_exchange);
     }
 };