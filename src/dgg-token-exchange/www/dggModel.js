/* global parameters */
/* global nodeRPC */
/*  global metal */

var dggModel = {

    state: {
        inited: false,
        last_transfer_hash: null,
        last_transfer_position: null,
        dgg_exchange_ratio: null,
        dgg_exchange_balance: null,
        accounts_bigmap: null
    },


    __accountsMapID: function() {
        return new Promise(
            function(resolve, reject) {
                // TODO accounts_bigmap and players_bigmap should be the same ?? The latter doesn't appear in state
                if (dggModel.state.accounts_bigmap !== null) {
                    resolve(dggModel.state.players_bigmap);
                } else {
                    nodeRpcHelpers.bigMapID(parameters.dgg_token, e => e.accounts).then(function(res) {
                        dggModel.state.players_bigmap = res;
                        resolve(dggModel.state.players_bigmap);
                    }, function(err) { reject(utils.networkError(err)) });
                }
            });
    },

    __statusOfRecentChoice: function() {
        return new Promise(
            function(resolve, reject) {
                if (dggModel.state.last_transfer_position !== null || !dggModel.state.inited) {
                    // look in mempool, otherwise, OK ? also look in head ?
                    let metalState = metalModel.getState();
                    if (metalState === null || metalState.keyHash === null) {
                        resolve();
                    } else {
                        nodeRpcHelpers.trackOperation(parameters.node, dggModel.state.last_transfer_hash,
                            metalState.keyHash, parameters.dgg_token, 3).then(function(last_op) {
                            dggModel.state.last_transfer_position = last_op.position;
                            if (dggModel.state.last_transfer_hash === null) {
                                dggModel.state.last_transfer_hash = last_op.hash;
                            }
                            resolve();
                        }, function(err) {
                            dggModel.state.last_transfer_hash = null;
                            dggModel.state.last_transfer_position = null;
                            resolve();
                        });
                    }
                } else {
                    resolve();
                }
            });
    },
    __updateDggExchangeRatio: function() {
        return new Promise(
            function(resolve, reject) {
                nodeRPC.storage(parameters.dgg_token_exchange, parameters.node).then(function(res) {
                    dggModel.state.dgg_exchange_ratio = null;
                    if (res !== null && res.success) {
                        dggModel.state.dgg_exchange_ratio = parseInt(res.response.dune_expr.record.conv_ratio.nat);
                    }
                    resolve();
                });
            });
    },

    refreshModel: async function() {
        try {
            await dggModel.__statusOfRecentChoice();
            await dggModel.__updateDggExchangeRatio();
            let dunBal = await nodeRPC.balance(parameters.dgg_token_exchange, parameters.node);
            if (dunBal.success) {
                dggModel.state.dgg_exchange_balance = parseInt(dunBal.response) / 1000000;
            }
            return (JSON.parse(JSON.stringify(dggModel.state)));
        } catch (error) {
            throw (utils.networkError(error));
        }
    },

    sendDGG: function(dest, amount) {
        return new Promise(
            function(resolve, reject) {
                if (typeof metal === 'undefined' || metal === undefined) {
                    reject(utils.metalNotDetected);
                } else {
                    metal.isEnabled(function(res) {
                        if (!res) {
                            reject(utils.metalNotConfigured);
                        }
                        metal.getAccount(function(r) {
                            let op = {
                                dst: parameters.dgg_token,
                                network: parameters.network,
                                amount: '0',
                                entrypoint: 'transfer',
                                parameter: '#love:(' + dest + ',' + amount + 'p, None)',
                                gas_limit: '197000',
                                storage_limit: '77',
                                cb: function(res) {
                                    if (res.ok) {
                                        dggModel.state.last_transfer_hash = res.msg;
                                        dggModel.state.last_transfer_position = -1; // mempool
                                    }
                                    resolve(res);
                                }
                            };
                            metal.send(op);
                        });
                    });
                }
            }
        );
    }
};
