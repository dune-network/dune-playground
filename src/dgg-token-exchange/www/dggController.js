/* global dggModel */
/* global dggView, utils */

var dggController = {

    showPage: function(autoReload) {
        utils.setWaitingMsg(utils.loadingPage, autoReload).then(function() {
            dggModel.refreshModel().then(function(data) {
                dggView.updateView(data);
                utils.unsetWaitingMsg(false /* not autoReload*/ ).then(function() {});
            }, utils.modelErrHandler).catch(utils.modelErrHandler);
        });
    },

    sendDGG: function() {
        var dest = utils.getValue('dgg-destination');
        var amount = parseInt(utils.getValue('dgg-amount'));
        dggModel.sendDGG(dest, amount).then(function(res) {
            dggView.updateSentDGG(res);
        }, function(err) {
            console.log(err);
        });
    },

    setExchangeAsDestTransfer: function() {
        dggView.setExchangeAsDestTransfer();
    }
};