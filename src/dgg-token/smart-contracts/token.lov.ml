#love

type accounts = (address, nat) bigmap

type metadata = {
  version : nat * nat; (* version of token standard *)
  decimals : nat;
  name : string;
  symbol : string;
}

(* Implementation dependent: can be any type *)
type internal_storage = {
  owner : address;
  rest : unit;
}

type storage = {
  accounts : accounts;
  totalSupply : nat;
  metadata : metadata;
  internStore : internal_storage;
}

contract type TokenReceiver = sig
  type storage
  val%entry receiveTokens : (address * nat * bytes option)
end

val%init storage
    ((owner, decimals, name, symbol) : address * nat * string * string) =
  {
    accounts = BigMap.empty [:address] [:nat];
    totalSupply = 0p;
    metadata = {
      version = 1p, 0p; (* Version of standard : 1.0 *)
      decimals;
      name;
      symbol;
    };
    internStore = {
      owner;
      rest = ();
    }
  }

(* Helper functions *)

val no_base_currency (() : unit) : unit =
  if Current.amount () <> 0DUN then failwith [:string] "Don't send DUN" [:unit]

val get_balance ((a, accounts) : address * accounts) : nat =
  match BigMap.find [:address] [:nat] a accounts with
  | None -> 0p
  | Some balance -> balance

val set_balance ((a, balance, accounts) : address * nat * accounts) : accounts =
    if balance = 0p then BigMap.remove [:address] [:nat] a accounts
    else BigMap.add [:address] [:nat] a balance accounts

val perform_transfer
    ((dest, tokens, accounts) : address * nat * accounts) : accounts =
  (* Decrease balance of sender *)
  let sender_balance = get_balance (Current.sender (), accounts) in
  let new_sender_balance = match Nat.of_int (sender_balance -+ tokens) with
    | None ->
        failwith [:string * nat]
          ("Not enough tokens for transfer", sender_balance) [:nat]
    | Some b -> b in
  let accounts =
    set_balance (Current.sender (), new_sender_balance, accounts) in
  (* Increase balance of destination *)
  let dest_balance = get_balance (dest, accounts) in
  let new_dest_balance = dest_balance ++ tokens in
  let accounts = set_balance (dest, new_dest_balance, accounts) in
  accounts

(*------------------ Transfer tokens --------------------*)

val%entry transfer storage duns
    ((dest, tokens, data) : address * nat * bytes option) =
  no_base_currency ();
  let accounts = perform_transfer (dest, tokens, storage.accounts) in
  let ops = match Contract.at<:TokenReceiver> dest with
    | Some (contract DestC : TokenReceiver) ->
      [DestC.receiveTokens 0DUN (Current.sender (), tokens, data)] [:operation]
    | None -> match data with
      | None -> [] [:operation]
      | Some _ ->
          failwith [:string]
            "Cannot send data to a non TokenReceiver contract" [:operation list]
  in
  ops, { storage with accounts }


(* --------------- Storage access from outside ---------------- *)

val%view balanceOf storage (addr : address) : nat =
  get_balance (addr, storage.accounts)


(* ------------------ Burning tokens  ------------------------ *)

val%entry burn storage duns (tokens : nat) =
  no_base_currency ();
  let accounts = storage.accounts in
  (* Decrease balance of sender *)
  let sender_balance = get_balance (Current.sender (), accounts) in
  let new_sender_balance = match Nat.of_int (sender_balance -+ tokens) with
    | None ->
        failwith [:string * nat]
          ("Not enough tokens for transfer", sender_balance) [:nat]
    | Some b -> b in
  let accounts =
    set_balance (Current.sender (), new_sender_balance, accounts) in
  let totalSupply = match Nat.of_int (storage.totalSupply -+ tokens) with
    | None -> failwith [:unit] () [:nat]
    | Some t -> t in
  [] [:operation], { storage with accounts; totalSupply }


(* ------------------ Minting tokens ------------------------ *)
(* Not part of standard *)

val%entry mint storage duns (new_accounts : (address * nat) list) =
  no_base_currency ();
  if Current.sender () <> [:address] storage.internStore.owner then
    failwith [:string] "Only owner can create accounts" [:unit];
  let accounts, totalSupply =
    List.fold [:address * nat] [:accounts * nat] (
      fun ((dest, tokens) : address * nat) (acc : accounts * nat) ->
        let accounts, totalSupply = acc in
        if tokens = 0p then acc
        else
          let balance = get_balance (dest, accounts) in
          (BigMap.add [:address] [:nat] dest (balance ++ tokens) accounts,
           totalSupply ++ tokens)
      ) new_accounts (storage.accounts, storage.totalSupply) in
  [] [:operation], { storage with accounts; totalSupply }
