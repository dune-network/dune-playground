#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

MINT_DEST=$1

if [ "$MINT_DEST" = "" ]; then
    echo "You should provide the alias of the address to fund as first argument!"
    exit 1
fi

$DUNE_PLAYGROUND_CLIENT_CMD transfer 0 \
                            from playground_manager to dgg_token \
                            --entrypoint mint \
                            --arg '#love:[ ${contract:'$MINT_DEST'} , 10000p ]' \
                            --burn-cap 4
