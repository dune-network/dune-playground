#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."

if [ "$DUNE_PLAYGROUND_CLIENT_CMD" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_CLIENT_CMD env variable"
    exit 1
fi

DECIMALS="0p"
NAME="Dune Good Game"
SYMBOL="DGG"

INIT="(\${pkh:playground_manager}, $DECIMALS , \"$NAME\" , \"$SYMBOL\")"

echo INIT = $INIT

$DUNE_PLAYGROUND_CLIENT_CMD originate contract dgg_token \
                            transferring 0 from playground_manager \
                            running file:./token.lov.ml \
                            --init "#love:$INIT" --burn-cap 5 --force
