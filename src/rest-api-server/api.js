require('log-timestamp');
const axios = require('axios').default;
const utils = require('utils-lib');
const ezdun = require('ezdun');
const sha1 = require('sha1');

// messages
const invalid_dn1 = "Provided dn1 is invalid!";
const invalid_captcha = "Provided captcha is invalid!";
const db_access_failed = "Access to database failed!";
const dn1_saved = "Your dn1 has been saved in our databases. It should be processed soon!";
const unexpected_error = "An unexpected error occured!";
const kt1_not_supported = "KT1 addresses are not supported!"
const invalid_kind = "Provided kind is invalid (should be one of: hourly, daily, weekly)!";

function dn1_waiting_for_airdrop(blocking_airdrop_period) {
    return `The provided dn1 is already waiting to be airdropped, or has been airdropped less than ${blocking_airdrop_period} minutes ago.`;
};

debug = function(config, lvl, msg) {
    if (lvl < config.back_debug_level) {
        console.log(msg);
    }
};

valid_dn1 = function(addr) {
    return (ezdun.eztz.crypto.checkAddress(addr));
};

valid_kind = function(kind) {
    switch (kind) {
        case 'hourly':
        case 'daily':
        case 'weekly':
            return true;
        default:
            return false;
    }
};


airdropDuns = function(config, pool, request, response) {
    console.log('POST /airdrop-duns');
    const { dn1, gcaptcha } = request.body;

    debug(config, 3, `received request: ` + JSON.stringify(request.body));
    debug(config, 0, `request to airdrop: ` + dn1);
    if (!valid_dn1(dn1)) {
        debug(config, 0, invalid_dn1);
        response.status(200).send(invalid_dn1);
        return;
    }
    let secret = config.google_captcha_secret_key;
    const data = `secret=${secret}&response=${gcaptcha}`;
    axios.post('https://www.google.com/recaptcha/api/siteverify', data).then(function(result0) {
        let result = result0.data;
        if (!result.success) {
            debug(config, 0, "Error:" + invalid_captcha + " : " + JSON.stringify(result));
            debug(config, 0, `captcha verification FAILED :: ${dn1} > ${gcaptcha}`);
            response.status(200).send(invalid_captcha);
            return;
        }
        debug(config, 0, `captcha verification succeeded :: ${dn1} > ${gcaptcha}`);
        let req = `SELECT * from accounts_to_airdrop WHERE dn1_ = '${dn1}'`;
        pool.query(req, (err, res) => {
            if (err) {
                debug(config, 0, db_access_failed + "(1)\n" + err);
                response.status(200).send(db_access_failed);
                return;
            }
            if (res.rowCount !== 0) {
                response.status(200).send(dn1_waiting_for_airdrop(config.duns_airdrop.blocking_airdrop_period));
                return;
            }
            let req = `SELECT count(dn1_) as nb_airdrops FROM airdropped_accounts where dn1_ = '${dn1}'`;
            pool.query(req, (err, nb_airdrops) => {
                if (err) {
                    debug(config, 0, db_access_failed + "(2)\n" + err);
                    response.status(200).send(db_access_failed);
                    return;
                }
                let nb_airdropped = parseInt(nb_airdrops.rows[0].nb_airdrops);
                let req = `INSERT INTO accounts_to_airdrop (dn1_, nb_airdrops_) VALUES ('${dn1}', ${nb_airdropped})`;
                pool.query(req, (err) => {
                    if (err) {
                        debug(config, 0, db_access_failed + "(3)\n" + err);
                        response.status(200).send(db_access_failed);
                        return;
                    }
                    let req = 'SELECT count(*) from accounts_to_airdrop WHERE op_ IS NULL';
                    pool.query(req, (err, res) => {
                        if (err) {
                            msg = dn1_saved;
                        } else {
                            console.log('config.duns_airdrop.operations_per_airdrop = ' + config.duns_airdrop.operations_per_airdrop);

                            let count = res.rows[0].count;
                            let blocks = 1 + Math.ceil(count / config.duns_airdrop.operations_per_airdrop);
                            let minutes = Math.ceil((blocks * config.time_between_blocks) / 60);
                            msg = dn1_saved + " (Estimated Wait Time: " + minutes + " minutes)";
                        }
                        debug(config, 0, msg);
                        response.status(201).send(msg);
                        return;
                    });
                });
            });

        });
    }).catch(function(error) {
        debug(config, 0, "catch 1:" + error);
        response.status(200).send(unexpected_error);
        return;
    });
};


contractGames = function(config, pool, table, request, response) {
    console.log('GET games from table ' + table);
    try {
        let addr = request.params.address;
        let page = parseInt(request.query.page);
        let size = parseInt(request.query.size);
        page = Number.isInteger(page) ? page : 0;
        size = Number.isInteger(size) ? Math.max(1, Math.min(size, 50)) : 50;
        let offset = page * size;
        console.log(`address ${addr}: request to provide page ${page} of size ${size} (offset = ${offset})`);
        if (!valid_dn1(addr)) {
            debug(config, 0, invalid_dn1);
            response.status(500).send(invalid_dn1);
            return;
        }
        let req = `
            select game_id_, op_played_, op_revealed_, game_block_level_, player_, player_choice_, oracle_choice_, status_, reward_, failed_
            from  ${table}
            where player_ = $1 order by game_block_level_ desc limit $2 offset $3;`
        pool.query(req, [addr, size, offset], (err, res) => {
            if (err) {
                debug(config, 0, err);
                response.status(400).send('DB error!');
            } else {
                debug(config, 0, `request result has ${res.rows.length} rows`);
                response.status(200).send(res.rows);
            }
        });
    } catch (e) {
        console.log('error: ' + e);
        response.status(400).send('Server Error!');
    }
};


rpsGames = function(config, pool, request, response) {
    console.log('GET /rps-games/:address');
    contractGames(config, pool, 'crawled_rps_with_server', request, response)
};


moonshotGames = function(config, pool, request, response) {
    console.log('GET /moonshot-games/:address');
    contractGames(config, pool, 'crawled_moonshot_with_server', request, response)
};

mk_raffle_table = function(kind, entrypoint) {
    return `${kind}_raffle__${entrypoint}__autocrawled`
};


rafflePlayers = function(config, pool, request, response) {
    try {
        let gid = parseInt(request.params.game_id);
        let kind = request.query.kind;
        console.log(`Request to provide players of raffle game ${gid} for kind = ${kind}`);
        if (!valid_kind(kind)) {
            debug(config, 0, invalid_kind);
            response.status(500).send(invalid_kind);
            return;
        }
        let play = mk_raffle_table(kind, 'play');
        let player_reveal = mk_raffle_table(kind, 'player_reveal');
        let req = `
          SELECT ${play}.player_, ${play}.commit_, FOO.secret_
          FROM ${play}
          LEFT JOIN (select * from ${player_reveal} where ${player_reveal}.game_id_ = $1) AS FOO  ON ${play}.player_ = FOO.player_
          WHERE ${play}.game_id_ = $1;`;
        pool.query(req, [gid], (err, res) => {
            if (err) {
                debug(config, 0, err);
                response.status(400).send('DB error!');
            } else {
                debug(config, 0, `request result has ${res.rows.length} rows`);
                response.status(200).send(res.rows.reverse());
            }
        });
    } catch (e) {
        console.log('error: ' + e);
        response.status(400).send('Server Error!');
    }
};


raffleGames = function(config, pool, request, response) {
    try {
        let addr = request.params.address;
        let kind = request.query.kind;
        let page = parseInt(request.query.page);
        let size = parseInt(request.query.size);
        page = Number.isInteger(page) ? page : 0;
        size = Number.isInteger(size) ? Math.max(1, Math.min(size, 50)) : 50;
        let offset = page * size;
        console.log(`address ${addr}: request to provide page ${page} of size ${size} (offset = ${offset}) with kind = ${kind}`);
        if (!valid_dn1(addr) & addr !== 'all') {
            debug(config, 0, invalid_dn1);
            response.status(500).send(invalid_dn1);
            return;
        }
        if (!valid_kind(kind)) {
            debug(config, 0, invalid_kind);
            response.status(500).send(invalid_kind);
            return;
        }
        let org_reveal = mk_raffle_table(kind, 'organizer_reveal');
        let org_denounce = mk_raffle_table(kind, 'denounce_organizer');
        let play = mk_raffle_table(kind, 'play');
        var req = '';
        if (addr === 'all') {
            req = `
                SELECT ${org_reveal}.block_hash_, ${org_reveal}.game_id_, ${org_reveal}.result_, ${org_reveal}.winner_,
                       ${org_reveal}.nb_regular_, ${org_reveal}.nb_committed_, ${org_reveal}.nb_revealed_commits_, ${org_reveal}.inserted_on_
                    FROM ${org_reveal}
                UNION
                SELECT ${org_denounce}.block_hash_, ${org_denounce}.game_id_, 'Expired' AS result_, null AS winner_,
                       ${org_denounce}.nb_regular_, ${org_denounce}.nb_committed_, ${org_denounce}.nb_revealed_commits_, ${org_denounce}.inserted_on_
                FROM ${org_denounce}
                ORDER BY game_id_ DESC
                LIMIT $1 OFFSET $2;`
        } else {
            req = `
            SELECT ${org_reveal}.block_hash_, ${org_reveal}.game_id_, ${org_reveal}.result_, ${org_reveal}.winner_,
                    ${org_reveal}.nb_regular_, ${org_reveal}.nb_committed_, ${org_reveal}.nb_revealed_commits_, ${org_reveal}.inserted_on_
                FROM ${org_reveal}, ${play}
                WHERE ${play}.game_id_ = ${org_reveal}.game_id_
                AND ${play}.player_ = '${addr}'
            UNION
            SELECT ${org_denounce}.block_hash_, ${org_denounce}.game_id_, 'Expired' AS result_, null AS winner_,
                   ${org_denounce}.nb_regular_, ${org_denounce}.nb_committed_, ${org_denounce}.nb_revealed_commits_, ${org_denounce}.inserted_on_
                FROM ${org_denounce}, ${play}
                WHERE ${play}.game_id_ = ${org_denounce}.game_id_
                AND ${play}.player_ = '${addr}'
            ORDER BY game_id_ DESC
            LIMIT $1 OFFSET $2;`
        }
        pool.query(req, [size, offset], (err, res) => {
            if (err) {
                debug(config, 0, err);
                response.status(400).send('DB error!\n' + err);
            } else {
                debug(config, 0, `request result has ${res.rows.length} rows`);
                response.status(200).send(res.rows);
            }
        });
    } catch (e) {
        console.log('error: ' + e);
        response.status(400).send('Server Error!');

    }
};

var cache = [];
var pending_operations_cache = null;

objIsEmpty = function(obj) {
    return (Object.keys(obj).length === 0);
}

var curr_head_hash = "";
var curr_head_hash_milliseconds = 0;

clearMempoolCache = function() {
    //console.log('clear cache of /chains/main/mempool/pending_operations/');
    pending_operations_cache = null; // mempool/pending_operations cache reset every 2 seconds
}

clearCache = async function(node_addr) {
    console.log();
    console.log("clear cache ? ");
    let now_utc = new Date(new Date().toUTCString()).getTime();
    let diff = now_utc - curr_head_hash_milliseconds;
    console.log(`since last received head: ${diff / 1000} seconds`);
    if (diff <= 60000) { // less than 60 seconds. Should be parameterized
        console.log('No ! no block yet (assuming the node is boostrapped)');
        return;
    }
    let result = await axios.get(node_addr + '/chains/main/blocks/head/header');
    new_head_hash = result.data.hash;
    if (new_head_hash === curr_head_hash) {
        console.log('No ! head hash is the equal to the last seen');
        return;
    }
    let timestamp = result.data.timestamp;
    let ts_milliseconds = new Date(timestamp).getTime();
    console.log('cache size before ' + Object.keys(cache).length);
    cache = [];
    console.log('cache size after ' + Object.keys(cache).length);
    curr_head_hash = new_head_hash;
    curr_head_hash_milliseconds = ts_milliseconds;
    console.log('cache reset!');
};

var clearCacheTimer = null;
var clearMempoolCacheTimer = setInterval(clearMempoolCache, 2000);

initTimer = function(node_addr) {
    let aux = async function() {
        await clearCache(node_addr);
    };
    if (clearCacheTimer === null) {
        clearCacheTimer = setInterval(aux, 2000);
    }
};

nodeRPC = async function(config, pool, request, response) {
    node_addr = utils.mk_request_prefix(config.node_info);
    if (clearCacheTimer === null) {
        initTimer(node_addr);
    }
    let req = request.query.request;
    if (req === "/chains/main/mempool/pending_operations" || req === "/chains/main/mempool/pending_operations/") {
        // should not be cached or cache purged more often (every two seconds currently)
        var res = pending_operations_cache;
        if (res !== undefined && res !== null) {
            console.log(`${req} found in cache with value \${JSON.stringify(res)}`);
        } else {
            result = await axios.get(node_addr + req);
            res = result.data;
            console.log(`new res to be added in cache for ${req} : \${JSON.stringify(res)}`);
            pending_operations_cache = res;
        }
        response.status(200).send(JSON.stringify(res));
    } else {
        let data = request.body;
        console.log("nodeRPC: req = " + JSON.stringify(req) + " body = " + JSON.stringify(data));
        let hash = sha1(req + JSON.stringify(data));

        var res = cache[hash]; // access cache once in an atomic way
        if (res !== undefined) {
            console.log(`${req} with data ${JSON.stringify(data)} and hash ${hash} found in cache with value \${JSON.stringify(res)}`);
        } else {
            var result;
            if (objIsEmpty(data)) {
                result = await axios.get(node_addr + req);
            } else {
                result = await axios.post(node_addr + req, data, { headers: { 'Content-Type': 'application/json' } });
            }
            res = result.data;
            console.log(`new res to be added in cache for ${req} with data ${JSON.stringify(data)} and hash ${hash} : \${JSON.stringify(res)}`);
            cache[hash] = res;
        }
        response.status(200).send(JSON.stringify(res));
    }
}

module.exports = {
    airdropDuns,
    rpsGames,
    moonshotGames,
    rafflePlayers,
    raffleGames,
    nodeRPC
};