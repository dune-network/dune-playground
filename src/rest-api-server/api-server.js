const bodyParser = require('body-parser');
const express = require('express');
const server = express();
const api = require('./api');
const Pool = require('pg').Pool;
const configLib = require('config-lib');

var config = configLib.parseArgs();
const pool = new Pool(config.database);

server.use(bodyParser.json());

server.use(
    bodyParser.urlencoded({
        extended: true
    })
);

server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

server.post('/airdrop-duns', function(request, response) {
    api.airdropDuns(config, pool, request, response);
});


server.get('/rps-games/:address', function(request, response) {
    api.rpsGames(config, pool, request, response);
});


server.get('/moonshot-games/:address', function(request, response) {
    api.moonshotGames(config, pool, request, response);
});

server.get('/raffle-players/:game_id', function(request, response) {
    api.rafflePlayers(config, pool, request, response);
});


server.get('/raffle-games/:address', function(request, response) {
    api.raffleGames(config, pool, request, response);
});

server.get('/nodeRPC/', function(request, response) {
    api.nodeRPC(config, pool, request, response);
});

server.post('/nodeRPC/', function(request, response) {
    api.nodeRPC(config, pool, request, response);
});


server.get('/about', function(request, response) {
    response.status(200).send('REST API server for Dune Playground');
});

server.listen(config.local_api_server.port, config.local_api_server.addr, () => {
    console.log(`Server running on ${config.local_api_server.addr}:${config.local_api_server.port}.`);
});