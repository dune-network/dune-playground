---------------------------------------------
--- See README.md for database configuration
---------------------------------------------

-- Drop tables and types in DB

DROP TABLE accounts_to_airdrop;
DROP TABLE airdropped_accounts;
DROP TABLE crawled_rps_with_server CASCADE;
DROP TABLE crawled_moonshot_with_server CASCADE;
DROP TYPE RPS_CHOICE_ CASCADE;
DROP TYPE STATUS_ CASCADE;

DROP TABLE serverless_rps_commits;
DROP TABLE rps_with_server_commits;
DROP TABLE serverless_moonshot_commits;
DROP TABLE moonshot_with_server_commits;
DROP TABLE hourly_raffle_commits;
DROP TABLE daily_raffle_commits;
DROP TABLE weekly_raffle_commits;

-- Create types again
CREATE TYPE RPS_CHOICE_ AS ENUM ('Rock', 'Paper', 'Scissors');
CREATE TYPE STATUS_ AS ENUM ('PlayerWins', 'PlayerLoses', 'Tie');

-- Create tables

CREATE TABLE accounts_to_airdrop(
     id_ SERIAL,                 -- SERIAL or timestamp: used to sort the table
                                 -- to airdrop accounts in a FIFO style
     dn1_ CHAR(36) PRIMARY KEY,  -- the dn1 that'll receive the airdrop
     nb_airdrops_ INT DEFAULT 0,  -- how many times the dn1 has already been airdropped
     op_ CHAR(51) DEFAULT NULL,  -- operation hash, once airdrop is done
     op_level_ INT DEFAULT NULL  -- block level of the airdrop
);

CREATE TABLE airdropped_accounts(
     dn1_ CHAR(36),   -- airdropped dn1
     op_ CHAR(52),    -- operation hash in which it has been airdropped
     op_level_ INT,   -- block level of the airdrop
     PRIMARY KEY (dn1_, op_level_),
     UNIQUE (dn1_, op_)
);


CREATE TABLE crawled_rps_with_server(
    op_played_ CHAR(51),      -- Op hash in which the player made his choice
    block_played_ CHAR(51),   -- Block hash in which the player made his choice
    level_played_ BIGINT,     -- Level of the block in which the player made his choice

    op_revealed_ CHAR(51),    -- Op hash in which the oracle revealed his choice
    block_revealed_ CHAR(51), -- Block hash in which the oracle revealed his choice
    level_revealed_ BIGINT,   -- Level of the block in which the oracle revealed his choice

    game_id_ INT,    -- The game's ID in the smart contract
    game_block_level_ INT,       -- same as level_played_, kept for readability
    player_ CHAR(36),            -- player's dn1
    player_choice_ RPS_CHOICE_ , -- player's choice

    oracle_choice_ RPS_CHOICE_,  -- oracle's revealed choice
    status_ STATUS_,             -- game's status NULL = Ongoing (if not failed)
    reward_ INT,                 -- amount of rewards
    failed_ BOOLEAN,             -- true if the player's operation failed.
                                 -- In this case, no reveal should be made
    delegated_ BOOLEAN          -- true if player delegated the injection of
                                 -- his move to our server

    -- PRIMARY KEY (game_id_, game_block_level_, failed_) not correct in case failed_ is true
);

CREATE TABLE crawled_moonshot_with_server(
    op_played_ CHAR(51),      -- Op hash in which the player made his choice
    block_played_ CHAR(51),   -- Block hash in which the player made his choice
    level_played_ BIGINT,     -- Level of the block in which the player made his choice

    op_revealed_ CHAR(51),    -- Op hash in which the oracle revealed his choice
    block_revealed_ CHAR(51), -- Block hash in which the oracle revealed his choice
    level_revealed_ BIGINT,   -- Level of the block in which the oracle revealed his choice

    game_id_ INT,    -- The game's ID in the smart contract
    game_block_level_ INT,       -- same as level_played_, kept for readability
    player_ CHAR(36),            -- player's dn1
    player_choice_ INT , -- player's choice

    oracle_choice_ INT,  -- oracle's revealed choice
    status_ STATUS_,             -- game's status NULL = Ongoing (if not failed)
    reward_ INT,                 -- amount of rewards
    failed_ BOOLEAN,             -- true if the player's operation failed.
                                 -- In this case, no reveal should be made
    delegated_ BOOLEAN          -- true if player delegated the injection of
                                 -- his move to our server

    -- PRIMARY KEY (game_id_, game_block_level_, failed_) not correct in case failed_ is true
);

create table serverless_rps_commits (
       id_ INTEGER PRIMARY KEY,
       commit_ VARCHAR(100) NOT NULL,
       choice_ VARCHAR(5) NOT NULL,
       nonce_ VARCHAR(100) NOT NULL,
       inserted_level_ INT NOT NULL,
       op_hash_ CHAR(53),
       included_ BOOLEAN
);

create table rps_with_server_commits (
       id_ INTEGER PRIMARY KEY,
       commit_ VARCHAR(100) NOT NULL,
       choice_ VARCHAR(5) NOT NULL,
       nonce_ VARCHAR(100) NOT NULL,
       inserted_level_ INT NOT NULL,
       op_hash_ CHAR(53),
       included_ BOOLEAN
);

create table serverless_moonshot_commits (
       id_ INTEGER PRIMARY KEY,
       commit_ VARCHAR(100) NOT NULL,
       choice_ INTEGER NOT NULL,
       nonce_ VARCHAR(100) NOT NULL,
       inserted_level_ INT NOT NULL,
       op_hash_ CHAR(53),
       included_ BOOLEAN
);

create table moonshot_with_server_commits (
       id_ INTEGER PRIMARY KEY,
       commit_ VARCHAR(100) NOT NULL,
       choice_ INTEGER NOT NULL,
       nonce_ VARCHAR(100) NOT NULL,
       inserted_level_ INT NOT NULL,
       op_hash_ CHAR(53),
       included_ BOOLEAN
);

-- DUNE RAFFLE STUFF

create table hourly_raffle_commits (
       id_ INTEGER PRIMARY KEY,
       commit_ VARCHAR(150) NOT NULL,
       secret_ BIGINT NOT NULL,
       nonce_  BIGINT NOT NULL,
       inserted_level_ INT NOT NULL,
       op_hash_ CHAR(53),
       included_ BOOLEAN
);

CREATE INDEX hourly_raffle_commits__included_ on hourly_raffle_commits(included_);

create table daily_raffle_commits (
       id_ INTEGER PRIMARY KEY,
       commit_ VARCHAR(150) NOT NULL,
       secret_ BIGINT NOT NULL,
       nonce_  BIGINT NOT NULL,
       inserted_level_ INT NOT NULL,
       op_hash_ CHAR(53),
       included_ BOOLEAN
);

CREATE INDEX daily_raffle_commits__included_ on daily_raffle_commits(included_);

create table weekly_raffle_commits (
       id_ INTEGER PRIMARY KEY,
       commit_ VARCHAR(150) NOT NULL,
       secret_ BIGINT NOT NULL,
       nonce_  BIGINT NOT NULL,
       inserted_level_ INT NOT NULL,
       op_hash_ CHAR(53),
       included_ BOOLEAN
);

CREATE INDEX weekly_raffle_commits__included_ on weekly_raffle_commits(included_);

-- <begin: crawling tables for raffle automatically generated by raffle-crawler.js>

DROP TYPE raffle_final_status CASCADE;

CREATE TYPE raffle_final_status AS ENUM ('End', 'Compromised', 'Expired');

DROP TABLE raffle_seen_blocks CASCADE;

CREATE TABLE raffle_seen_blocks (hash_ CHAR(53) PRIMARY KEY, level_ BIGINT NOT NULL UNIQUE);

CREATE INDEX raffle_seen_blocks_IndexOn_level_ ON raffle_seen_blocks(level_);


DROP TABLE hourly_raffle__play__autocrawled CASCADE;

CREATE TABLE hourly_raffle__play__autocrawled(
  game_id_ INT,
  commit_ VARCHAR(150),
  player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX hourly_raffle__play__autocrawled_IndexOn_game_id_ ON hourly_raffle__play__autocrawled(game_id_);

CREATE INDEX hourly_raffle__play__autocrawled_IndexOn_player_ ON hourly_raffle__play__autocrawled(player_);

CREATE INDEX hourly_raffle__play__autocrawled_IndexOn_game_id__player_ ON hourly_raffle__play__autocrawled(game_id_, player_);

DROP TABLE hourly_raffle__player_reveal__autocrawled CASCADE;

CREATE TABLE hourly_raffle__player_reveal__autocrawled(
  game_id_ INT,
  secret_ BIGINT,
  nonce_ BIGINT,
  player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX hourly_raffle__player_reveal__autocrawled_IndexOn_game_id_ ON hourly_raffle__player_reveal__autocrawled(game_id_);

CREATE INDEX hourly_raffle__player_reveal__autocrawled_IndexOn_player_ ON hourly_raffle__player_reveal__autocrawled(player_);

DROP TABLE hourly_raffle__reimburse_players__autocrawled CASCADE;

CREATE TABLE hourly_raffle__reimburse_players__autocrawled(
  game_id_ INT,
  refunded_player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX hourly_raffle__reimburse_players__autocrawled_IndexOn_game_id_ ON hourly_raffle__reimburse_players__autocrawled(game_id_);

DROP TABLE hourly_raffle__organizer_reveal__autocrawled CASCADE;

CREATE TABLE hourly_raffle__organizer_reveal__autocrawled(
  game_id_ INT,
  secret_ BIGINT,
  nonce_ BIGINT,
  winner_ CHAR(36),
  result_ raffle_final_status,
  nb_regular_ INT,
  nb_committed_ INT,
  nb_revealed_commits_ INT,
  inserted_on_ BIGINT,
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX hourly_raffle__organizer_reveal__autocrawled_IndexOn_game_id_ ON hourly_raffle__organizer_reveal__autocrawled(game_id_);

DROP TABLE hourly_raffle__denounce_organizer__autocrawled CASCADE;

CREATE TABLE hourly_raffle__denounce_organizer__autocrawled(
  game_id_ INT,
  nb_regular_ INT,
  nb_committed_ INT,
  nb_revealed_commits_ INT,
  inserted_on_ BIGINT,
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX hourly_raffle__denounce_organizer__autocrawled_IndexOn_game_id_ ON hourly_raffle__denounce_organizer__autocrawled(game_id_);

DROP TABLE daily_raffle__play__autocrawled CASCADE;

CREATE TABLE daily_raffle__play__autocrawled(
  game_id_ INT,
  commit_ VARCHAR(150),
  player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX daily_raffle__play__autocrawled_IndexOn_game_id_ ON daily_raffle__play__autocrawled(game_id_);

CREATE INDEX daily_raffle__play__autocrawled_IndexOn_player_ ON daily_raffle__play__autocrawled(player_);

CREATE INDEX daily_raffle__play__autocrawled_IndexOn_game_id__player_ ON daily_raffle__play__autocrawled(game_id_, player_);

DROP TABLE daily_raffle__player_reveal__autocrawled CASCADE;

CREATE TABLE daily_raffle__player_reveal__autocrawled(
  game_id_ INT,
  secret_ BIGINT,
  nonce_ BIGINT,
  player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX daily_raffle__player_reveal__autocrawled_IndexOn_game_id_ ON daily_raffle__player_reveal__autocrawled(game_id_);

CREATE INDEX daily_raffle__player_reveal__autocrawled_IndexOn_player_ ON daily_raffle__player_reveal__autocrawled(player_);

DROP TABLE daily_raffle__reimburse_players__autocrawled CASCADE;

CREATE TABLE daily_raffle__reimburse_players__autocrawled(
  game_id_ INT,
  refunded_player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX daily_raffle__reimburse_players__autocrawled_IndexOn_game_id_ ON daily_raffle__reimburse_players__autocrawled(game_id_);

DROP TABLE daily_raffle__organizer_reveal__autocrawled CASCADE;

CREATE TABLE daily_raffle__organizer_reveal__autocrawled(
  game_id_ INT,
  secret_ BIGINT,
  nonce_ BIGINT,
  winner_ CHAR(36),
  result_ raffle_final_status,
  nb_regular_ INT,
  nb_committed_ INT,
  nb_revealed_commits_ INT,
  inserted_on_ BIGINT,
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX daily_raffle__organizer_reveal__autocrawled_IndexOn_game_id_ ON daily_raffle__organizer_reveal__autocrawled(game_id_);

DROP TABLE daily_raffle__denounce_organizer__autocrawled CASCADE;

CREATE TABLE daily_raffle__denounce_organizer__autocrawled(
  game_id_ INT,
  nb_regular_ INT,
  nb_committed_ INT,
  nb_revealed_commits_ INT,
  inserted_on_ BIGINT,
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX daily_raffle__denounce_organizer__autocrawled_IndexOn_game_id_ ON daily_raffle__denounce_organizer__autocrawled(game_id_);

DROP TABLE weekly_raffle__play__autocrawled CASCADE;

CREATE TABLE weekly_raffle__play__autocrawled(
  game_id_ INT,
  commit_ VARCHAR(150),
  player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX weekly_raffle__play__autocrawled_IndexOn_game_id_ ON weekly_raffle__play__autocrawled(game_id_);

CREATE INDEX weekly_raffle__play__autocrawled_IndexOn_player_ ON weekly_raffle__play__autocrawled(player_);

CREATE INDEX weekly_raffle__play__autocrawled_IndexOn_game_id__player_ ON weekly_raffle__play__autocrawled(game_id_, player_);

DROP TABLE weekly_raffle__player_reveal__autocrawled CASCADE;

CREATE TABLE weekly_raffle__player_reveal__autocrawled(
  game_id_ INT,
  secret_ BIGINT,
  nonce_ BIGINT,
  player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX weekly_raffle__player_reveal__autocrawled_IndexOn_game_id_ ON weekly_raffle__player_reveal__autocrawled(game_id_);

CREATE INDEX weekly_raffle__player_reveal__autocrawled_IndexOn_player_ ON weekly_raffle__player_reveal__autocrawled(player_);

DROP TABLE weekly_raffle__reimburse_players__autocrawled CASCADE;

CREATE TABLE weekly_raffle__reimburse_players__autocrawled(
  game_id_ INT,
  refunded_player_ CHAR(36),
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX weekly_raffle__reimburse_players__autocrawled_IndexOn_game_id_ ON weekly_raffle__reimburse_players__autocrawled(game_id_);

DROP TABLE weekly_raffle__organizer_reveal__autocrawled CASCADE;

CREATE TABLE weekly_raffle__organizer_reveal__autocrawled(
  game_id_ INT,
  secret_ BIGINT,
  nonce_ BIGINT,
  winner_ CHAR(36),
  result_ raffle_final_status,
  nb_regular_ INT,
  nb_committed_ INT,
  nb_revealed_commits_ INT,
  inserted_on_ BIGINT,
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX weekly_raffle__organizer_reveal__autocrawled_IndexOn_game_id_ ON weekly_raffle__organizer_reveal__autocrawled(game_id_);

DROP TABLE weekly_raffle__denounce_organizer__autocrawled CASCADE;

CREATE TABLE weekly_raffle__denounce_organizer__autocrawled(
  game_id_ INT,
  nb_regular_ INT,
  nb_committed_ INT,
  nb_revealed_commits_ INT,
  inserted_on_ BIGINT,
  op_hash_ CHAR(51),
  block_hash_ CHAR(51),
  block_level_ BIGINT
);

CREATE INDEX weekly_raffle__denounce_organizer__autocrawled_IndexOn_game_id_ ON weekly_raffle__denounce_organizer__autocrawled(game_id_);


-- </end: crawling tables for raffle automatically generated by raffle-crawler.js>
