#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


called_from=`dirname $0`
cd $called_from

source ../src/shell-common/parse-params.sh "$@" "--prefix-dir=.."

if [ "$DUNE_PLAYGROUND_DB_USER" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_DB_USER env variable"
    exit 1
fi

if [ "$DUNE_PLAYGROUND_DB_NAME" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_DB_NAME env variable"
    exit 1
fi

if [ "$DUNE_PLAYGROUND_DB_HOST" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_DB_HOST env variable"
    exit 1
fi

if [ "$DUNE_PLAYGROUND_DB_PORT" = "" ] ; then
    printf "Error: ../../shell-common/parse-params.sh failed to set a value for \
DUNE_PLAYGROUND_DB_PORT env variable"
    exit 1
fi

echo psql $DUNE_PLAYGROUND_DB_NAME \
     --host $DUNE_PLAYGROUND_DB_HOST \
     --port $DUNE_PLAYGROUND_DB_PORT \
     --username $DUNE_PLAYGROUND_DB_USER


psql $DUNE_PLAYGROUND_DB_NAME \
     --host $DUNE_PLAYGROUND_DB_HOST \
     --port $DUNE_PLAYGROUND_DB_PORT \
     --username $DUNE_PLAYGROUND_DB_USER  \
     --file DATABASE.sql
