all:
	@-printf "\nNo default target. See DEPLOY.md for a tutorial if needed\n\n"

help:all

beta-publish:
	rsync -azi --no-owner --no-group . --exclude '.git' --exclude 'db.commits'  --exclude 'node_modules' --exclude 'nbproject' --exclude '*~' --exclude '_coverage' --exclude 'operation_hash' okkad@s4.xtz.world:/home/okkad/dune-good-games/

.PHONY: website all help

## BEGIN website

website:
	echo "TODO: generate dynamic parts of the website in website/"
	mkdir -p website/dapps/dgg-token
	mkdir -p website/dapps/free-duns
	mkdir -p website/dapps/rps
	mkdir -p website/dapps/dune-raffle
	mkdir -p website/dapps/rps+server
	mkdir -p website/dapps/moonshot
	mkdir -p website/dapps/moonshot+server
	mkdir -p website/dapps/fake
	mkdir -p website/images
	cp -rf src/dgg-token-exchange/www/* website/dapps/dgg-token/
	cp -rf src/dun-airdrop/www/* website/dapps/free-duns/
	cp -rf src/serverless-rps/www/* website/dapps/rps/
	cp -rf src/rps-with-server/www/* website/dapps/rps+server/
	cp -rf src/serverless-moonshot/www/* website/dapps/moonshot/
	cp -rf src/moonshot-with-server/www/* website/dapps/moonshot+server/
	cp -rf src/website-common/static/* website/dapps/fake/
	cp -rf src/website-common/css/* website/dapps/
	cp -rf src/website-common/js/* website/dapps/
	./src/shell-common/gen-website-parameters.sh website/dapps/parameters.js
	cp -rf src/website-common/php/* website/
	cp -rf src/website-common/images/* website/images/
	cp -rf src/website-common/images/favicon.ico website/favicon.ico
	cp -rf src/dune-raffle/www/* website/dapps/dune-raffle/

website-clean:
	rm -rf website/dapps
	rm -rf website/*.php
	rm -rf website/images
	rm -rf website/favicon.ico

## END website

## BEGIN services

services-clean:
	rm -rf systemd-services/systemd/*.service

services-deploy:
	./systemd-services/systemd/deploy-services.sh

services-undeploy:
	./systemd-services/systemd/undeploy-services.sh

services-start:
	./systemd-services/systemd/update-services.sh start

services-stop:
	./systemd-services/systemd/update-services.sh stop

services-restart:
	./systemd-services/systemd/update-services.sh restart

services-status:
	./systemd-services/systemd/update-services.sh status

## END services

logs-clean:
	-rm -f systemd-services/logs/*.log*
	-rm -rf systemd-services/logs/last-emails-timestamp
