#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

services=""

for s in `ls *.service`; do
    service=`basename $s`
    printf "Undeploying service $service\n"
    services="$services $service"
done

sudo systemctl stop $services
sudo systemctl disable $services

cd /etc/systemd/system/ && sudo rm -f $services
cd /usr/lib/systemd/system && sudo rm -f $services

sudo systemctl daemon-reload
sudo systemctl reset-failed
