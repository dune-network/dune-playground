#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

DUNE_PLAYGROUND_SCRIPTS_DIR=`pwd`

source ../../src/shell-common/parse-params.sh "$@" "--prefix-dir=../.."

services=""

for s in `ls *.service`; do
    service=`basename $s`
    if [ "$service" = "01-playground-dune-node.service" ] &&
           ! [ "$DUNE_PLAYGROUND_LOCAL_NODE_ENABLED" = "true" ]; then
        printf "!! Skip service $service: not enabled !!\n"
    else
        printf "Will Deploy service $service\n"
        services="$services $service"
        # use cp instead of ln to avoid warning: The unit file xxx.service changed on disk.
        sudo cp -rf $DUNE_PLAYGROUND_SCRIPTS_DIR/$service /etc/systemd/system/$service
    fi
done

sudo systemctl enable $services
sudo systemctl daemon-reload # in case the service is already deployed and needs reload
