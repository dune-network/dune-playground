#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

DUNE_PLAYGROUND_SCRIPTS_DIR=`pwd`

source ../../src/shell-common/parse-params.sh "$@" "--prefix-dir=../.."


ACTION=$1

if [ "$ACTION" = "" ] ; then
    printf "$0: an action (start | stop | restart | status) is expected as first argument"
    exit 1
fi

services=""

for s in `ls *.service`; do
    service=`basename $s`
    if [ "$service" = "01-playground-dune-node.service" ] &&
           ! [ "$DUNE_PLAYGROUND_LOCAL_NODE_ENABLED" = "true" ]; then
        printf "!! Skip service $service: not enabled !!\n"
    else
        printf "Will add service $service\n"
        services="$services $service"
    fi
done

printf "I'll $ACTION services $services\n"

sudo systemctl $ACTION $services -n 3
