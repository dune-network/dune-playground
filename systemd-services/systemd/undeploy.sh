#!/bin/bash
# set -euo pipefail
# IFS=$' \n\t'

cd `dirname $0`

services=""

for s in `ls *.service`; do
    service=`basename $s`
    printf "Undeploying service $service\n"
    services="$services $service"
done

systemctl stop $services
systemctl disable $services

cd /etc/systemd/system/ && rm $services
cd /usr/lib/systemd/system && rm $services

systemctl daemon-reload
systemctl reset-failed
