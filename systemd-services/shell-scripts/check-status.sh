#!/bin/bash

set -euo pipefail
IFS=$' \n\t'

# exit 0 => command is running
# exit 1 => command is not running
# exit 2 => script error

COMMAND_NAME=$DUNE_PLAYGROUND_COMMAND_NAME
PIDFILE=$DUNE_PLAYGROUND_PID_FILE

if test -z "$PIDFILE"; then
    echo "check-status.sh should be called from another script."
    exit 2
fi

if test -f $PIDFILE; then
    PID=$(cat $PIDFILE)
    echo check if PID $PID is alive
    if pkill -0 -P $PID || kill -0 $PID; then
        echo "$COMMAND_NAME is running ! (pid=$PID)"
        exit 0
    else
        echo "$COMMAND_NAME is down. Removing pid file."
        rm -f $PIDFILE
        exit 1
    fi
else
    echo $COMMAND_NAME is not running
    exit 1
fi
