#!/bin/bash
set -euo pipefail
IFS=$' \n\t'


DUNE_PLAYGROUND_PID_FILE=$DUNE_PLAYGROUND_SCRIPTS_DIR/../logs/$DUNE_PLAYGROUND_COMMAND_NAME.pid
DUNE_PLAYGROUND_LOG_FILE=$DUNE_PLAYGROUND_SCRIPTS_DIR/../logs/$DUNE_PLAYGROUND_COMMAND_NAME.log

export DUNE_PLAYGROUND_PID_FILE
export DUNE_PLAYGROUND_LOG_FILE

start() {
    $DUNE_PLAYGROUND_SCRIPTS_DIR/send-mail.sh "STARTING $DUNE_PLAYGROUND_COMMAND_NAME"
    check_status=`$DUNE_PLAYGROUND_SCRIPTS_DIR/check-status.sh || true`
    echo "$check_status"
    nb_running=`echo "$check_status" | grep -c " is running ! (pid=" || true`
    if ! [ "$nb_running" -eq "0" ]; then
        echo "It seem that the script is already running. Ignoring 'start' command ..."
        exit 1
    else
        $DUNE_PLAYGROUND_DUNE_NETWORK_DIR/dune-rotate --max 200 $DUNE_PLAYGROUND_LOG_FILE &> /dev/null
        cd $DUNE_PLAYGROUND_WORKING_DIR

        ##
        ## Compiling twice is mainly done because some npm versions fail if 'npm
        ## init' is not ran and/or if 'npm install . ' is not ran twice
        ##
        [ "$DUNE_PLAYGROUND_COMPILE_CMD" = "" ] || (printf "Compiling 1 ...\n" && $DUNE_PLAYGROUND_COMPILE_CMD) || true
        sleep 1
        [ "$DUNE_PLAYGROUND_COMPILE_CMD" = "" ] || (printf "Compiling 2 ...\n" && $DUNE_PLAYGROUND_COMPILE_CMD)

        $DUNE_PLAYGROUND_START_CMD &> $DUNE_PLAYGROUND_LOG_FILE &

        echo $! > $DUNE_PLAYGROUND_PID_FILE
        echo "$DUNE_PLAYGROUND_COMMAND_NAME started"

        sleep 1
        echo "You can watch the log with:"
        echo tail -f $DUNE_PLAYGROUND_LOG_FILE
        $DUNE_PLAYGROUND_SCRIPTS_DIR/check-status.sh
    fi
}

stop() {
    $DUNE_PLAYGROUND_SCRIPTS_DIR/send-mail.sh "STOPPING $DUNE_PLAYGROUND_COMMAND_NAME"
    $DUNE_PLAYGROUND_SCRIPTS_DIR/stop-process.sh
}

status() {
    $DUNE_PLAYGROUND_SCRIPTS_DIR/check-status.sh
}

case "$1" in
    start)
        start
        ;;

    stop)
        stop
        ;;

    restart)
        stop || true
        sleep 1
        start
        ;;

    status)
        status
        ;;

    *)
        printf "\nBad argument \"$1\""
        printf "\nUsage: <script> [start|stop|restart|status]"
        exit 1
        ;;
esac
