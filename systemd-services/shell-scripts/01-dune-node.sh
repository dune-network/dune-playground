#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

DUNE_PLAYGROUND_SCRIPTS_DIR=`pwd`

source ../../src/shell-common/parse-params.sh "$@" "--prefix-dir=../.."

DUNE_PLAYGROUND_COMMAND_NAME=01-dune-node

DUNE_PLAYGROUND_WORKING_DIR="$DUNE_PLAYGROUND_DUNE_NETWORK_DIR"

DUNE_PLAYGROUND_COMPILE_CMD="" # we assume the node already compiled

ENABLED=$DUNE_PLAYGROUND_LOCAL_NODE_ENABLED
ADDR=$DUNE_PLAYGROUND_LOCAL_NODE_ADDR
P2P_PORT=$DUNE_PLAYGROUND_LOCAL_NODE_P2P_PORT
RPC_PORT=$DUNE_PLAYGROUND_LOCAL_NODE_RPC_PORT
BOOTSTRAP_PEERS=$DUNE_PLAYGROUND_LOCAL_NODE_BOOTSTRAP_PEERS
DATA_DIR=$DUNE_PLAYGROUND_LOCAL_NODE_DATA_DIR
DUNE_CONTEXT_STORAGE=$DUNE_PLAYGROUND_LOCAL_NODE_DUNE_CONTEXT_STORAGE

if [ "$ENABLED" = "true" ] ; then
    export DUNE_CONFIG="$DUNE_PLAYGROUND_NETWORK_CONFIG_FLAG"
    if ! [ "$DUNE_CONTEXT_STORAGE" = "" ]; then
        export DUNE_CONTEXT_STORAGE="$DUNE_CONTEXT_STORAGE"
    fi
    DUNE_PLAYGROUND_START_CMD=" $DUNE_PLAYGROUND_DUNE_NETWORK_DIR/dune-node run \
                                       --net-addr $ADDR:$P2P_PORT \
                                       --rpc-addr $ADDR:$RPC_PORT \
                                       $BOOTSTRAP_PEERS \
                                       --connections 10 \
                                       --data-dir $DATA_DIR \
                                       --cors-origin=*"

    echo "$DUNE_PLAYGROUND_START_CMD"

    export DUNE_PLAYGROUND_COMMAND_NAME
    export DUNE_PLAYGROUND_SCRIPTS_DIR
    export DUNE_PLAYGROUND_WORKING_DIR
    export DUNE_PLAYGROUND_COMPILE_CMD
    export DUNE_PLAYGROUND_START_CMD

    source ./start-and-stop.sh $@
else
    echo "Local node is not enabled in config file. Skipping"
    exit 0
fi
