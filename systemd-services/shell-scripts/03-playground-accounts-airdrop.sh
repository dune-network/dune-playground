#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

DUNE_PLAYGROUND_SCRIPTS_DIR=`pwd`

source ../../src/shell-common/parse-params.sh "$@" "--prefix-dir=../.."

DUNE_PLAYGROUND_COMMAND_NAME=03-playground-accounts-airdrop

DUNE_PLAYGROUND_WORKING_DIR="../../src/accounts-manager/accounts-airdrop/"

DUNE_PLAYGROUND_COMPILE_CMD="npm install ."

DUNE_PLAYGROUND_START_CMD="./accounts-airdrop.sh loop"

export DUNE_PLAYGROUND_COMMAND_NAME
export DUNE_PLAYGROUND_SCRIPTS_DIR
export DUNE_PLAYGROUND_WORKING_DIR
export DUNE_PLAYGROUND_COMPILE_CMD
export DUNE_PLAYGROUND_START_CMD

source ./start-and-stop.sh $@
