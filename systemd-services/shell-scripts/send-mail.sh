#!/bin/bash

set -euo pipefail
IFS=$' \n\t'

CALLER="$1"

# supposed to be executed by a caller script that already called parse-params.sh.
# source ../../shell-common/parse-params.sh "$@" "--prefix-dir=../../.."
#

if [ "$DUNE_PLAYGROUND_ALERT_EMAIL_ADDRESSES" = "" ] ; then
    printf "[send-mail.sh] No email provided in DUNE_PLAYGROUND_ALERT_EMAIL_ADDRESSES! will not report event by email !\n\n"
else
    subject="[Playground] $CALLER"
    content="$CALLER"

    here=`dirname $0`
    timestamp_dir=$here/../logs/last-emails-timestamp/
    mkdir -p $timestamp_dir

    CALLER_ID=`echo $CALLER | sha256sum | cut -d " " -f1`

    function send_mail(){
        seconds=`date +%s`
        date=`date`
        to="$1"
        printf $seconds > $timestamp_dir/$CALLER_ID
        printf "\nsend mail: running command\n  mail -s \"$subject\" \"$to\" <<< \"$date : $content\"\n\n"
        mail -s "$subject" "$to" <<< "$date : $content" || true
    }

    if ! [ -e "$timestamp_dir/$CALLER_ID" ] ; then
        # first time we send an alert
        send_mail "$DUNE_PLAYGROUND_ALERT_EMAIL_ADDRESSES"
    else
        old=`cat $timestamp_dir/$CALLER_ID`
        new=`date +%s`
        diff=$(($new - $old))
        if [ "$diff" -ge "300" ] ; then
            # No more than 1 email alert every 300 seconds
            send_mail "$DUNE_PLAYGROUND_ALERT_EMAIL_ADDRESSES"
        fi

    fi
fi
