#!/bin/bash
set -euo pipefail
IFS=$' \n\t'

cd `dirname $0`

DUNE_PLAYGROUND_SCRIPTS_DIR=`pwd`

source ../../src/shell-common/parse-params.sh "$@" "--prefix-dir=../.."

DUNE_PLAYGROUND_COMMAND_NAME=02-api-server

DUNE_PLAYGROUND_WORKING_DIR="../../src/rest-api-server/"

DUNE_PLAYGROUND_COMPILE_CMD="npm install ."

DUNE_PLAYGROUND_START_CMD="node api-server.js --config $DUNE_PLAYGROUND_CONFIG_FILE"

export DUNE_PLAYGROUND_COMMAND_NAME
export DUNE_PLAYGROUND_SCRIPTS_DIR
export DUNE_PLAYGROUND_WORKING_DIR
export DUNE_PLAYGROUND_COMPILE_CMD
export DUNE_PLAYGROUND_START_CMD

source ./start-and-stop.sh $@
